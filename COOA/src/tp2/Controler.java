package tp2;

public class Controler {
	
	Modele m;
	
	
	public Controler(Modele m){
		this.m = m;
	}
	
	public void incTemp(){
		if(m.getTempC()<100){
			m.setTempC(m.getTempC()+1);
		}else{
			m.setTempC(100.0);
		}
		
	}

	public void decTemp(){
		if(m.getTempC()>-20){
			m.setTempC(m.getTempC()-1);
		}else{
			m.setTempC(-20.0);
		}
		
	}
	
	public void setTempC(double x){
		m.setTempC(x);
	}
	
	/*public void incTempF(){
		m.setTempC(FtoC(
				CtoF(m.getTempC())+1
				));
	}

	public void decTempF(){
		m.setTempC(FtoC(
				CtoF(m.getTempC())-1
				));
	}
	
	
	private double CtoF(double cel){
		return cel * (9/5) + 32;
	}
	
	private double FtoC(double fa){
		return (fa-32)*(5/9);
	}*/
	

}
