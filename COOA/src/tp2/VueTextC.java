package tp2;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Observable;
import java.util.Observer;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class VueTextC extends Application implements Observer{
	
	Controler c;
	Modele m;
	TextField text;
	
	public VueTextC(Modele m,Controler c){
		this.m = m;
		this.c = c;
	}

	public void update(Observable o, Object arg) {
			
		this.m = (Modele)o;
		NumberFormat formatter = new DecimalFormat("#0.00");  
		text.setText(""+formatter.format(m.getTempC()));
		
		
	}

	@Override
	public void start(Stage stage) throws Exception {
		
		VBox main = new VBox();
			Label titre = new Label("Température celcuis");
			
			this.text = new TextField();
			/*text.setOnKeyPressed(e->{
				if(e.getCode()==KeyCode.ENTER){
					try {
						int res = Integer.valueOf(text.getText());
						System.out.println("Vue text c ligne 40");
					} catch (Exception e2) {
						text.clear();
					}
				}
			});*/
			
			HBox boutons = new HBox();
			Button moin = new Button("-");
			moin.setOnAction(e->{
				c.decTemp();
			});
			Button plus = new Button("+");
			plus.setOnAction(e->{
				c.incTemp();
			});
			boutons.getChildren().addAll(plus,moin);
			
		main.getChildren().addAll(titre,text,boutons);
		
		Scene s = new Scene(main);
		stage.setScene(s);
		stage.show();
			
		
		
	}

	
	public void go(){
		try {
			this.start(new Stage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
