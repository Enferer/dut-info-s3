package tp2;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		Modele m = new Modele();
		Controler cont = new Controler(m);
		
		VueTextF vue = new VueTextF(m,cont); 
		m.addObserver(vue);
		vue.go();
		
		VueTextC vuetextc = new VueTextC(m,cont);
		m.addObserver(vuetextc);
		vuetextc.go();
		
		VueSliderC vuesliderc = new VueSliderC(m,cont);
		m.addObserver(vuesliderc);
		vuesliderc.go();
		
	}
	
	public static void main(String[]agrs){
		Application.launch(agrs);
	}

}
