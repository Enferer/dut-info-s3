package tp2;

import java.util.Observable;

public class Modele extends Observable{

	double tempC = 0;



	public void setTempC(double tempC) {
		this.tempC = tempC;
		this.setChanged();
		this.notifyObservers(null);
	}
	

	public double getTempC(){
		return tempC;
	}
	
	public double getTempF(){
		//System.out.println(""+((tempC * (9/5)) + 32));
		return tempC * 1.8 + 32;
	}
	
	
	


}
