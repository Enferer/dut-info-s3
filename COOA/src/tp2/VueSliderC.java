package tp2;

import java.util.Observable;
import java.util.Observer;

import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class VueSliderC extends Application implements Observer{

	Controler c;
	Modele m;
	Slider s;
	public VueSliderC(Modele m,Controler c){
		this.m = m;
		this.c = c;
	}
	
	public void update(Observable o, Object arg) {
		this.m = (Modele) o;
		s.setValue(m.getTempC());
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		VBox cont = new VBox();
		Scene scene = new Scene(cont);
		this.s = new Slider();
		s.setBlockIncrement(1);
		s.setMajorTickUnit(10);
		s.setOrientation(Orientation.VERTICAL);
		s.setPrefSize(200, 300);
		s.setValue(0);
		s.setShowTickLabels(true);
		s.setShowTickMarks(true);
		s.setMin(-20);
		s.setMax(100);
		//s.setOn
		s.setOnMouseMoved(e->{
			//s.getValue();
			c.setTempC(s.getValue());
		});
		cont.getChildren().add(s);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Celsius");
		primaryStage.show();
		
	}
	
	public void go(){
		try {
			this.start(new Stage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
