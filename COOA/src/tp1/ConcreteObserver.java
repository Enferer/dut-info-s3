package tp1;



public class ConcreteObserver extends Observer{

	private AfficheurObserver afficheur;
	private ConcreteObservable mainStage;
	private String text;
	
	public ConcreteObserver(ConcreteObservable mainStage) {
		this.mainStage = mainStage;
		afficheur = new AfficheurObserver();
		afficheur.go();
		
		
	}
	


	@Override
	public void updateObserver() {
		this.text = this.mainStage.getState();
		afficheur.setText(text);
		
	}
	
	
	
	

}
