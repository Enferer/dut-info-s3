package tp1;



import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AfficheurObserver extends Application{

	
	TextArea vue = new TextArea();
	
	public void start(Stage stage) throws Exception {
		vue.setEditable(false);
		VBox h = new VBox();;
		h.getChildren().add(vue);
		vue.setMinSize(200, 300);
		Scene s = new Scene(h);
		
		stage.setTitle("Salut");
		stage.setScene(s);
		stage.show();
		
		
	}
	
	public void setText(String msg){
		
		vue.clear();
		vue.appendText(msg);
	}
	

	public void go() {
		try {
			start(new Stage());
		} catch (Exception e) {
			
		}
	}
	

}
