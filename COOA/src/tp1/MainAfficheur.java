package tp1;




import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainAfficheur extends Application{

	ConcreteObservable c = new ConcreteObservable();
	String text = "";
	
	@Override
	public void start(Stage stage) throws Exception {
		VBox v = new VBox();
		Scene s = new Scene(v);
		Button addAudit = new Button("Nouvel auditeur");

		TextArea vue = new TextArea();
		TextField ecrit = new TextField();
		
		v.getChildren().add(vue);
		v.getChildren().add(ecrit);
		v.getChildren().add(addAudit);
		
		vue.setMinSize(100, 600);
		vue.setEditable(false);
	//	vue.setAlignment(Pos.TOP_LEFT);
		
		
		
		stage.setTitle("Radui tchat");
		addAudit.setMinSize(600, 10);
		
		addAudit.setOnMouseClicked(e->{
			c.attach(new ConcreteObserver(c));
		});
		
		ecrit.setOnKeyPressed(e->{
			if(e.getCode() == KeyCode.ENTER ){
					text += ecrit.getText()+"\n";
					vue.clear();
					vue.appendText(text);
					ecrit.clear();
					c.setState(text);
					c.notifyObserver();

			}
			
		});
		

		stage.setScene(s);
		stage.show();
		
	}
	
	public static void main(String[]args){
		Application.launch(args);
	}
	
	
}
