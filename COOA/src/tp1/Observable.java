package tp1;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable {
	
 private List<Observer> listObservers= new ArrayList<Observer>();

	public void attach(Observer o){
		listObservers.add(o);
	}
	
	public void detach(Observer o){
		listObservers.remove(o);
	}
	
	public void notifyObserver(){
		for(Observer o: listObservers){
			o.updateObserver();
		}
	}
}