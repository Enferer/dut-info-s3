package tp1;

public class ConcreteObservable extends Observable{
	
	
	private String text;
	
	
	public String getState(){
		return this.text;
	}

	public void setState(String text){
		this.text = text;
	}
	
	
	
}
