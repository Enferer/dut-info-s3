package tp3;

import java.util.ArrayList;
import java.util.Stack;

public class Calculatrice {
	
	
	public int calculate(String exp){
		if(!parentheseOk(exp)){
			return 0;
		}
		
		String expPostFix = getPostFix(exp);
		return 0;
		
		
	}
	
	
	
	private String getPostFix(String exp) {
		Stack<String> pile = new Stack<>();
		String res = "";
		exp = suppSpace(exp);
		ArrayList<String> l = new ArrayList<>();
		l = getListElement(exp);
		
		for (String s : l) {
			if(isNumber(s)){
				res += s+" ";
			}
			else{
				pile.add(s);
				res += depile(pile);
			}
		}
		
		return res;
	}
	
	private String depile(Stack<String> pile) {
		if(pile.get(pile.size()-1).equals(")")){
			pile.pop();
			String res = pile.pop();
			try {
				pile.pop();
			} catch (Exception e) {
				
			}
			
			//System.out.println("if");
			return res;
		}
		
		return "";
	}



	private String suppSpace(String s){
		String res = "";
		for (int i = 0; i < s.length(); i++) {
			if(s.charAt(i)!=' '){
				res += s.charAt(i);
			}
		}
		return res;
	}
	
	public ArrayList<String> getListElement(String exp) {
		ArrayList<String> res = new ArrayList<>();
		boolean nbr = false;
		//int cptL = 0;
		for (int i = 0; i < exp.length(); i++) {
			if(isNumber(""+exp.charAt(i))){
				if(nbr){
					String tmp = res.get(res.size()-1);
					tmp += exp.charAt(i);
					res.remove(res.size()-1);
					res.add(tmp);
					//cptL++;
				}else{
					res.add(""+exp.charAt(i));
					//cptL++;
				}
				nbr = true;
			}else{
				res.add(""+exp.charAt(i));
				//cptL++;
				nbr = false;
			}
			
		}
		return res;
	}



	private boolean isNumber(String s){
		try{
			int i = Integer.valueOf(s);
		}catch(Exception e){
			return false;
		}
		return true;
	}



	private boolean parentheseOk(String s){
		int cpt = 0;
		for (int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == '('){
				cpt++;
			}else if(s.charAt(i) == ')'){
				cpt--;
			}
			if( cpt < 0){
				return false;
			}
		}
		if(cpt == 0){
			return true;
		}
		return false;
		
	}

	
	public static void main(String arg[]){
		Calculatrice c = new Calculatrice();
		System.out.println(c.getPostFix("(250+250)*(150+150)"));
	}
}
