package tp3;

import static org.junit.Assert.*;

import org.junit.Test;

public class MissionTest {

	@Test
	public void test1() { //jdep==jret && hdep=11 && hret=18
		Mission m = new Mission(17,11,17,18,0);
		assertEquals(m.fraisMiss(), 30);
	}
	
	@Test
	public void test2() { //jdep==jret && hdep=15 && hret=22
		Mission m = new Mission(17,15,17,22,0);
		assertEquals(m.fraisMiss(), 0);
	}
	
	@Test
	public void test3() { //jdep=18 && jret=19 && hdep=18 && hret=19
		Mission m = new Mission(18,18,19,19,0);
		assertEquals(m.fraisMiss(), 105);
	}
	
	@Test
	public void test4() { //jdep=18 && jret=19 && hdep=11 && hret=14
		Mission m = new Mission(18,11,19,14,0);
		assertEquals(m.fraisMiss(), 105);
	}
	/*@Test
	public void test5() { //jdep=18 && jret=19 && hdep=22 && hret=15
		Mission m = new Mission(18,22,19,15,0);
		assertEquals(m.fraisMiss(), 75);
	}*/
	

}
