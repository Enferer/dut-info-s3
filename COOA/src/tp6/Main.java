package tp6;

public class Main {

	public static void main(String[] args) {
		Factory factory = Factory.getInstance();
		
		Dessert d1 = factory.getDessert("gauffre");
		Dessert d2 = factory.getDessert("crepe");
		Dessert d3 = factory.getDessert("crepe");
		Dessert d4 = factory.getDessert("crepe");
		DessertComposite plateau = new DessertComposite();
		plateau.add(d1);
		plateau.add(d2);
		plateau.add(d3);
		plateau.add(d4);
		//plateau.print();
		//System.out.println("prix plateau : " + plateau.getPrice());
		DessertCompositeVisiteur d = new DessertCompositeVisiteur();
		d.visiterStock(plateau);
		
        Dessert d42 = new Crepe();
        d42 = new Chocolat(d42);
        d42 = new Chantilly(d42);
        System.out.println(d42);
		
	}

}
