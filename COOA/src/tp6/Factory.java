package tp6;

public class Factory {
	
	private static Factory INSTANCE = new Factory();
	
	public Factory(){}
	
	public static Factory getInstance(){
		return INSTANCE;
	}
	
	public Dessert getDessert(String dessert){
		switch (dessert) {
		case "crepe":
			return new Crepe();
		case "gauffre":
			return new Gauffre();
		default:
			return null;
		}
	}

}
























