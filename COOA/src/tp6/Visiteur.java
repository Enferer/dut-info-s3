package tp6;

public interface Visiteur {

	public float visiterPrix(DessertComposite p);
	
	public float visiterPrixRemise(DessertComposite p);
	
	public void visiterStock(DessertComposite p);
	
}
