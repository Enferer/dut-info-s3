package tp6;



public abstract class Dessert{
        private String nom;
        private double prix;
        

        public String getNom(){
                return nom;
        }
        public double getPrix(){
                return prix;
        }
        
        public void setNom(String libelle){
                this.nom = libelle;
        }
        
        public void setPrix(double prix){
                this.prix = prix;
        }

        public String toString(){
               return getNom()+" : "+getPrix()+"€";
        }
        
     
        public static class Main{
                
                public static void main(String[] args){
                       
                        Dessert d1 = new Gauffre();
                        d1 = new Chocolat(d1);
                        System.out.println(d1);
                        Dessert d2 = new Crepe();
                        d2 = new Chocolat(d2);
                        d2 = new Chantilly(d2);
                        System.out.println(d2);
                }
        }
}
