package tp6;

public class Crepe extends Dessert{
	
	private static final double PRIX = 1.5;
    
     public Crepe(){
             setNom("Crêpe");
             setPrix(Crepe.PRIX);
     }
}