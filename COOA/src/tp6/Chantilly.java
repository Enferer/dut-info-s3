package tp6;

public class Chantilly extends DecorateurIngredient{
	
		private static final double PRIX = 0.5; 
        
        public Chantilly(Dessert d){
                dessert = d;
        }
        
        public String getNom(){
                return dessert.getNom()+", chantilly";
        }
        
        public double getPrix(){
                return dessert.getPrix()+Chantilly.PRIX;
        }
}