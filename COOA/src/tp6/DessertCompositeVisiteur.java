package tp6;

public class DessertCompositeVisiteur implements Visiteur{

	@Override
	public float visiterPrix(DessertComposite p) {
		return (float)((float)p.getPrice()*1.1); 
	}

	@Override
	public float visiterPrixRemise(DessertComposite p) {
		return p.getPrice();
	}

	@Override
	public void visiterStock(DessertComposite p) {
		p.print();
		
	}
	
	
	
	

}
