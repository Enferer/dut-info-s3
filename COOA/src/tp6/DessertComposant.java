package tp6;

public interface DessertComposant {


		public void add(Dessert d);
		public void remove(Dessert d);
		public void print();
		public float getPrice();
		
		

}