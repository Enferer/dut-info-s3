package tp6;

public class Chocolat extends DecorateurIngredient{
	
		private static final double PRIX = 0.2; 
	
        public Chocolat(Dessert d){
                dessert = d;
        }
        
        public String getNom(){
                return dessert.getNom()+", chocolat";
        }
        
        public double getPrix(){
                return dessert.getPrix()+Chocolat.PRIX;
        }
}