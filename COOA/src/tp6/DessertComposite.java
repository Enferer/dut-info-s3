package tp6;

import java.util.ArrayList;

public class DessertComposite implements DessertComposant{
	
	
	private ArrayList<Dessert> plateau = new ArrayList<>();

	
	@Override
	public void add(Dessert d) {
		// TODO Auto-generated method stub
		if( d != null){
			plateau.add(d);
		}
	}

	@Override
	public void remove(Dessert d) {		
		plateau.remove(d);
	}
	
	

	@Override
	public void print() {
		for(Dessert d : plateau){
			System.out.println(d.toString());
		}
	}

	@Override
	public float getPrice() {
		float price=0;
		for(Dessert d : plateau){
			price+=d.getPrix();
		}
		price = (float)(price*0.9);
		
		return price;
	
	}
	
	


}
