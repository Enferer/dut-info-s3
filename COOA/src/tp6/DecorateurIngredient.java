package tp6;

public abstract class DecorateurIngredient extends Dessert{
      
		protected Dessert dessert;
       
        public abstract String getNom();
        public abstract double getPrix();

}