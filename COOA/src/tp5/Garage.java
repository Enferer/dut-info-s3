package tp5;

public class Garage implements IConteneur{
	
	Voiture[] tabVoiture ;
	AtelierPanneLegere atl1 = AtelierPanneLegere.getInstance();
	AtelierPanneSevere atl2 = AtelierPanneSevere.getInstance();
	
	public Garage(int nbDeVoiture) {
		tabVoiture = new Voiture[nbDeVoiture];
	}
	
	public class IterateurGarage implements Iiterateur{
		private int position;
		@Override
		public boolean hasNext() {
			return position < tabVoiture.length;
			
		}

		@Override
		public Object next() {
			if(hasNext()){
				//position++;
				return tabVoiture[position++];
			}
			return null;
		}

	
		
	}

	@Override
	public Iiterateur creerIterateur() {
		// TODO Auto-generated method stub
		return new IterateurGarage();
	}
	public void add(Voiture v,int position){
		tabVoiture[position]=v;
	}
	
	public static void main(String [] args) throws InterruptedException{
		Garage g = new Garage(5);
		
		Iiterateur it = g.creerIterateur();
		while(it.hasNext()){
				System.out.println(it.next());
		}
		
		

		
		
	}
	
}