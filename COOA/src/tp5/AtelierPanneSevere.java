package tp5;

import java.util.Random;

public final class AtelierPanneSevere extends Atelier{
	
	private static final AtelierPanneSevere INSTANCE = new AtelierPanneSevere(); 

	public static AtelierPanneSevere getInstance(){
		return INSTANCE;
	}
	

	@Override
	public int reparer(Voiture v) {
		v.setEtat(Etat.MARCHE);
		return new Random().nextInt(4)+3;
	}

}
