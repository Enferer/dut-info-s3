package tp5;

public class Voiture {
	private String immatriculation;
	private Etat etat;
	
	public Voiture(String immatriculation, Etat etat) {
		super();
		this.immatriculation = immatriculation;
		this.etat = etat;
	}

	public String getImmatriculation() {
		return immatriculation;
	}

	public void setImmatriculation(String immatriculation) {
		this.immatriculation = immatriculation;
	}

	public Etat getEtat() {
		return etat;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
	}
	
	public String toString(){
		return "imm : "+immatriculation + " etat : "+ etat.getName();
	}
	
	
	
}