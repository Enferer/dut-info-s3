package tp5;

public abstract class Atelier {

	boolean occuper = false;
	
	public boolean isOccuper(){
		return occuper;
	}
	
	public void setOccuper(boolean occuper){
		this.occuper = occuper;
	}
	
	public abstract int reparer(Voiture v);
	
	
}