package tp5;

public enum Etat {
	MARCHE("marche"),PANNELEGERE("panne légère"),PANNESEVERE("panne sévère");
	String name;

	private Etat(String name) {
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
}