package tp5;

import java.util.Random;

public final class AtelierPanneLegere extends Atelier{
	
	private static final AtelierPanneLegere INSTANCE = new AtelierPanneLegere(); 

	public static AtelierPanneLegere getInstance(){
		return INSTANCE;
	}
	
	@Override
	public int reparer(Voiture v) {
		v.setEtat(Etat.MARCHE);
		return new Random().nextInt(3)+1;
	}
	
	

}
