package tp4;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Test;

public class TestExoSet {

	@Test
	public void testUnion(){
		HashSet<Integer> ens1 = new HashSet<Integer>();
		HashSet<Integer> ens2 = new HashSet<Integer>();
		ens1.add(2);ens1.add(14);
		ens2.add(52);ens2.add(4);
		
		HashSet<Integer> res = new HashSet<>();
		res.add(2);res.add(52);res.add(14);res.add(4);
		
		assertEquals(res, ExoSet.union(ens2, ens1));
		assertNotEquals(ens1, ens2);
	}
	
	@Test
	public void testInter(){
		HashSet<Integer> ens1 = new HashSet<Integer>();
		HashSet<Integer> ens2 = new HashSet<Integer>();
		ens1.add(2);ens1.add(14);
		ens2.add(2);ens2.add(4);
		
		HashSet<Integer> res = new HashSet<>();
		res.add(2);
		
		assertEquals(res, ExoSet.inter(ens2, ens1));
		assertNotEquals(ens1, ens2);
	}
	
	@Test
	public void testIsIn(){
		HashSet<Integer> ens1 = new HashSet<Integer>();
		HashSet<Integer> ens2 = new HashSet<Integer>();
		ens1.add(2);ens1.add(4);
		ens2.add(2);ens2.add(4);ens2.add(5);ens2.add(14);
		
		assertTrue(ExoSet.isIn(ens1, ens2));
		assertFalse(ExoSet.isIn(ens2, ens1));
	}
	
	

}
