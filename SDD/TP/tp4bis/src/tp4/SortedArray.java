package tp4;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;

public class SortedArray<T> implements Set{
	
	private static final int DEFAULT_SIZE = 20;
	
	final Comparator<T> comparator = null;
	private T[] tab;
	int n = -1;
	
	@SuppressWarnings("unchecked")
	public SortedArray(){
		tab = (T[])new Object[DEFAULT_SIZE];
	}
	
	@SuppressWarnings("unchecked")
	public SortedArray(int n){
		tab = (T[])new Object[n];
	}

	private int indexOf(T t){
		for (int i = 0; i < tab.length; i++) {
			if(t == tab[i]){
				return i;
			}
		}
		return -1;
	}
	
	private void insert(int idx,T t){
		for (int i = n; i >= idx; i--) {
			tab[i+1] = tab[i];
		}
		tab[idx]=t;
	}
	
	private void remove(int idx){
		for (int i = idx; i < tab.length-1; i++) {
			tab[idx]=tab[idx+1];
		}
	}
	
	@Override
	public int size() {
		
		return this.n+1;
	}

	@Override
	public boolean isEmpty() {
		return n == -1;
	}

	@Override
	public boolean contains(Object o) {
		int i;
		for (i = 0; i < tab.length && tab[i]==o; i++) {	
		}
		return i<tab.length-1;
	}
	
	

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(Object e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}
	
	
	public static void main(String args[]){
		int[] tab = {1,2,3,4,5,6,7};
		for (int i = 2; i < tab.length-2; i++) {
			tab[i]=tab[i+1];
		}
		System.out.println(tab[2]);
		
	}
	
	
	

}
