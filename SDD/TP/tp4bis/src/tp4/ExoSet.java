package tp4;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class ExoSet {
	
	public static void main(String[]args){
		HashSet<Integer> ens1 = new HashSet<Integer>();
		HashSet<Integer> ens2 = new HashSet<Integer>();
		ens1.add(2);ens1.add(14);ens1.add(133);ens1.add(18);ens1.add(465);ens1.add(42);
		ens2.add(52);ens2.add(4);ens2.add(16);ens2.add(15);ens2.add(45);ens2.add(42);
		
		ExoSet.afficher(ens1);
		ExoSet.afficher(ens2);
		
		ExoSet.afficher((HashSet<Integer>)inter(ens1, ens2));
		ExoSet.afficher((HashSet<Integer>)union(ens1, ens2));
		
		
	}
	
	public static void afficher(HashSet<Integer> e){
		Iterator<Integer> i = e.iterator();
		System.out.print("{");
		if(i.hasNext()){
			System.out.print(i.next());
		}
		while(i.hasNext()){
			System.out.print(","+i.next());
		}
		System.out.println("}");
	}

	public static Set<Integer> inter(Set<Integer> set1, Set<Integer> set2){
		HashSet<Integer> res= new HashSet<>();
		
		for (Integer i : set1) {
			if(set2.contains(i)){
				res.add(i);
			}
		}
		
		
		return res;
	}
	
	public static Set<Integer> union(Set<Integer> set1, Set<Integer> set2){
		HashSet<Integer> res= new HashSet<>();
		
		for (Integer i : set1) {
			res.add(i);
		}
		for (Integer i : set2) {
			res.add(i);
		}
		return res;
		
	}
	
	
	public static boolean isIn(Set<Integer> set1, Set<Integer> set2){
		for (Integer i : set1) {
			if(!set2.contains(i)){
				return false;
			}
		}
		return true;
	}
	
}
