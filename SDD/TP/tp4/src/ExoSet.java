import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ExoSet {
	HashSet<Integer> ens1=new HashSet<>();
	HashSet<Integer> ens2=new HashSet<>();

	public ExoSet(){
		for(int i=0; i<3;i++){
			ens1.add((int)(Math.random()*10));
			ens2.add((int)(Math.random()*10));
			ens2.add((int)(Math.random()*10));
		}
	}
	
	public static void afficher(Set<Integer> ens){
		String res="["+ens.size()+"]{";
		Iterator<Integer> it = ens.iterator();
		if(!ens.isEmpty()){
			res+=it.next().toString();
		
			while(it.hasNext()){
			
			res+=", "+it.next().toString();
			}
		}
		res+='}';
		System.out.println(res);
	}
	
	public static Set<Integer> inter(Set<Integer> set1, Set<Integer> set2){
		HashSet<Integer> res = new HashSet<>();
		for(Integer i: set1){
			if(set2.contains(i)){
				res.add(i);
			}
		}
		return res;
			
	}
	
	public static Set<Integer> union(Set<Integer> set1, Set<Integer> set2){
		HashSet<Integer> res=new HashSet<>();
		for(Integer i:set1){
			res.add(i);
		}
		for(Integer i:set2){
			res.add(i);
		}
		return res;
		
	}
	
	public static boolean isIn(Set<Integer> set1, Set<Integer> set2){
		return ExoSet.inter(set1, set2).equals(set1);
	}
	
	
}
