

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;

public class SortedArraySet<E> implements SortedSet<E> {

	Comparator<E> comp;
	static final int DEFAULT_SIZE = 1024;
	@SuppressWarnings("unchecked")
	E[] tab = (E[]) new Object[DEFAULT_SIZE];
	int n=0;
	
	public SortedArraySet() {
		
		this(2000);
	}
	@SuppressWarnings("unchecked")
	public SortedArraySet(int size) {
		n=0;
		tab=(E[])new Object[size];
	}
	@SuppressWarnings("unchecked")
	public SortedArraySet(int size,Comparator<E> c) {
		comp=c;
		n=0;
		tab=(E[])new Object[size];
	}
	public SortedArraySet(Comparator<E> c) {
		this(20,c);
	}
	public SortedArraySet(Collection<E> c) {
		
		Iterator<E> it= c.iterator();
		while(it.hasNext()){
			add(it.next());
		}
		
	}
	
	public String toString(){
		return "{"+n+"}[" + arrayToString(tab) + "}";
	}
	
	private String arrayToString(E[] tab){
		String s = "";
		if (tab.length>0)
			s += tab[0];
		for(int i=1; i<n; ++i)
			s += ", " + tab[i];
		return s;
		
	}
	
	@SuppressWarnings("unchecked")
	private int compare(E e1, E e2){
		if(comp != null){
			return comp.compare(e1, e2);
		}
		return ((Comparable<E>)e1).compareTo(e2);
	}
	
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n==0;
	}

	@Override
	public boolean contains(Object o) {
			@SuppressWarnings("unchecked")
			int i = indexOf((E)o);
			return i<n &&tab[i].equals(o);
	}

	@Override
	public Iterator<E> iterator() {
		return new IterateurSet();
	}
	
	private class IterateurSet implements Iterator<E>{

		private int pos=0;
		
		@Override
		public boolean hasNext() {
			return pos < n;
		}

		@Override
		public E next() {
			
			return tab[pos++];
			
		}
		
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	public int indexOf(E e){
		int i=0;
		for(; i<n && compare(tab[i],e)<0 ; i++);
		return i;
	}
	
	public void shiftRight(int idx){
		for(int i=n; i>=idx; i--){
			 tab[i+1] = tab[i];
		}
	}
	
	public void shiftLeft(int idx){
		for(int i=n; i>=idx; i--){
			 tab[i-1] = tab[i];
		}
	}
	
	@Override
	public boolean add(E e) {
		int i = indexOf(e);
		if(i<n &&tab[i].equals(e)){
			return false;
		}
		shiftRight(i);
		n++;
		tab[i]=e;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean remove(Object o) {
		int i = indexOf((E) o);
		if(i<n &&tab[i].equals(o)){
			shiftLeft(i);
			n--;
			return true;
		}
		return false;
		
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		Iterator<?> i = c.iterator();
		while(i.hasNext()){
			if(!this.contains(i.next()))return false;
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		for(E e : c){
			if(!add(e)){
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		n = 0;
		this.tab = (E[]) new Object[DEFAULT_SIZE];
		
	}

	@Override
	public Comparator<? super E> comparator() {
		return comp;
	}

	@Override
	public SortedSet<E> subSet(E fromElement, E toElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SortedSet<E> headSet(E toElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SortedSet<E> tailSet(E fromElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E first() {
		return tab[0];
	}

	@Override
	public E last() {
		return tab[tab.length-1];
	}
}

	
	
