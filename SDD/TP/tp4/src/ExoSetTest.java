import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Test;

public class ExoSetTest {

	@Test
	public void testInter() {
		HashSet<Integer> h1=new HashSet<>();
		HashSet<Integer> h2=new HashSet<>();
		HashSet<Integer> expected=new HashSet<>();
		h1.add(1);
		h1.add(2);
		h2.add(1);
		h2.add(3);
		expected.add(1);
		assertEquals(expected,ExoSet.inter(h1, h2));
		
		
	}
	@Test
	public void testUnion() {
		HashSet<Integer> h1=new HashSet<>();
		HashSet<Integer> h2=new HashSet<>();
		HashSet<Integer> expected=new HashSet<>();
		h1.add(1);
		h1.add(2);
		h2.add(1);
		h2.add(3);
		expected.add(1);
		expected.add(2);
		expected.add(3);
		assertEquals(expected,ExoSet.union(h1, h2));
		
	}
	
	@Test
	public void testIsIn(){
		HashSet<Integer> h1=new HashSet<>();
		HashSet<Integer> h2=new HashSet<>();
		HashSet<Integer> expected=new HashSet<>();
		h1.add(1);
		h2.add(1);
		h2.add(3);
		expected.add(1);
		assertTrue(ExoSet.isIn(h1, h2));
		assertFalse(ExoSet.isIn(h2, h1));
	}
	

}
