

public class testDico {

	public static final int[] tab = {1,2,3,4,5,6,7,8,9,10,11,12,13};
	
	public static int indexOf(int i){
		boolean trouve = false;
		int idxd = 0; 
		int idxi = tab.length/2;
		int idxf = tab.length;
		int res = -1;
		
		while(!trouve){
			idxi = (idxf+idxd)/2;
			
			if(tab[idxi]==i){
				res = idxi;
				trouve = true;
			}
			
			if(tab[idxi]<i){idxd = idxi;}
			else{idxf = idxi;}
			
			if(idxd == idxf) {trouve = true;}
		}
		return res;
	}
	
	public static void main(String[]args){
		System.out.println(indexOf(5));		
	}
	
}
