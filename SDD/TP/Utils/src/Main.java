
public class Main {

	public static void main(String[] args) {
		
		int[] tab = {9,8,7,6,5,4,3,2,1};		
		triSelection t = new triSelection();
		
		afficher(tab);
		t.trier(tab);
		afficher(tab);

	}
	
	private static void afficher(int[]tab){
		if(tab.length == 0){
			return;
		}
		String res = "["+tab[0];
		
		for (int i = 1; i < tab.length; i++) {
			res += ","+tab[i];
		}
		res+= "]";
		System.out.println(res);
	}

}
