import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class triSelection {
	
	public void trier(int[]tab){
		for (int i = 0; i < tab.length; i++) {
			swap(tab,i,searchMin(tab,i));
		}
		
	}
	
	private void swap(int[]tab,int idx1,int idx2){
		int temp = tab[idx1];
		tab[idx1] = tab[idx2];
		tab[idx2] = temp;
	}
	
	private int searchMin(int[]tab,int idxFenetre){
		int res = idxFenetre;
		int minVal = tab[idxFenetre];
		for (int i = idxFenetre+1; i < tab.length; i++) {
			if(tab[i]<minVal){
				res = i;
				minVal = tab[i];
			}
		}
		return res;
	}
	
	public static void main(String[] args) {
		Map<String,Integer> iMap = new HashMap<String, Integer>();
		iMap.put("one", 1);
		iMap.put("two", 2);
		iMap.put(null, 42);
		iMap.put(null, 32);
		Set<Entry<String,Integer>> set = iMap.entrySet();
		System.out.println(set);
		
		
	}

}
