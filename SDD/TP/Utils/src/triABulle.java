
public class triABulle {
	
	
	public void trier(int[] tab){
		boolean trie = false;
		while(!trie){
			for (int i = 0; i < tab.length-1; i++) {
				if(tab[i]>tab[i+1]){
					swap(tab,i,i+1);
				}
			}
			trie = trieOk(tab);
		}
	}
	
	public void swap(int[]tab,int idx1,int idx2){
		int temp = tab[idx1];
		tab[idx1] = tab[idx2];
		tab[idx2] = temp;
	}
	
	private boolean trieOk(int[]tab){
		for (int i = 0; i < tab.length-1; i++) {
			if(tab[i]>tab[i+1]){
				return false;
			}
		}
		return true;
	}

}
