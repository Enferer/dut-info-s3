package tp3;

public class Pile<T> {

	private Object[] tab;
	private int tete = -1;
	
	
	public Pile(int taille){
		tab = new Object[taille];
	}
	
	public void add(T t){
		tete++;
		tab[tete]= t;
		
	}
	
	@SuppressWarnings("unchecked")
	public T pop(){
		tete--;
		return (T)tab[tete+1];
	}
	
	public boolean isEmpty(){
		return tete < 0;
	}
	
	public T peek(){
		return (T)tab[tete];
	}
}
