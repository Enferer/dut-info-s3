package tp3;

import java.util.LinkedList;

public class Parcours {

	
	/*public static void main(String[]args){
		Labyrinthe l = new Labyrinthe();
		Pile<Cellule> p = new Pile<>(5000);
		//Stack<Cellule> p = new Stack<Cellule>();
		p.add(new Cellule(1, 1));
		l.poserMarque(0, 1);
		
		while(!p.isEmpty()){
			
			try {
				Thread.sleep(2) ;
				}
			catch(InterruptedException e){}
			Cellule c = p.peek();
			
			l.poserMarqueRetour(c.getX(), c.getY());
			if(c.getX() == l.n()-1 && c.getY() == l.n()-2){
				System.out.println("chemin trouvé");
				break;
			}
			
			else{
				     if( !(l.estMur(c.getX()+1, c.getY())) && !(l.estMarque(c.getX()+1, c.getY()))    ){
					p.add(new Cellule(c.getX()+1, c.getY()));
					l.poserMarque(c.getX()+1, c.getY());
				
				}
				else if( !(l.estMur(c.getX()-1, c.getY())) && !(l.estMarque(c.getX()-1, c.getY()))    ){
					p.add(new Cellule(c.getX()-1, c.getY()));
					l.poserMarque(c.getX()-1, c.getY());
				
				}
				else if( !(l.estMur(c.getX(), c.getY()+1)) && !(l.estMarque(c.getX(), c.getY()+1))    ){
					p.add(new Cellule(c.getX(), c.getY()+1));
					l.poserMarque(c.getX(), c.getY()+1);
			
				}
				else if( !(l.estMur(c.getX(), c.getY()-1)) && !(l.estMarque(c.getX(), c.getY()-1))    ){
					p.add(new Cellule(c.getX(), c.getY()-1));
					l.poserMarque(c.getX(), c.getY()-1);
					
				}else{
					p.pop();
				}
				
			}

			
		}
		if(p.isEmpty()){
			System.out.println("Aucun chemin trouvé");
		}
		
		

		
	}*/
	
	public static void main(String[]args){
		Labyrinthe l = new Labyrinthe();
		LinkedList<Cellule> p = new LinkedList<>();
		//Stack<Cellule> p = new Stack<Cellule>();
		p.add(new Cellule(1, 1));
		l.poserMarque(0, 1);
		
		while(!p.isEmpty()){
			
			try {
				Thread.sleep(4) ;
				}
			catch(InterruptedException e){}
			Cellule c = p.peek();
			
			l.poserMarqueRetour(c.getX(), c.getY());
			if(c.getX() == l.n()-1 && c.getY() == l.n()-2){
				System.out.println("chemin trouvé");
				break;
			}
			
			else{
				     if( !(l.estMur(c.getX()+1, c.getY())) && !(l.estMarque(c.getX()+1, c.getY()))    ){
					p.add(new Cellule(c.getX()+1, c.getY()));
					l.poserMarque(c.getX()+1, c.getY());
				
				}
				else if( !(l.estMur(c.getX()-1, c.getY())) && !(l.estMarque(c.getX()-1, c.getY()))    ){
					p.add(new Cellule(c.getX()-1, c.getY()));
					l.poserMarque(c.getX()-1, c.getY());
				
				}
				else if( !(l.estMur(c.getX(), c.getY()+1)) && !(l.estMarque(c.getX(), c.getY()+1))    ){
					p.add(new Cellule(c.getX(), c.getY()+1));
					l.poserMarque(c.getX(), c.getY()+1);
			
				}
				else if( !(l.estMur(c.getX(), c.getY()-1)) && !(l.estMarque(c.getX(), c.getY()-1))    ){
					p.add(new Cellule(c.getX(), c.getY()-1));
					l.poserMarque(c.getX(), c.getY()-1);
					
				}else{
					p.pop();
				}
				
			}

			
		}
		if(p.isEmpty()){
			System.out.println("Aucun chemin trouvé");
		}
		
		

		
	}
	
}
