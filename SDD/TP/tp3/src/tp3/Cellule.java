package tp3;

public class Cellule {
	
	int x;
	int y;
	
	public Cellule(int x, int y){
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return "Cellule [x=" + x + ", y=" + y + "]";
	}
	
	
	
	
	

}
