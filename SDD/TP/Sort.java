package tp1;

import java.util.Random;

public class Sort {
	

    public static int [] generateRdmIntArray(int n, int min, int max){
    	Random r = new Random();
		int[] res = new int[n];
		for (int i = 0; i < res.length; i++) {
			res[i] = r.nextInt(max-min)+min;
		}
		return res;
    }

//
//    public static void insertSort(int [] tab) {
//    	
//    	 for (int i = tab.length; i > 0 ; i--) {
//			insert(tab, i);
//		}
//
//    	
//    }
//    
//    
//    
//    private static void insert(int[]tab, int IEnd){
//    	
//    	for (int i = IEnd-1; i > 0 && tab[i] > tab[i-1]; i--) {
//			int temp = tab[i];
//			tab[i] = tab[i-1];
//			tab[i-1] = temp;
//		}
//    }
    
    public static void insertSort(int[]tab){
    	   int i, j;

    	   for (i = 1; i < tab.length; ++i) {

    	       int elem = tab[i];

    	       for (j = i; j > 0 && tab[j-1] > elem; j--){

    	           tab[j] = tab[j-1];
    	       }

    	       tab[j] = elem;

    	   }
    }

    public static void selectSort(int [] tab){
    	for (int i = 0; i < tab.length; i++) {
			swap(tab, i, getIdxMin(tab, i));
		}
    }
    
    private static int getIdxMin(int[] tab,int iBeg){
    	int res = iBeg;
    	for (int i = iBeg+1; i < tab.length; i++) {
			if(tab[res]>tab[i]){
				res = i;
			}
		}
    	return res;
    }
    
    
    
    public static void main(String[]args){
    	int[] tab = {10,9,11,7,6,5,4,3,2,1};
    	//System.out.println(getIdxMin(tab, 0));
    	insertSort(tab);
    	printArray(tab);
    }

    public static void printArray(int [] tab){
    	for (int i = 0; i < tab.length; i++) {
			System.out.print(tab[i]+",");
		}
    }

    public static void swap(int [] tab, int idx, int idx2){
    	int temp = tab[idx];
    	tab[idx] = tab[idx2];
    	tab[idx2] = temp;
//    	tab[idx] = tab[idx]+tab[idx2];
//    	tab[idx2] = tab[idx]-tab[idx2];
//    	tab[idx] = tab[idx]-tab[idx2];
    }

    public static void bubbleSort(int [] tab){
    	boolean tri = true;
    	while(tri){
    		tri = false;
    		for (int i = 0; i < tab.length-1; i++) {
				if(tab[i]>tab[i+1]){
					Sort.swap(tab,i,i+1);
					tri = true;
				}
			}
    	}
    }


}

