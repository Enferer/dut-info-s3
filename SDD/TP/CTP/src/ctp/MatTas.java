package ctp;

import java.util.Comparator;
import java.util.Iterator;


/**
 * CTP SDD
 * Classe qui implement FilePriorite
 * Le nombre d'éléments est connu grace à un attribut interne
 * @author Thibaut Depommier
 * @date 22 Nov 2017
 */

public class MatTas<E> implements FilePriorite<E>{
	
	private static final int TAILLE = 3;
	
	E[][] tab;
	private int indice = 0;
	private Comparator<E> comp = null;
	
	public MatTas() {
		this(MatTas.TAILLE);
	}
	
	@SuppressWarnings("unchecked")
	public MatTas(int taille){
		tab = (E[][]) new Object[taille][taille];
	}
	
	@SuppressWarnings("unchecked")
	public MatTas(int taille,Comparator<E> comp){
		tab = (E[][]) new Object[taille][taille];
		this.comp = comp;
	}
	
	
	public MatTas(FilePriorite<E> f){
		Iterator<E> i = f.iterator();
		while(i.hasNext()){
			this.offer(i.next());
		}
	}


	@Override
	public Iterator<E> iterator() {
		return new IterateurTas();
	}
	
	private class IterateurTas implements Iterator<E>{

		private int idx=0;
		
		@Override
		public boolean hasNext() {
			return tab[idx/tab.length][idx%tab.length] != null;
		}

		@Override
		public E next() {
			E res = tab[idx/tab.length][idx%tab.length];
			idx++;
			return res;
			
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		indice = 0;
		tab = (E[][]) new Object[tab.length][tab.length];
		
	}

	@Override
	public Comparator<E> comparator() {
		return comp;
	}
	
	
	// Permet de comparer les éléments E
	@SuppressWarnings("unchecked")
	private int compare(E e1, E e2){
		if(this.comp!=null)return comp.compare(e1, e2);
		
		else{
			Comparable<E> c1=(Comparable<E>) e1;
			return c1.compareTo(e2);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean offer(Object e) {
		if(isFull()) return false;
		
		int idx = indice;
		
		tab[idx/tab.length][idx%tab.length] = (E) e;
		
		
		// Remonte l'élément à inserer tant que l'élément au dessu est plus petit
		while((idx/tab.length)-1 >= 0 && 
				compare(tab[idx/tab.length][idx%tab.length], tab[idx/(tab.length)-1][idx%tab.length])> 0){
			
			swapUp(idx);
			idx = idx - tab.length;
		
		}
		// Décale l'élément à gauche tant que l'élément a gauche est plus petit
		while((idx%tab.length)-1 >= 0 && 
				compare(tab[idx/tab.length][idx%tab.length], tab[idx/tab.length][(idx%tab.length)-1])> 0){
			
			swapLeft(idx);
			idx--;
		
		}
		this.indice++;
		
		return true;
	}
	
	// échange l'élément à l'indice idx avec celui de gauche
	private void swapLeft(int idx) {
		E temp = tab[idx/tab.length][idx%tab.length];
		tab[idx/tab.length][idx%tab.length] = tab[idx/tab.length][(idx%tab.length)-1];
		tab[idx/tab.length][(idx%tab.length)-1] = temp;
		
	}

	// Permet de remonter un élément en l'échangeant avec celui au-dessu de lui
	private void swapUp(int idx){
		E temp = tab[idx/tab.length][idx%tab.length];
		tab[idx/tab.length][idx%tab.length] = tab[idx/(tab.length)-1][idx%tab.length];
		tab[idx/(tab.length)-1][idx%tab.length] = temp;
		
	}

	
	// Renvoie si true si le tas est plein 
	private boolean isFull(){
		return indice > tab.length*2;
	}

	@Override
	public E peek() {
		if(this.isEmpty())
			return null;
		
		return tab[0][0];
	}

	@Override
	public E poll() {
		
		if(isEmpty()) return null;
		
		E res;
		
		if(size()==1){
			res = peek();
			clear();
			return res;
		}
		
		res = peek();
		
		tab[0][0] = tab[(indice-1)/tab.length][(indice-1)%tab.length];
		tab[indice/tab.length][indice%tab.length] = null;
		indice --;
		int idx = 0;

		// Décale l'élément à droite tant que l'élément a droite est plus grand
		while(idx+1 < tab.length && tab[0][idx+1] != null && 
				compare(tab[0][idx], tab[0][idx+1])< 0){
			swapRight(idx);
			idx++;
		
		}
		// Descend l'élément tant que l'élément en dessou est plus grand
		while((idx+1)/tab.length < tab.length && tab[(idx+tab.length)/tab.length][idx%tab.length] != null &&
				compare(tab[idx/tab.length][idx%tab.length], tab[(idx+tab.length)/tab.length][idx%tab.length])< 0){
			swapDown(idx);
			idx+=tab.length;
		
		}

		return res;
	}

	// échange l'élément à l'indice idx avec celui en dessou
	private void swapDown(int idx) {
		E tmp = tab[idx/tab.length][idx%tab.length];
		tab[idx/tab.length][idx%tab.length] = tab[(idx/tab.length)+1][idx%tab.length];
		tab[(idx/tab.length)+1][idx%tab.length] = tmp;
	}
	
	
	// échange l'élément à l'indice idx avec celui de droite
	private void swapRight(int idx) {
		E tmp = tab[idx/tab.length][idx%tab.length];
		tab[idx/tab.length][idx%tab.length] = tab[(idx/tab.length)][(idx%tab.length)+1];
		tab[idx/tab.length][(idx%tab.length)+1] = tmp;
		
	}

	@Override
	public int size() {
		return indice;
	}

	@Override
	public boolean isEmpty() {
		return indice == 0; 
		
	}

}
