import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;

import ctp.FilePriorite;
import ctp.MatTas;

/**
 * Tests for a priority queue
 * @author <a href="mailto:Frederic.Guyomarch@univ-lille1.fr">Frédéric Guyomarch</a>, IUT-A
 * @date 2017 November 20th
 */

public class MatTasTest {
	@Test
	public void testIsEmpty() {
		FilePriorite<String> queue = new MatTas<>();
		assertTrue(queue.isEmpty());
		queue.offer("food");
		assertFalse(queue.isEmpty());

	}

	@Test
	public void testSize() {
		FilePriorite<String> queue = new MatTas<>();
		assertEquals(0, queue.size());
		queue.offer("food");
		assertEquals(1, queue.size());
		queue.offer("apple");
		assertEquals(2, queue.size());
		queue.offer("spork");
		assertEquals(3, queue.size());
	}

	@Test
	public void singleItemTest() {
		FilePriorite<String> queue = new MatTas<>();
		queue.offer("kiwi");
		assertEquals("kiwi", queue.peek());
		assertEquals(1, queue.size());
		assertFalse(queue.isEmpty());
	}

	@Test
	public void  peekTest() {
		FilePriorite<String> queue = new MatTas<>();
		queue.offer("boo");
		queue.offer("achoo");
		assertEquals("boo", queue.peek());
		assertEquals("boo", queue.peek());
	}

	@Test
	public void fiveItemsTest() {
		FilePriorite<Integer> queue = new MatTas<>();
		queue.offer(3);
		queue.offer(1);
		queue.offer(7);
		queue.offer(5);
		queue.offer(4);

		assertFalse(queue.isEmpty());
		assertEquals(5, queue.size());
		assertEquals(new Integer(7), queue.peek());
	}
	
	@Test
	public void pollPeekTest() {
		FilePriorite<String> queue = new MatTas<>();
		queue.offer("kiwi");
		queue.offer("apple");
		queue.offer("orange");

		assertEquals("orange", queue.poll());
		assertEquals(2, queue.size());

		assertEquals("kiwi", queue.peek());
		assertEquals("kiwi", queue.poll());
		assertEquals(1, queue.size());

		assertEquals("apple", queue.poll());
		assertTrue(queue.isEmpty());
		assertEquals(0, queue.size());
	}

	@Test
	public void emptyPollTest() {
		FilePriorite<String> queue = new MatTas<>();
		assertNull(queue.poll());
	}

	@Test
	public void iteratorTest() {
		FilePriorite<String> queue = new MatTas<>();
		queue.offer("kiwi");
		queue.offer("apple");
		queue.offer("orange");

		String[] expected = { "orange", "kiwi", "apple" };
		int i = 0;

		for (String fruit : queue) {
			assertEquals(expected[i++], fruit);
		}
	}

	@Test
	public void iteratorListTest() {
		FilePriorite<String> queue = new MatTas<>();
		queue.offer("apples");
		queue.offer("kiwi");
		queue.offer("banana");

		ArrayList<String> fruit1 = new ArrayList<>();
		fruit1.add("kiwi");
		fruit1.add("banana");
		fruit1.add("apples");

		ArrayList<String> fruit2 = new ArrayList<>();
		for (String fruit : queue) {
			fruit2.add(fruit);
		}

		assertEquals(fruit1, fruit2);
	}
	
	@Test
	public void iteratorIntTest() {
		FilePriorite<Integer> queue = new MatTas<>();
		int testSum = 0;
		queue.offer(1);
		queue.offer(2);
		queue.offer(3);

		for (int element : queue) {
			testSum = testSum + element;
		}
		assertEquals(6, testSum);
	}
}