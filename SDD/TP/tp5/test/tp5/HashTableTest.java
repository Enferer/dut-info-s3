package tp5;

import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;



public class HashTableTest {
	
	//HashTable h = new HashTableChainage();
	HashTable h = new HashTableAdressage();
	
	
	@Before
	public void setUp() {
		h = new HashTableChainage();
	}
	
	@Test
	public void testSize(){
		h.put("Un", 1);
		h.put("Dos", 2);
		h.put("Tres", 3);
		
		assertEquals(h.size(),3);
	}
	
	@Test
	public void testPut(){
		h.put("Un", 1);
		h.put("Dos", 2);
		h.put("Tres", 3);
		
		assertEquals(h.contains("Un"),true);
	}
	
	
	@Test
	public void testGet(){
		h.put("Un", 1);
		h.put("Dos", 2);
		h.put("Tres", 3);
		
		assertEquals(h.get("Un"),1);
	}
	
	
	@Test
	public void testRemove(){
		h.put("Un", 1);
		h.put("Dos", 2);
		h.put("Tres", 3);
		
		h.remove("Un");
		
		assertEquals(h.contains("Un"),false);
	}
	
	
	@Test
	public void testContains(){
		h.put("Un", 1);
		h.put("Dos", 2);
		h.put("Tres", 3);
		System.out.println(h);
		h.remove("Un");
		
		assertEquals(h.contains("Un"),false);
		assertEquals(h.contains("Dos"),true);
	}

}
