package tp5;


public class HashTableAdressage implements HashTable{
	
	protected HashCouple[] table;
	private final int SIZE = 50 ;
	int cpt = 0;
	
	public HashTableAdressage(){
		table = new HashCouple[SIZE];
	}

	@Override
	public int put(String key, int value) {
		HashCouple e = new HashCouple(key, value);
		if(idxOf(key)==-1) return -1;
		table[idxOf(key)] = e;
		cpt++;
		return idxOf(key);
	}

	@Override
	public int get(String key) {
		if(table[idxOf(key)].getValue() != null){
			return table[idxOf(key)].getValue();
		}
		return -1;
	}

	@Override
	public boolean remove(String key) {
		if(table[idxOf(key)]!=null){
			table[idxOf(key)] = null;
			cpt--;
			return true;
		}
		return false;
	}

	@Override
	public boolean contains(String key) {
		if(table[idxOf(key)].getValue()==null){
			return false;
		}
		return true;
	}

	@Override
	public int size() {
		return cpt;
	}
	
	private int idxOf(String key){
		int hashcode = key.hashCode();
		while(!table[hashcode].getKey().equals(key) && table[hashcode]!= null){
			hashcode++;
			hashcode = hashcode%SIZE;
		}
		if(table[hashcode] == null) return -1;
		return hashcode;
	}

}
