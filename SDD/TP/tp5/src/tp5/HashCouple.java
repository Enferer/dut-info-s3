package tp5;

import java.util.Map.Entry;

public class HashCouple implements Entry<String,Integer>{
	
	@Override
	public String toString() {
		return "HashCouple [value=" + value + ", key=" + key + "]";
	}

	private Integer value;
	private String key;
	
	
	public HashCouple(String key,Integer value){
		this.value = value;
		this.key = key;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public Integer getValue() {
		return value;
	}

	@Override
	public Integer setValue(Integer value) {
		Integer res = value;
		this.value = value;
		return res;
		
	}
	
	public int hashCode(){
		return key.hashCode();
	}

	
	

}
