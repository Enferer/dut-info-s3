package tp5;

import java.util.LinkedList;

public class HashTableChainage implements HashTable{
	
	private final static int DEFAULT_SIZE = 16;
	protected LinkedList<HashCouple>[] table;
	private int cpt = 0;
	
	
	
	@SuppressWarnings("unchecked")
	public HashTableChainage() {
		table = (LinkedList<HashCouple>[]) new LinkedList[DEFAULT_SIZE]; 
		for (int i = 0; i < table.length; i++) {
			table[i] = new LinkedList<>();
		}
	}

	@Override
	public int put(String key, int value) {
		HashCouple h = new HashCouple(key,value);
		table[h.hashCode()%DEFAULT_SIZE].add(h);
		this.cpt++;
		return 1;
	}

	@Override
	public int get(String key) {
		for (HashCouple h : table[key.hashCode()%DEFAULT_SIZE]) {
			if(h.getKey().equals(key)){
				return h.getValue();
			}
		}
		return -1;
	}

	@Override
	public boolean remove(String key) {
		for (HashCouple h : table[key.hashCode()%DEFAULT_SIZE]) {
			if(h.getKey().equals(key)){
				table[key.hashCode()%DEFAULT_SIZE].remove(h);
				this.cpt--;
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean contains(String key) {
		for (HashCouple h : table[key.hashCode()%DEFAULT_SIZE]) {
			if(h.getKey().equals(key)){
				return true;
			}
		}
		return false;
	}

	@Override
	public int size() {
		return cpt;
	}
	
	public String toString(){
		String res = "[";
		for (int i = 0; i < table.length; i++) {
			for (HashCouple h : table[i]) {
				res+=h.toString()+",";
			}
		}
		res = res.substring(0, res.length()-1);
		res += "]";
		return res;
	}
	
	

}
