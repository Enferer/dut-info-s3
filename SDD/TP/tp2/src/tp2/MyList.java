package tp2;

public class MyList<T> {

	private Node<T> first;
	private Node<T> last;
	
	
	public boolean add(T t){
		first = new Node<>(t,first);
		return true;
	}
	
	public String toString(){
		String res = "[";
		Node<T> temp = first;
		if(temp != null){
			res += temp.getElement().toString();
		}
		temp = temp.getNext();
		while(temp != null){
			res += ","+temp.getElement().toString();
		}
		
		return res+"]";
	}
	
}
