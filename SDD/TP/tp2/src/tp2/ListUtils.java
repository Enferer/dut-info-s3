package tp2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class ListUtils{

	public List<Integer> genereRdmIntList(){
		List<Integer> l = new ArrayList<Integer>();
		int i = new Random().nextInt(30)+1;
		for(;i>0;i--){
			l.add(new Random().nextInt(100)+1);
		}
		return l;
	}
	
	public void affiche(List<Integer> l){
		Iterator<Integer> i = l.iterator();
		if(i.hasNext()){
			System.out.print(i.next());
		}
		while(i.hasNext()){
			System.out.print(" -> "+i.next());
		}
		System.out.println("");
	}
	
	public void afficheInverse(List<Integer> l){
		ListIterator<Integer> i = l.listIterator(l.size());
		if(i.hasPrevious()){
			System.out.print(i.previous());
		}
		while(i.hasPrevious()){
			System.out.print(" -> "+i.previous());
		}
		System.out.println("");
	}
	
	public int somme(List<Integer> l){
		int res = 0;
		Iterator<Integer> i = l.iterator();
		
		while(i.hasNext()){
			res += i.next();
		}
		return res;
	}
	
	public int moyenne(List<Integer> l){
		return (int)somme(l)/l.size();
	}
	
	public int max(List<Integer> l){

		Iterator<Integer> i = l.iterator();
		int res = -1;
		
		while(i.hasNext()){
			int temp = i.next();
			if(temp>res){
				res = temp;
			}
		}
		return res;
	}
	
	public int min(List<Integer> l){
		int res = Integer.MAX_VALUE;
		Iterator<Integer> i = l.iterator();
		if(i.hasNext()){
			res = i.next();
		}

		while(i.hasNext()){
			int temp = i.next();
			if(temp<res){
				res = temp;
			}
		}
		return res;
	}
	
	public List<Integer> positions(List<Integer> l, int n){
		List<Integer> res = new ArrayList<>();
		Iterator<Integer> i = l.iterator();
		int cpt = 0;
		while(i.hasNext()){
			if(i.next() == n){
				res.add(cpt);
			}
			cpt++;
		}
		return res;
	}
	
	public List<Integer> paire(List<Integer> l){
		List<Integer> res = new ArrayList<>();
		Iterator<Integer> i = l.iterator();
		while(i.hasNext()){
			int temp = i.next();
			if(temp%2 == 0){
				res.add(temp);
			}
		}
		return res;
		
	}
	public boolean estTrie(List<Integer> l){

		int valPrec=Integer.MIN_VALUE;
		Iterator<Integer> i=l.iterator();
		while(i.hasNext()){
			int val=i.next();
			 if(val<valPrec){
				 return false;
			 }
			 valPrec=val;
		}
		return true;

	}
	
	public List<Integer> trie(List<Integer> l){
		
		List<Integer> copie=new ArrayList<Integer>();
		for(Integer i:l){
			copie.add(i);
		}
		int idx=0;
		while(!copie.isEmpty()){
			l.set(idx, min(copie));
			copie.remove(copie.indexOf(min(copie)));
			idx++;
		}
		return l;
	}



}
