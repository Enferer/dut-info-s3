package tp6;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Set;

public class TestMap {

	
	HashMap<String, Integer> map = new HashMap<>();

	public void initialiser(String url){
		String text = getStringUrl(url); 
		String[] tab = text.split("!|;|,|-|’|\\(|\\)|\\ |\\.|—|\\?|\\!");
		for (int i = 0; i < tab.length; i++) {
				insert(tab[i]);			
		}

		
		HashMap<String, Integer> temp = (HashMap<String, Integer>) map.clone();
		Set<String> set = map.keySet();
		temp.put("---", -1);
		map.put("---", -1);
		String[] freq = new String[6];
		for (int i = 0; i < freq.length; i++) {
			String current = "---"; 
			for (String s : set) {

				if(temp.get(s)> temp.get(current)){
					current = s;
				}
			}
			set = temp.keySet();
			freq[i] = current;
			temp.remove(current);
			
		}
		for (int i = 1; i < freq.length; i++) {
			System.out.println(freq[i]+" : "+map.get(freq[i]));
		}

		
	}

	
	
	private void insert(String mot) {
		
		if(this.map.containsKey(mot)){
			this.map.replace(mot, this.map.get(mot)+1);
		}
		else{
			this.map.put(mot, 1);
		}
		
	}



	private String getStringUrl(String url){
		String res = "";
		String ligne;
		FileReader flux;
		BufferedReader fichier;

		try{
			flux=new FileReader(url); 
			fichier=new BufferedReader(flux);
			while ((ligne=fichier.readLine())!=null){
				res += ligne + " ";
			}
			flux.close();
			fichier.close(); 
		}
		catch (Exception e){
			System.err.println("Erreur ==> "+e.toString());
		}
		return res;
		
	}
	
	public static void main(String[]args){
		TestMap t = new TestMap();
		t.initialiser("ressource/VICTOR_HUGO-Notre_dame_de_paris.txt");
		//t.initialiser("ressource/bis");
	}
	
	
}
