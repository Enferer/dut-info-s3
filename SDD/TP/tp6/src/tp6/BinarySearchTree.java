
package tp6;

import java.util.List;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;

public class BinarySearchTree<K extends Comparable<K>,V> implements Map<K,V>{
	
	K key = null;
	V value = null;
	BinarySearchTree<K, V> left = null;
	BinarySearchTree<K, V> right = null;
	
	
	public BinarySearchTree(){}
	
	public BinarySearchTree(K key,V value) {
		this.key = key;
		this.value = value;
		
		this.left = new BinarySearchTree<>();
		this.right = new BinarySearchTree<>();
	}
	
	public BinarySearchTree(K key, V value, BinarySearchTree<K, V> left, BinarySearchTree<K, V> right) {
		super();
		this.key = key;
		this.value = value;
		this.left = left;
		this.right = right;
	}


	@Override
	public int size() {
		if(isEmpty()) 
			return 0;
		return 1 + left.size() + right.size();
	}

	@Override
	public boolean isEmpty() {
		return key == null;
	}

	@Override
	public boolean containsKey(Object key) {
		if(this.key == key){
			return true;
		}else if(this.isEmpty()){
			return false;
		}
		return this.left.containsKey(key)||this.right.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		if(this.value == value){
			return true;
		}else if(this.isEmpty()){
			return false;
		}
		return this.left.containsValue(value)||this.right.containsValue(value);
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(Object key) {
		return search((K)key).value;
	}

	@Override
	public V put(K key, V value) {
		if(this.key == null){
			this.key = key;
			this.value = value;
			this.left = new BinarySearchTree<>();
			this.right = new BinarySearchTree<>();
			return value;
		}else if(key.compareTo(this.key)>0){
			return this.right.put(key, value);	
		}else if(key.compareTo(this.key)<0){
			return this.left.put(key, value);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V remove(Object key) {
		BinarySearchTree<K, V> t = search((K)key);
		if(this.isEmpty()) return null;
		V tmp = t.value;
		if(t.right.isEmpty()){
			t.copy(t.left);
			return (V) t;
		}
		SimpleEntry<K, V> sc = t.right.deleteMin();
		t.key = sc.getKey();
		t.value = sc.getValue();
		return tmp;
	}
	
	public BinarySearchTree<K,V> min(){
		if(this.left.value == null)return this;
		return this.left.min();
	}
	
	public SimpleEntry<K, V> deleteMin(){
		BinarySearchTree<K,V> min = min();
		SimpleEntry<K, V> res = new SimpleEntry<K, V>(min.key, min.value);
		min.copy(min.right);
		return res;
	}
	
	public void copy(BinarySearchTree<K, V> bst){
		this.key=bst.key;
		this.value=bst.value;
		this.left=bst.left;
		this.right=bst.right;
	}



	
	//  ############## IMPORTANT ############# 
	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		Set<? extends K> s = m.keySet();
		for (K k : s) {
			this.put(k, m.get(k));
		}
	}
	//  ############## IMPORTANT ############# 
	

	@Override
	public void clear() {
		this.left = null;
		this.right = null;
		
	}

	@Override
	public Set<K> keySet() {
		Set<K> res = new  HashSet<K>();
		if(left == null && right == null) return res;
		else{
			if(left != null) res.addAll(left.keySet());
			if(left != null) res.addAll(right.keySet());
		}
		return res;
	}

	@Override
	public Collection<V> values() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		// TODO
		return null;
	}

	public List<Map.Entry<K, V>> infixe(){
		List<Map.Entry<K,V>> list = new ArrayList<>();
		
		if(left != null)list.addAll(left.infixe());
		list.add(new AbstractMap.SimpleEntry<K, V>(key, value));
		if(right != null)list.addAll(right.infixe());
		
		return list;
	}
	
	private BinarySearchTree<K,V> search(K key){
		if(this.isEmpty() || key.equals(this.key)) return this;
		if(key.compareTo(this.key) <= 0) return left.search(key);
		return right.search(key);
	}
	
	@Override
	public String toString() {return infixe().toString();}

	
	public static void main(String[] args) {
		BinarySearchTree<Integer, String> m = new BinarySearchTree<>();
		m.put(1, "Un");
		m.put(2, "Deux");
		m.put(3, "Trois");
		m.put(4, "Quatre");
		m.put(5, "Cinq");
		m.put(6, "Six");
		m.put(7, "Sept");
		m.put(8, "Huit");
		m.put(9, "Neuf");
		m.put(10, "Dix");
		System.out.println("toString de m : ");
		System.out.println(m);
		System.out.println("Test de size : "+m.size());
		System.out.println("Value de search de Un : "+m.search(1).value);
		m.remove(1);
		m.remove(2);
		m.remove(7);
		m.remove(4);
		
		System.out.println(m);
		
	}
}
