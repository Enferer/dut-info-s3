package tp7;

import java.util.Comparator;

public class FilePrioriteTas<T> implements FilePriorite<T>{
	
	protected final Comparator<? super T> comp;
	T[] tab;
	int idx = 0;

	public FilePrioriteTas() {this(20,null);}

	public FilePrioriteTas(int i){this(i,null);}
	
	@SuppressWarnings("unchecked")
	public FilePrioriteTas(int i,Comparator<T> comp){
		tab = (T[])new Object[i];
		comp = null;
		this.comp = comp;
	}

	@Override
	public void clear() {idx = 0;}

	@Override
	public Comparator<? super T> comparator() {
		
		return null;
	}

	@Override
	public boolean offer(T e) {
		if (this.idx == tab.length) {return false;}
		int idxe = idx;
		tab[idx] = e;
		idx++;
		
		if(tab[(idxe-1)/2] == null) {
			return true;
		}
		
		while(compare(tab[(idxe-1)/2], tab[idxe]) < 0){
			swap(idxe,(idxe-1)/2);
			idxe = (idxe-1)/2;
			
		}
		
		return true;
	}

	private void swap(int idxe, int i) {
		T temp = tab[idxe];
		tab[idxe] = tab[i];
		tab[i] = temp;
	}
	
	@Override
	public T peek() {
		return tab[0];
	}

	@Override
	public T poll() {
		T res = tab[0];
		
		tab[0] = tab[idx];
		idx--;
		int idxe = 0;
		while(tab[(2*idxe)+1]!=null){
			swap(idxe, getIdxMax(idxe));
			idxe = getIdxMax(idxe);
		}
		return res;
		
	}



	private int getIdxMax(int idx) {
		if(tab[(2*idx) + 2] == null) return (2*idx)+1;
		if(compare(tab[(2*idx) + 2], tab[(2*idx)+1])>0) return (2*idx) + 2;
		else return (2*idx) + 1;
		
	}

	@Override
	public int size() {
		return idx;
	}

	@Override
	public boolean isEmpty() {
		return idx == 0;
	}
	
	private int compare(T e1, T e2){
		if(this.comp!=null)return comp.compare(e1, e2);
		
		else{
			Comparable<T> c1=(Comparable<T>) e1;
			return c1.compareTo(e2);
		}
	}
	
	public String toString(){
		String res = "[";
		for (int i = 0; i < tab.length && tab[i]!=null; i++) {
			res += tab[i]+", ";
		}
		res = res.substring(0,res.length()-2);
		res += "]";
		return res;
	}

	
}
