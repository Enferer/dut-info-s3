class main extends Program{
 	
 	int xp = 0;
   	void algorithm(){

   		clearScreen();
   		cursor(1,1);


    		int fini = 0;
    		while(!(fini == 1)){

			
			clearScreen();
			cursor(1,1);
			println("Vous avez : " + xp + " xp ");
			println("");
			println("Choisi un jeu : ");
			println("");
			println("1. Pendu");
			println("2. Re-fait le mot");
			println("3. Operations à trous");
			println("4. Quizz");
			//println("5. mo-mo-motus");
			//println("6. La roue de la fortune");

			println("5. Quitter le jeu");
			print("Entre le numero du jeu : ");
			char choixDujeu = readChar();

				switch(choixDujeu){				//Dectection du choix du jeu
				case '1':
					//playSound("son.mp3");
					pendu();
					break;
				case '2':
					//playSound("son.mp3");
					break;
				case '3':
					//playSound("son.mp3");
					break;
				case '4':
					//playSound("son.mp3");
					quiz();					
					break;
				case '5':
					fini = 1;
					break;
				default:
					println("reponse invalide");
					break;
			}
		}
	
		println("Merci d'avoir joué");
	}




	//  			*****ENSEMBLE QUI GÉRE LE QUIZ*****



	void quiz(){
		boolean quizpasfini = true;
		while(quizpasfini){
			int bonnereponse = 0;
			int nombresessai = 0;
			while(bonnereponse < 4 && nombresessai <4){							//Boucle qui s'arréte quand on quitte le quiz
				clearScreen();
				cursor(1,1);
				println("");
				int idQuestion = (int) (random()*2);
				println("Nombre de bonne réponse : " + bonnereponse + "/4 ( Il te faut minimum 3 bonnes réponses pour gagner 500 points )");
				String question = questionaleatoire(idQuestion);
				println(question);
				String propositions = propositions(idQuestion);
				println(propositions);
				int reponse = reponses(idQuestion);
				int reponseUtilisateur = readInt();
				
				while(!(reponseUtilisateur == 1 || reponseUtilisateur == 2 || reponseUtilisateur == 3)){
					println("reponse invalide");
					reponseUtilisateur = readInt();
				}
				nombresessai++;

				if(reponseUtilisateur == reponse){
					bonnereponse++;
				}
			}

			if (bonnereponse >= 3) {
				clearScreen();
				cursor(1,1);
				println("Bien joué");
				println("Tu as répondu à" + bonnereponse +"/4 !");
				println(" +500 xp");				
				xp = xp + 500;
			}
			else{
				clearScreen();
				cursor(1,1);
				println("Tes nul !");
				println("Tu as répondu à" + bonnereponse +"/4 !");			
			}
				println("Veux tu continuer ? (oui / non)");			// Demande si l'utilisateur veut quitter ou pas
				
				quizpasfini = equals(readString(), "oui");
		}
		
		
		
	}
		
  	  
    		String questionaleatoire(int id){							//Banque de question
			
    		 	String question[] = {
    		 		"Quelle est la couleur du cheval blanc d'henri 4",
    		 		"Quelle est la couleur du cheval rouge d'henri 5"
    		 	};
    		 	return question[id];
 		 }
   		

   		String propositions(int id){							// Banque de proposition
			String propositions[] = {
    		 		" 1. blanc \n 2. rouge \n 3. noir",
    		 		" 1. blanc \n 2. rouge \n 3. noir"
    		 	};
    		 	return propositions[id];
   		 }
          		
		int reponses(int id){								// réponse à la question
  			int reponses[] = {
    		 		1,
    		 		2
    		 	};
    		 	return reponses[id];  	   		
    		}

//			***** FIN DU QUIZZ *****
   
//			***** DÉBUT DU PENDU *****
	void pendu(){
		
		boolean pendupasfini = true;						// si le jeu est fini pendupasfini = false

		while(pendupasfini){
			
			clearScreen();
			cursor(1,1);

			char[] lettrespropose;
			char[] visualisation;						// Tableau de ce qu'on va affiché à l'écran
			char[] lettres;	
			char[] lettresutilise;
			lettresutilise = new char[26];						// Tableau qui contient le mot à trouvé lettre par lettre
			
			boolean victoire = false;						
			boolean saisi = true;									// quand le joueur a gagné ou n'a plus d'essaie saisi = false
			
			int nombresessai = 10;		
			int idmot = (int) (random()*2);
			int nbrlettrestrouve = 0;				// Tirage au sort du mot
			
			String mot = motauhasard(idmot);

			println("Le mot comporte :" + length(mot) + " lettres");
			lettres = new char [length(mot)] ;				
			visualisation = new char [length(mot)] ;
			for(int cpt = 0; cpt < length(mot); cpt++){			// On rentre toutes les lettres dans le tableau
				lettres[cpt]=charAt(mot,cpt);
			}



			while(nombresessai > 0 && !victoire){				
				for(int cpt = 0; cpt < length(mot); cpt++){		// On remplie la chaine de visualisation 
					visualisation[cpt]='_';
					
				}
				
				while(saisi && nombresessai > 0){
					clearScreen();
					cursor(1,1);
					
					boolean lettrebonne = false;
					
					println("Il te reste " + nombresessai + " essai");	

					for(int cpt = 0; cpt < length(mot); cpt++){ 	// On affiche la chaine de visualisation
						print((visualisation[cpt])+" ");
					}
					
					println(" ");
					println("Proposez une lettre");			// On demande à l'utilisatateur de faire un proposition
						char proposition = readChar();

						while(!( proposition <= 48 || proposition >= 57)){
							println("Vous ne pouvez entrer que des lettres");
							proposition = readChar();
						}

					for (int cpt = 0 ; cpt  < length(mot) ; cpt++ ) {	// Si le caractére est dans le mot on le remplace dans le tableau visualisation
						if (proposition == lettres[cpt]) {
						visualisation[cpt] = proposition;
						lettrebonne = true;
						nbrlettrestrouve++;

					 	}					 
					}
					if (!lettrebonne) {
						nombresessai--;
					}
					if (nbrlettrestrouve == length(mot)) {			//On regarde si le joueurs a gagner
						saisi = false;
						victoire = true;
						xp = xp + 500;
						clearScreen();
						cursor(1,1);						
						println("Bien joué tu as gagné !");
						String phraseaafficher = "Le mot était : " + mot;
						println(" +500 xp\n");
						afficherMot(phraseaafficher);
					}
					else if( nombresessai == 0){
						println("Tu as perdu");
					}
				}
			}	 
			

			println(" ");
			println("Veux-tu rejouer au pendu ? (oui / non)");			// Demande si l'utilisateur veut quitter ou pas
			pendupasfini = equals(readString(), "oui");

		}
	}

	String motauhasard(int idMot){						// Donné de mot
		String [] mot = {
			"maisons",
			"tunnel"
		};
		return mot[idMot];
	}

	void afficherMot(String phrase){
		char [] caracteredephrase;						// On rentre la phrase dans un tableau caractéres pas caractére.
		caracteredephrase = new char[length(phrase)];
		for (int cpt = 0;cpt < length(phrase) ;cpt++ ) {
			caracteredephrase[cpt]=charAt(phrase,cpt);
		}
		int tailleh = length(phrase)+6;						// Initialisation de la taille de "cadre".
		int taillev = 5;
		for (int i = 0; i < taillev; i++ ) {
			for (int j = 0; j <tailleh ; j++ ) {
				if (i == 2) {
					print("#  ");							// à la 3 ligne on affiche la phrase.
					for (int cpt = 0;cpt <length(caracteredephrase) ;cpt++ ) {
						print(caracteredephrase[cpt]);
					}
					println("  #");
					i++;
					j = -1;
				}
				else if(j == 0 || i == 0){							//affichage du cadre
					print('#');
				}
				else if (j == tailleh-1 || i == taillev-1) {
					print('#');
				}
				
				else{
					print(' ');
				}	
			}
			print("\n");						
		}
		println(" ");
	}
//			*****FIN DU PENDU*****
}


 
	     
