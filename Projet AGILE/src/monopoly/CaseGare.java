package monopoly;

import java.util.Scanner;

public class CaseGare extends Case {


	private int prix = 200;
	private boolean libre;
	private int loyer = 25;
	private int proprietaire;

	public CaseGare(String nom) {
		this.setNom(nom);
		libre = true;
		proprietaire = 0;

	}

	public void acheter(Pion p){
		if(p.getArgent()>=this.prix && libre){
			//texte qui demande d'acheter
			Affichage.afficher(this.getNom()+ ":Voulez vous acheter ?(Si oui ,taper <<o>> )");
			//System.out.print(this.getNom()+ ":Voulez vous acheter ?(Si oui ,taper <<o>> )");
			Scanner sc = new Scanner(System.in);
			String rep = sc.nextLine();
			if(rep.equals("o") || rep.equals("O")){
				p.setArgent(p.getArgent()-this.prix); 
				//texte qui confirme l'achat
				Affichage.afficher("Achat effectue");
				setProprietaire(p.getEquipe());
				p.getProp().add(this);
				libre=false;
				
			}
			else{
				//texte qui confirme le non achat
				Affichage.afficher("Achat annule");
			}
			
		}
		else if(p.getArgent()<this.prix){
			
			//texte qui dit que le joueur n'a pas assez d'argent
			Affichage.afficher(this.getNom()+ "Argent insuffisant");
		}
		try {
			Thread.sleep(800);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void payer(Pion p1, Pion p2){
		//message qui dit le loyer à payer
		if(p1.peutPayer(this.loyer)){
			p1.setArgent(p1.getArgent()-(this.loyer * p2.getNbGares()));
			p2.setArgent(p2.getArgent()+(this.loyer * p2.getNbGares()));
		}
		else{
			//perdu pour le moment
		}

	}
	public void action(Pion p1, Pion p2) {
		if(libre == true){
			acheter(p1);
		}else{
			if(getProprietaire() == p1.getEquipe()){
				// vous etes chez vous.
				Affichage.afficher(this.getNom()+" :Vous etes sur la Gare de votre equipe");

			}else{
				Affichage.afficher(this.getNom()+" :vous devez payer pour le joueur adverse la somme:"+(this.loyer * p2.getNbGares()) );
				payer(p1,p2);
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub

	}


	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	public int getLoyer() {
		return loyer;
	}

	public void setLoyer(int loyer) {
		this.loyer = loyer;
	}

	public int getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(int proprietaire) {
		this.proprietaire = proprietaire;
	}



}
