package monopoly;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Pion {
	private int equipe;
	private int argent = 1500;
	private int indice;	
	private int nbGares;
	private int nbComp;
	private boolean libre;
	private boolean cartePrison;
	private List<Case> prop = new ArrayList<>();
	private int[] quartiers = new int[8];
	
	public List<Case> getProp() {
		return prop;
	}

	public void setProp(List<Case> prop) {
		this.prop = prop;
	}

	public int getNbGares() {
		return nbGares;
	}

	public void setNbGares(int nbGares) {
		this.nbGares = nbGares;
	}

	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}

	public Pion(int equipe) {
		super();
		this.equipe = equipe;
		this.libre = true;
		this.setCartePrison(false);
	}
	
	public int getEquipe() {
		return equipe;
	}

	public int getArgent() {
		return argent;
	}

	public void setArgent(int argent) {
		this.argent = argent;
	}
	
	public void deplacer(int entier,Plateau p,boolean chance) throws InterruptedException{
		int reste=0;
		if (libre){
			
		try{
		if (this.getEquipe()==1) {p.getCase(this.indice).setP1(null);}
		else {p.getCase(this.indice).setP2(null);}
		}catch(NullPointerException BOUM){}
		if (indice+entier>=40){
			reste=indice+entier-40;
			Affichage.afficher("Bonus de passage de depart");
			Scanner sc = new Scanner(System.in);
			sc.nextLine();

			indice=0;
			argent+=1000;
			entier=reste;
			Thread.sleep(1000);
		}
		
		indice+=entier;
		if(indice==30){
			Affichage.afficher("Allez en prison :Pas de deplacement jusqu'a la sortie ");
			this.setIndice(10);
			this.setLibre(false);
			Thread.sleep(1000);
		}
		try{
		if (this.getEquipe()==1) {p.getCase(this.indice).setP1(this);}
		else {p.getCase(this.indice).setP2(this);}
		}catch(NullPointerException BOUM){}
		}
		if (!chance){
			p.activerCase(this);
		}
	}
	
	
	
	
	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	@Override
	public String toString() {
		if(this.getEquipe()==1){
			return "A";
		}
		else{
			return "B";
		}
	}
	
	public boolean peutPayer(int i){
		if(this.getArgent()-i < 0){
			return false;
		}
		else{
			return true;
		}
	}

	public static void main(String[] args) throws InterruptedException{
		Plateau miam=new Plateau();
		Pion piou=new Pion(0);
		Pion wouaf=new Pion(1);
		for (int x=0;x<30;x++){
			System.out.println("Tour j1");
			piou.deplacer(6,miam,false);
			System.out.print("arrive a "+ piou.getIndice() +" ");
			System.out.println("Tour j2");
			wouaf.deplacer(6,miam,false);
			System.out.print("arrive a "+ wouaf.getIndice() +" ");
			
		}
	}
	
	public void verifQuartiers(){
		if(quartiers[0]==2){
			((CaseImmobilier)Constante.belleville).setLoyer(40);
			((CaseImmobilier)Constante.lecourbe).setLoyer(40);
		}
		if(quartiers[1]==3){
			((CaseImmobilier)Constante.vaugirard).setLoyer(100);
			((CaseImmobilier)Constante.cc).setLoyer(100);
			((CaseImmobilier)Constante.publi).setLoyer(120);
			
		}
		if(quartiers[2]==3){
			((CaseImmobilier)Constante.ptitville).setLoyer(140);
			((CaseImmobilier)Constante.neuil).setLoyer(140);
			((CaseImmobilier)Constante.paradis).setLoyer(160);
			
		}
		if(quartiers[3]==3){
			((CaseImmobilier)Constante.mosard).setLoyer(180);
			((CaseImmobilier)Constante.michel).setLoyer(180);
			((CaseImmobilier)Constante.pigaille).setLoyer(200);
			
		}
		if(quartiers[4]==3){
			((CaseImmobilier)Constante.magigno).setLoyer(2200);
			((CaseImmobilier)Constante.males).setLoyer(220);
			((CaseImmobilier)Constante.henri).setLoyer(240);
			
		}
		if(quartiers[5]==3){
			((CaseImmobilier)Constante.honore).setLoyer(260);
			((CaseImmobilier)Constante.bourse).setLoyer(260);
			((CaseImmobilier)Constante.fayette).setLoyer(280);
			
		}
		if(quartiers[6]==3){
			((CaseImmobilier)Constante.breteille).setLoyer(300);
			((CaseImmobilier)Constante.foch).setLoyer(300);
			((CaseImmobilier)Constante.capuchne).setLoyer(320);
			
		}
		if(quartiers[7]==2){
			((CaseImmobilier)Constante.elysees).setLoyer(350);
			((CaseImmobilier)Constante.paix).setLoyer(400);
			
		}
	}


	public int getNbComp() {
		return nbComp;
	}

	public void setNbComp(int nbComp) {
		this.nbComp = nbComp;
	}

	public boolean isCartePrison() {
		return cartePrison;
	}

	public void setCartePrison(boolean cartePrison) {
		this.cartePrison = cartePrison;
	}

	
}
