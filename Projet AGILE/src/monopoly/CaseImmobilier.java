package monopoly;

import java.util.Scanner;

public class CaseImmobilier extends Case {
	
	private int prix;
	private boolean libre;
	private boolean toutQuartier;
	private String quartier;
	private int loyer;
	private int nombreBatiment;
	private int proprietaire;

	public CaseImmobilier(int prix, String nom, int loyer,String quartier) {
		this.prix = prix;
		this.quartier = quartier;
		this.loyer = loyer;
		this.setNom(nom);
		nombreBatiment = 0;
		proprietaire = 0;
		toutQuartier = false;
		libre = true;
		}
	
	public void acheter(Pion p){
		if(p.getArgent()>=this.prix && libre && p.getIndice() != 0 && p.getIndice() != 10 && p.getIndice() != 20 && p.getIndice() != 30){
			//texte qui demande d'acheter
			Affichage.afficher(this.getNom()+ ":Voulez vous acheter ?(Si oui ,taper <<o>> )");
			//System.out.print(this.getNom()+ ":Voulez vous acheter ?(Si oui ,taper <<o>> )");
			Scanner sc = new Scanner(System.in);
			String rep = sc.nextLine();
			if(rep.equals("o") || rep.equals("O")){
				p.setArgent(p.getArgent()-this.prix); 
				//texte qui confirme l'achat
				Affichage.afficher("Achat effectue");
				setProprietaire(p.getEquipe());
				p.getProp().add(this);
				libre=false;
				loyer=prix;
			}
			else{
				//texte qui confirme le non achat
				Affichage.afficher("Achat annule");
			}
		}
		else if(p.getArgent()<this.prix){
			
			//texte qui dit que le joueur n'a pas assez d'argent
			Affichage.afficher(this.getNom()+ "Argent insuffisant");
		}

	}
	
	
	
	
	public void payer(Pion p1, Pion p2){
		//message qui dit le loyer à payer
		if(p1.peutPayer(this.loyer)){
			p1.setArgent(p1.getArgent()-this.loyer);
			p2.setArgent(p2.getArgent()+this.loyer);
		}
		else{
			//perdu pour le moment
			Affichage.afficher("PLUS D'ARGENT ! FALLIIITE! POUR LE JOUEUR"+p1.getEquipe());
			
		}
		
		
	}
	@Override
	public void action(Pion p1, Pion p2) {
		if(libre == true){
			acheter(p1);
		}else
		{if(getProprietaire() == p1.getEquipe()){
			// vous etes chez vous.
			Affichage.afficher(this.getNom()+" :Vous etes sur la maison de votre equipe");
			
		}else{
			Affichage.afficher(this.getNom()+" :vous devez payer pour le joueur abverse la somme:"+this.getLoyer() );
			payer(p1,p2);
		}
		
		// TODO Auto-generated method stub
		
	}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	public boolean isToutQuartier() {
		return toutQuartier;
	}

	public void setToutQuartier(boolean toutQuartier) {
		this.toutQuartier = toutQuartier;
	}

	public String getQuartier() {
		return quartier;
	}

	public void setQuartier(String quartier) {
		this.quartier = quartier;
	}

	public int getLoyer() {
		return loyer;
	}

	public void setLoyer(int loyer) {
		this.loyer = loyer;
	}

	public int getNombreBatiment() {
		return nombreBatiment;
	}

	public void setNombreBatiment(int nombreBatiment) {
		this.nombreBatiment = nombreBatiment;
	}

	public int getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(int propietaire) {
		this.proprietaire = propietaire;
	}
}
