package monopoly;

public abstract class Case {

	private String nom;
	private int indice;
	private Pion p1;
	private Pion p2;
	private Plateau p=Plateau.test;
	public Plateau getPlateau() {
		return p;
	}
	public void setP(Plateau p) {
		this.p = p;
	}
	
	public abstract void action(Pion p1, Pion p2);

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}

	public Pion getP1() {
		return p1;
	}

	public void setP1(Pion p1) {
		this.p1 = p1;
	}

	public Pion getP2() {
		return p2;
	}

	public void setP2(Pion p2) {
		this.p2 = p2;
	}

	
	public String getStringJoueurs(){
		if(p1 == null && p2 == null){
			return "   ";
		}
		if(p1 != null && p2 == null){
			return " "+p1.toString()+" ";
		}
		if(p1 == null && p2 != null){
			return " "+p2.toString()+" ";
		}
		if(p1 != null && p2 != null){
			return p1.toString()+" "+p2.toString();
		}
		return "bug case methode getStringJoueurs";

	}
	
	public String toString(){
		String res = nom;
		for (int i = nom.length(); i < 21; i++) {
			res += " ";
		}
		return res;
		
	}



	
	
}
