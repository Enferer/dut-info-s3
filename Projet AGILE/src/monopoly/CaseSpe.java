package monopoly;

public class CaseSpe extends Case {

	public final static int depart=99;
	public final static int parc=513;
	private int type=0;
	public CaseSpe(String noms,int type){
		this.setNom(noms);
		this.type=type;
	}
	@Override
	public void action(Pion p1, Pion p2) {
		// TODO Auto-generated method stub
		
		if (type==depart){
			System.out.println(this.getNom()+": Bonus pour atteindre sur cette case :1000 euro");
		//	Affichage.afficher("Depart: Bonus pour atteindre sur cette case :1000 euro");
			p1.setArgent(p1.getArgent()+1000);
		}else{
			System.out.println(this.getNom()+ ":Aucun impact sur le jeu ...");
			//Affichage.afficher("Parc gratuit :Aucun impact sur le jeu ...");
			
		}
	}
	public static void main(String[] args) throws InterruptedException{
		Plateau p =new Plateau();
		Pion p1=new Pion(1);
		System.out.println(p1.getArgent());
		p1.deplacer(0, p,true);
		System.out.println(p1.getArgent());
	}
}
