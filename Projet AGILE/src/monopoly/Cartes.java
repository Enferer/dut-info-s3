package monopoly;

public class Cartes {
	String [] chance = new String[10];
	String [] commu = new String[10];

	public Cartes(){
		chance[0]="Retournez à la case départ et touchez 1000€";
		chance[1]="Vous gagnez un voyage, 250€ offerts";
		chance[2]="Erreur de la banque en votre faveur, recevez 500€";
		chance[3]="Sortez de prison, cette carte peut etre conservee";
		chance[4]="Accident de voiture, 800€ de frais de reparation";
		chance[5]="C'est l'anniversaire de votre adversaire, donnez lui 200€";
		chance[6]="Votre chien a la rage le veterinaire demande 700€";
		chance[7]="Allez en prison sans passer par la case depart";
		chance[8]="Degats des eaux dans votre appartement, payez 1000€ de reparation";
		chance[9]="Seisme, chaque joueur perd 1000€";
		commu[0]="Vous gagnez au concours de beauté, vous recevez 500€";
		commu[1]="Amende pour ivresse, vous payez 1000€";
		commu[2]="Allez en directement en prison , vous ne recevez pas 2000€";
		commu[3]="Vos investissements vous rapportent, vous gagnez 1000€";
		commu[4]="Vous allez chez votre concessionaire, vous lui devez 500€";
		commu[5]="La vente de votre auto vous rapporte 1000€";
		commu[6]="Avancez jusqu'au la rue de la Paix, vous gagnez 2000€ si vous passez par la casse départ";
		commu[7]="Reculez de 3 cases";
		commu[8]="Avancez jusqu'au boulevard Malesherbes, vous gagnez 2000€ si vous passez par la casse départ";
		commu[9]="Vous gagnez à la loterie, vous recevez 750€";
}
	
	public void choixCarteChance(Pion p1, Pion p2, int i, Plateau p) throws InterruptedException{
		
		switch(i){
		
		case 0:
	//		p.getCase(p1.getIndice()).setP1(null);
			p1.deplacer(40-p1.getIndice(), p,true);
			p1.setIndice(0);
		//	p.getCase(p1.getIndice()).setP1(p1);
			break;	
		
		case 1:
			p1.setArgent(p1.getArgent()+250);
			break;
		
		case 2:
			p1.setArgent(p1.getArgent()+500);
			break;
		
		case 3:
			if (p1.isLibre()){
			p1.setCartePrison(true);
			}else{
				p1.setLibre(true);
			}
			break;
		
		case 4:
			p1.setArgent(p1.getArgent()-800);
			break;
		
		case 5:
			p1.setArgent(p1.getArgent()-200);
			p2.setArgent(p2.getArgent()+200);
			break;
		
		case 6:
			p1.setArgent(p1.getArgent()-700);
			break;
		
		case 7:
	//		p.getCase(p1.getIndice()).setP1(null);
	//		p1.deplacer(50-p1.getIndice(), p);
			p1.setIndice(10);
		//	p.getCase(p1.getIndice()).setP1(p1);
			p1.setLibre(false);
			break;
		
		case 8:
			p1.setArgent(p1.getArgent()-1000);
			break;
		
		case 9:
			p1.setArgent(p1.getArgent()-1000);
			p2.setArgent(p2.getArgent()-1000);
			break;
		
		
		}
	}
	
public void choixCarteCommu(Pion p, Plateau pl, int i) throws InterruptedException{
		
		switch(i){
		
		case 0:
			p.setArgent(p.getArgent()+500);
			break;
		
		case 1:
			p.setArgent(p.getArgent()-1000);
			break;
		
		case 2:
		//	pl.getCase(p.getIndice()).setP1(null);
//			p.deplacer(50-p.getIndice(), pl);
			p.setIndice(10);
			p.setLibre(false);
	//		pl.getCase(p.getIndice()).setP1(p);
			break;
		
		case 3:
			p.setArgent(p.getArgent()+1000);
			break;
		
		case 4:
			p.setArgent(p.getArgent()-500);
			break;
		
		case 5:
			p.setArgent(p.getArgent()+1000);
			break;
		
		case 6:
	//		pl.getCase(p.getIndice()).setP1(null);
			if (p.getIndice()<39)
			p.deplacer(39-p.getIndice(), pl,true);
			else {
				p.deplacer(40-p.getIndice(), pl,true);
				p.setIndice(39);
			}
//			pl.getCase(p.getIndice()).setP1(p);
			break;
		
		case 7:
		//	pl.getCase(p.getIndice()).setP1(null);
			int val=3;
			if (p.getIndice()<3){
				val=p.getIndice();
			}
			p.deplacer(p.getIndice()-val, pl,true);
			
			p.setIndice(0);
	//		pl.getCase(p.getIndice()).setP1(p);
			break;
		
		case 8:
	//		pl.getCase(p.getIndice()).setP1(null);
			if (p.getIndice()<39)
			p.deplacer(23-p.getIndice(), pl,true);
			else{
				p.deplacer(40-p.getIndice()+23, pl,true);
				p.setIndice(23);
			}
		//	pl.getCase(p.getIndice()).setP1(p);
			break;
		
		case 9:
			p.setArgent(p.getArgent()+750);
			break;
		
		
		}
	}
}



