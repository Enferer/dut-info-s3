package monopoly;

import java.util.Random;

public class CaseCarte extends Case{
	public final static int chance=99;
	public final static int communication=513;
	private int type;
	public CaseCarte(int type,String noms){
		this.type=type;
		this.setNom(noms);
	}
	private Cartes tirage=new Cartes();
	
	@Override
	public void action(Pion p1, Pion p2) {
		// TODO Auto-generated method stub
		Random r=new Random();
		if (type==chance){
			int ch=r.nextInt(10);
			Affichage.afficher((getNom()+":"+tirage.chance[ch]));
			try {
				tirage.choixCarteChance(p1, p2,ch ,super.getPlateau());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if (type==communication){
			int co=r.nextInt(10);
			Affichage.afficher((getNom()+":"+tirage.commu[co]));
			try {
				tirage.choixCarteCommu(p1, super.getPlateau(), r.nextInt(10));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			
		}
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String[] args) throws InterruptedException{
		Plateau p=new Plateau();
		Pion p1=new Pion(1);
		Pion p2=new Pion(2);
		Affichage.afficher(p,p.getCase(0),p1,p2,"");
		Cartes tirage=new Cartes();
		Constante.chance.setP(p);
		Constante.commu.setP(p);
		p1.deplacer(47, p,true);
		Affichage.afficher(p,p.getCase(0),p2,p1,"");
		p1.deplacer(50, p,true);
	/*	for (int x=0;x<20;x++){
			Affichage.afficher(p,p.getCase(0),p1,p2,"");
			p1.deplacer(1, p);
			Constante.chance.action(p1, p2);
			Thread.sleep(1000);
			Affichage.afficher(p,p.getCase(0),p2,p1,"");
			p2.deplacer(1, p);

			Constante.commu.action(p2, p1);
			Thread.sleep(1000);
		}*/
		for (int x=0;x<10;x++){
			Affichage.afficher(p,p.getCase(0),p1,p2,"");
			Affichage.afficher((tirage.chance[x]));
			tirage.choixCarteCommu(p1, p, x);
			Thread.sleep(1000);
			
			Affichage.afficher(p,p.getCase(0),p2,p1,"");
			Affichage.afficher((tirage.commu[x]));
			tirage.choixCarteCommu(p2, p, x);
			Thread.sleep(1000);
	}
	}
	
}
