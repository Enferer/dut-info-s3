package monopoly;

import java.util.Random;
import java.util.Scanner;

public class CasePrison extends Case {
	final int indice=10;
	public CasePrison(){
		setNom("Prison");
	}
	@Override
	public void action(Pion p1, Pion p2) {
		// TODO Auto-generated method stub
		if (p1.isLibre())
		Affichage.afficher("Visite d'un prison ");
		else{
			String c="Tentative sortie :choix action:1)payer 50 | 2)echapper ";
			/*Affichage.afficher("Tentative de liberation :choisissez votre action:");
			Affichage.afficher("1)payer une somme de 50 euro");
			Affichage.afficher("2)tenter d'evader");*/
			if (p1.isCartePrison()){
				c+="| 3)CARTE PRISON";
			}
			Affichage.afficher(c);
			Scanner sc = new Scanner(System.in);
			String rep = sc.nextLine();
			if (rep.equals("1")) 
				p1.setLibre(payer(p1));
			else if (rep.equals("2"))
				p1.setLibre(tentative(p1));
			else if ((rep.equals("3"))&&(p1.isCartePrison()))
				p1.setLibre(cartechance(p1));
			else Affichage.afficher("Rester en prison");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	boolean cartechance(Pion p1){
		p1.setCartePrison(false);
		Affichage.afficher("Carte utilise ,vous etes libre");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	boolean tentative(Pion p1){
		Random r=new Random();
		int d1=r.nextInt(7)+1;
		int d2=r.nextInt(7)+1;
		if (d1==d2){
			Affichage.afficher(d1+"="+d2+"Tentative reussi");
			return true;
		}
		Affichage.afficher(d1+"!"+d2+"Tentative rate !");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	boolean payer(Pion p1){
		if (p1.getArgent()>50){
		p1.setArgent(p1.getArgent()-50);
		Affichage.afficher("Paye :Vous etes libre");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;}
		Affichage.afficher("Pas assez d'argent !");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public static void main(String[] args){
		Pion prison=new Pion(0);
		CasePrison garde=new CasePrison();
		prison.setIndice(10);
		prison.setLibre(false);
		prison.setCartePrison(true);
		do {
			garde.action(prison, null);
		}while (!prison.isLibre());
	}

}