package monopoly;

public class Constante {

	final static Case  depart= new CaseImmobilier(60,"Depart",0,"");
	
	final static Case  commu=new CaseCarte(CaseCarte.communication,"Communauté");
	final static Case  commu2=new CaseCarte(CaseCarte.communication,"Communauté");
	final static Case  commu3=new CaseCarte(CaseCarte.communication,"Communauté");
	
	final static Case  chance=new CaseCarte(CaseCarte.chance,"Chance");
	final static Case  chance2=new CaseCarte(CaseCarte.chance,"Chance");
	final static Case  chance3=new CaseCarte(CaseCarte.chance,"Chance");
	
	final static Case  parc=new CaseImmobilier(0,"Parc gratuit",0,"");
	final static Case  visPrison=new CasePrison();
	final static Case  chopPrison=new CaseImmobilier(60,"chopPrison",0,"");
	
	final static Case taxe=new CaseTaxe("Taxe");
	final static Case impot=new CaseTaxe("Impot sur le revenu");
	
	final static Case  elect=new CaseCompagnie("EDF");
	final static Case  eau=new CaseCompagnie("Distribution eau");

	
	final static Case  Lyon=new CaseGare("Gare Lyon");
	final static Case  Montparnasse=new CaseGare("Gare Montparnasse");	
	final static Case  Nord=new CaseGare("Gare du Nord");
	final static Case  Lazare=new CaseGare("Gare Lazare");
	
	final static Case  belleville=new CaseImmobilier(60,"boulevard belleville", 20,"violet");
	final static Case  lecourbe=new CaseImmobilier(80,  "rue lecourbe", 20,"violet");
	final static Case  vaugirard=new CaseImmobilier(100,"rue de vaugirard", 50,"bleu ciel");
	final static Case  cc=new CaseImmobilier(100,       "rue de courcelles", 50,"bleu ciel");
	final static Case  publi=new CaseImmobilier(120,    "avn de la republique", 60,"bleu ciel");
	final static Case  ptitville=new CaseImmobilier(140,"bvd de la villette", 70,"rose");
	final static Case  neuil=new CaseImmobilier(140,    "avenue de neully", 70,"rose");
	final static Case  paradis=new CaseImmobilier(160,  "rue de paradis", 80,"rose");
	final static Case  mosard=new CaseImmobilier(180,   "avenue mozard", 90,"orange");
	final static Case  michel=new CaseImmobilier(180,   "bvd saint-michel", 90,"orange");
	final static Case  pigaille=new CaseImmobilier(200, "place pigalle", 100,"orange");
	final static Case  magigno=new CaseImmobilier(220,  "avenue matignon", 110,"rouge");
	final static Case  males=new CaseImmobilier(220,    "boulevard malesherbes", 110,"rouge");
	final static Case  henri=new CaseImmobilier(240,    "avenue henri-martin", 120,"rouge");
	final static Case  honore=new CaseImmobilier(260,   "faubourg saint-honore", 130,"jaune");
	final static Case  bourse=new CaseImmobilier(260,   "place de la bourse", 130,"jaune");
	final static Case  fayette=new CaseImmobilier(280,  "rue de la fayette", 140,"jaune");
	final static Case  breteille=new CaseImmobilier(300,"avenue de breteuil", 150,"vert");
	final static Case  foch=new CaseImmobilier(300,     "avenue foch", 150,"vert");
	final static Case  capuchne=new CaseImmobilier(320, "bvd des capucines", 160,"vert");
	final static Case  elysees=new CaseImmobilier(350,  "av des champs elysees", 175,"bleu fonce");
	final static Case  paix=new CaseImmobilier(400,     "rue de la paix", 200,"bleu fonce");
	
	public static Case appliqueCase(int x){
		switch(x){
		case 0:
			return depart;///en cour
		case 1:
			return belleville;
		case 2:
			return commu;
		case 3:
			return lecourbe;
		case 4:
			return impot;
		case 5:
			return Montparnasse;
		case 6:
			return vaugirard;
		case 7:
			return chance;
		case 8:
			return cc;
		case 9:
			return publi;
		case 10:
			return visPrison;
		case 11:
			return ptitville;
		case 12:
			return elect;
		case 13:
			return neuil;
		case 14:
			return paradis;
		case 15:
			return Lyon;
		case 16:
			return mosard;
		case 17:
			return commu2;
		case 18:
			return michel;
		case 19:
			return pigaille;
		case 20:
			return parc;///en cour
		case 21:
			return magigno;
		case 22:
			return chance2;
		case 23:
			return males;
		case 24:
			return henri;
		case 25:
			return Nord;
		case 26:
			return honore;
		case 28:
			return eau;
		case 29:
			return fayette;
		case 30:
			return chopPrison;
		case 31:
			return breteille;
		case 32:
			return foch;
		case 33:
			return commu3;
		case 34:
			return capuchne;
		case 35:
			return Lazare;
		case 36:
			return chance3;
		case 37:
			return elysees;
		case 38:
			return taxe;
		case 39:
			return paix;
		case 27:
			return bourse;
		}
		
		return new CaseImmobilier(60,"boulevard belleville", 0,"");
	}
}
