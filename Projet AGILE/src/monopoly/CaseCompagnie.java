package monopoly;

import java.util.Scanner;

public class CaseCompagnie extends Case{
	
	private int prix = 150;
	private boolean libre;
	private int proprietaire;
	
	public CaseCompagnie(String nom){
		libre = true;
		proprietaire=0;
		this.setNom(nom);
	}
	
	public void acheter(Pion p){
		if(p.getArgent()>=this.prix && libre){
			//texte qui demande d'acheter
			Affichage.afficher(this.getNom()+ ":Voulez vous acheter ?(Si oui ,taper <<o>> )");
			//System.out.print(this.getNom()+ ":Voulez vous acheter ?(Si oui ,taper <<o>> )");
			Scanner sc = new Scanner(System.in);
			String rep = sc.nextLine();
			if(rep.equals("o") || rep.equals("O")){
				p.setArgent(p.getArgent()-this.prix); 
				//texte qui confirme l'achat
				Affichage.afficher("Achat effectue");
				setProprietaire(p.getEquipe());
				p.getProp().add(this);
				libre=false;
				
			}
			else{
				//texte qui confirme le non achat
				Affichage.afficher("Achat annule");
			}
		}
		else if(p.getArgent()<this.prix){
			
			//texte qui dit que le joueur n'a pas assez d'argent
			Affichage.afficher(this.getNom()+ "Argent insuffisant");
		}
	}

	public void payer(Pion p1, Pion p2, int de){
		//message qui dit le loyer à payer
		
		if(p2.getNbComp()==1){
			Affichage.afficher(this.getNom()+" :vous devez payer pour le joueur abverse la somme:" +de*4);
			if(p1.peutPayer(de*4)){
				p1.setArgent(p1.getArgent()-de*4);
				p2.setArgent(p2.getArgent()+de*4);
				
			}
			else{
				//perdu pour le moment
				Affichage.afficher("PLUS D'ARGENT ! FALLIIITE! POUR LE JOUEUR"+p1.getEquipe());
				
			}
		}
		else {
			Affichage.afficher(this.getNom()+" :vous devez payer pour le joueur abverse la somme:" +de*10);
			if(p1.peutPayer(de*10)){
				p1.setArgent(p1.getArgent()-de*10);
				p2.setArgent(p2.getArgent()+de*10);
				
			}
			else{
				//perdu pour le moment
				Affichage.afficher("PLUS D'ARGENT ! FALLIIITE! POUR LE JOUEUR"+p1.getEquipe());
				
			}
		}

	}
	
	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	public int getProprietaire() {
		return proprietaire;
	}

	private void setProprietaire(int equipe) {
		// TODO Auto-generated method stub
		
	}
	
	public void action(Pion p1, Pion p2){
		action(p1,p2,100);
	}


	public void action(Pion p1, Pion p2, int de) {
		if(libre){
			acheter(p1);
		}
		else {
			if(getProprietaire()==p1.getEquipe()){
				//vous etes chez vous
				Affichage.afficher(this.getNom()+" :Vous etes sur la Compagnie de votre equipe");
			}
			else{
				
				payer(p1,p2,de);
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
