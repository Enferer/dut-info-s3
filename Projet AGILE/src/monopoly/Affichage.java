package monopoly;

import java.util.List;

public class Affichage {

	private static String nomCase1;
	private static String nomCase2;
	private static String prixCase1;
	private static String prixCase2;
	private static String[] tabPion1 = new String[14];
	private static String[] tabPion2 = new String[14];
	
	
	private static Plateau p;
	private static Case c;
	private static Pion pion;
	private static Pion pion2;
	private static String phrase;
	
	
	public static void afficher(String phrase){
		Affichage.phrase = phrase;
		Affichage.afficher(Affichage.p,Affichage.c,Affichage.pion,Affichage.pion2,Affichage.phrase);
	}
	
	public static void afficher(String phrase,Case c){
		Affichage.c = c;
		Affichage.phrase = phrase;
		Affichage.afficher(Affichage.p,Affichage.c,Affichage.pion,Affichage.pion2,Affichage.phrase);
	}
	
	
	
	public static void afficher(Plateau p,Case c,Pion pion,Pion pion2,String phrase) throws NullPointerException{
		
		Affichage.p = p;
		Affichage.c = c;
		Affichage.pion = pion;
		Affichage.pion2 = pion2;
		Affichage.phrase = phrase;
		
		getNomdecouper(c.getNom());
		getStringPrix(c);
		tabPion1 = listToTab(pion.getProp());
		tabPion2 = listToTab(pion2.getProp());
		//getStringTab(pion,pion2);
		
		//fin decoupahe case courante
		System.out.println("\033[H\033[2J");
		System.out.flush();
		System.out.println("|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|");
		System.out.println("|           |"+rose()+"   Bvd de  "+noir()+"| Caisse de |"+rose()+"    Rue    "+noir()+"|Imports sur|   Gare    |"+cyan()+"   Rue du  "+noir()+"|           |"+cyan()+"  Rue de   "+noir()+"|"+cyan()+" Avenue de "+noir()+"|           |");
		System.out.println("|  Départ   |"+rose()+" Belleville"+noir()+"| Communauté|"+rose()+"  Lecourbe "+noir()+"| le revenu |MtPARNASSE |"+cyan()+" Vaugirard "+noir()+"|  CHANCE   |"+cyan()+" Courcelles"+noir()+"|"+cyan()+" la replu. "+noir()+"|  Prison   |");
		System.out.println("|    "+p.getCase(0).getStringJoueurs()+"    |"+rose()+"    "+p.getCase(1).getStringJoueurs()+"    "+noir()+"|    "+p.getCase(2).getStringJoueurs()+"    |"+rose()+"    "+p.getCase(3).getStringJoueurs()+"    "+noir()+"|    "+p.getCase(4).getStringJoueurs()+"    |    "+p.getCase(5).getStringJoueurs()+"    |"+cyan()+"    "+p.getCase(6).getStringJoueurs()+"    "+noir()+"|    "+p.getCase(7).getStringJoueurs()+"    |"+cyan()+"    "+p.getCase(8).getStringJoueurs()+"    "+noir()+"|"+cyan()+"    "+p.getCase(9).getStringJoueurs()+"    "+noir()+"|    "+p.getCase(10).getStringJoueurs()+"    |");
		System.out.println("|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|");
		System.out.println("|"+bleu()+"  Rue de   "+noir()+"|                                                                                                           |"+rose()+" Bvd de la "+noir()+"|");
		System.out.println("|"+bleu()+"  la paix  "+noir()+"|                                                                                                           |"+rose()+"  Villette "+noir()+"|");
		System.out.println("|"+bleu()+"    "+p.getCase(39).getStringJoueurs()+"    "+noir()+"|                                                                                                           |"+rose()+"    "+p.getCase(11).getStringJoueurs()+"    "+noir()+"|");
		System.out.println("|-----------|                                                                                                           |-----------|");
		System.out.println("|  Taxe de  |                                                                                                           |           |");
		System.out.println("|    Luxe   |         |---------------------|                                           |---------------------|         |    EDF    |");
		System.out.println("|    "+p.getCase(38).getStringJoueurs()+"    |         |                     |                                           |                     |         |    "+p.getCase(12).getStringJoueurs()+"    |");
		System.out.println("|-----------|         |      Joueur 1       |                                           |      Joueur 2       |         |-----------|");
		System.out.println("|"+bleu()+"   Champs  "+noir()+"|         |                     |          |---------------------|          |                     |         |"+rose()+"  Avn. de  "+noir()+"|");
		System.out.println("|"+bleu()+"  élysées  "+noir()+"|         |---------------------|          |                     |          |---------------------|         |"+rose()+"  neuilly  "+noir()+"|");
		System.out.println("|"+bleu()+"    "+p.getCase(37).getStringJoueurs()+"    "+noir()+"|         |Argent :"+getStringPrix2(pion.getArgent())+"|          |"+nomCase1+"|          |Argent :"+getStringPrix2(pion2.getArgent())+"|         |"+rose()+"    "+p.getCase(13).getStringJoueurs()+"    "+noir()+"|");
		System.out.println("|-----------|         |---------------------|          |"+nomCase2+"|          |---------------------|         |-----------|");
		System.out.println("|           |         |"+getStringTab(0,1)+"|          |_____________________|          |"+getStringTab(0,2)+"|         |"+rose()+"   Rue de  "+noir()+"|");
		System.out.println("|  CHANCE   |         |"+getStringTab(1,1)+"|          |                     |          |"+getStringTab(1,2)+"|         |"+rose()+"  Paradis  "+noir()+"|");
		System.out.println("|    "+p.getCase(36).getStringJoueurs()+"    |         |"+getStringTab(2,1)+"|          |      "+Affichage.prixCase1+"  |          |"+getStringTab(2,2)+"|         |"+rose()+"    "+p.getCase(14).getStringJoueurs()+"    "+noir()+"|");
		System.out.println("|-----------|         |"+getStringTab(3,1)+"|          |     "+Affichage.prixCase2+" |          |"+getStringTab(3,2)+"|         |-----------|");
		System.out.println("|   Gare    |         |"+getStringTab(4,1)+"|          |                     |          |"+getStringTab(4,2)+"|         |   Gare    |");
		System.out.println("| St-Lazare |         |"+getStringTab(5,1)+"|          |                     |          |"+getStringTab(5,2)+"|         |  de LYON  |");
		System.out.println("|    "+p.getCase(35).getStringJoueurs()+"    |         |"+getStringTab(6,1)+"|          |"+getStringTab(6,2)+"|          |                     |         |    "+p.getCase(15).getStringJoueurs()+"    |");
		System.out.println("|-----------|         |"+getStringTab(7,1)+"|          |                     |          |"+getStringTab(7,2)+"|         |-----------|");
		System.out.println("|"+vert()+" Bvd des   "+noir()+"|         |"+getStringTab(8,1)+"|          |                     |          |"+getStringTab(8,2)+"|         |"+orange()+"   Avenue  "+noir()+"|");
		System.out.println("|"+vert()+"  Capucines"+noir()+"|         |"+getStringTab(9,1)+"|          |                     |          |"+getStringTab(9,2)+"|         |"+orange()+"   Mozart  "+noir()+"|");
		System.out.println("|"+vert()+"    "+p.getCase(34).getStringJoueurs()+"    "+noir()+"|         |"+getStringTab(10,1)+"|          |"+getStringTab(10,2)+"|          |                     |         |"+orange()+"    "+p.getCase(16).getStringJoueurs()+"    "+noir()+"|");
		System.out.println("|-----------|         |"+getStringTab(11,1)+"|          |---------------------|          |"+getStringTab(11,2)+"|         |-----------|");
		System.out.println("| Caisse de |         |"+getStringTab(12,1)+"|                                           |"+getStringTab(12,2)+"|         | Caisse de |");
	    System.out.println("| Communauté|         |"+getStringTab(13,1)+"|                                           |"+getStringTab(13,2)+"|         | Communauté|");
		System.out.println("|    "+p.getCase(33).getStringJoueurs()+"    |         |---------------------|                                           |---------------------|         |    "+p.getCase(17).getStringJoueurs()+"    |");
	    System.out.println("|-----------|                                                                                                           |-----------|");
		System.out.println("|"+vert()+"  Avenue   "+noir()+"|                                                                                                           |"+orange()+"    Bvd    "+noir()+"|");
		System.out.println("|"+vert()+"   FOCH    "+noir()+"|                                                                                                           |"+orange()+"Saint mich."+noir()+"|");
	    System.out.println("|"+vert()+"    "+p.getCase(32).getStringJoueurs()+"    "+noir()+"|                                                                                                           |"+orange()+"    "+p.getCase(18).getStringJoueurs()+"    "+noir()+"|");
		System.out.println("|-----------|                                                                                                           |-----------|");
		System.out.println("|"+vert()+" Avenue de "+noir()+"|                                                                                                           |"+orange()+"   Place   "+noir()+"|");
		System.out.println("|"+vert()+"  breteuil "+noir()+"|                                                                                                           |"+orange()+"  Pigalle  "+noir()+"|");
		System.out.println("|"+vert()+"    "+p.getCase(31).getStringJoueurs()+"    "+noir()+"|                                                                                                           |"+orange()+"    "+p.getCase(19).getStringJoueurs()+"    "+noir()+"|");
		System.out.println("|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|");
		System.out.println("|  Allez en |"+orange()+"   Rue la  "+noir()+"|  Distrib. |"+orange()+"  Place de "+noir()+"|"+orange()+"  Faubourg "+noir()+"|   Gare    |"+rouge()+"Avn Henri- "+noir()+"|"+rouge()+"    Bvd    "+noir()+"|           |"+rouge()+"  Avenue   "+noir()+"|   Parc    |");
		System.out.println("|   prison  |"+orange()+"   Fayette "+noir()+"|  des eaux |"+orange()+"  la bourse"+noir()+"|"+orange()+" st-honoré "+noir()+"|  du NORD  |"+rouge()+"  martin   "+noir()+"|"+rouge()+"Malesherbes"+noir()+"|   CHANCE  |"+rouge()+"  Matignon "+noir()+"|  gratuit  |");
		System.out.println("|    "+p.getCase(30).getStringJoueurs()+"    |"+orange()+"    "+p.getCase(29).getStringJoueurs()+"    "+noir()+"|    "+p.getCase(28).getStringJoueurs()+"    |"+orange()+"    "+p.getCase(27).getStringJoueurs()+"    "+noir()+"|"+orange()+"    "+p.getCase(26).getStringJoueurs()+"    "+noir()+"|    "+p.getCase(25).getStringJoueurs()+"    |"+rouge()+"    "+p.getCase(24).getStringJoueurs()+"    "+noir()+"|"+rouge()+"    "+p.getCase(23).getStringJoueurs()+"    "+noir()+"|    "+p.getCase(22).getStringJoueurs()+"    |"+rouge()+"    "+p.getCase(21).getStringJoueurs()+"    "+noir()+"|    "+p.getCase(20).getStringJoueurs()+"    |");
		System.out.println("|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|");
		System.out.println("");
		System.out.println("     ---------------------------------------------------------------------------------------------------------------------------");
		System.out.println("     |                                                                                                                         |");
		System.out.println(getPhrase(phrase));
		System.out.println("     |                                                                                                                         |");
		System.out.println("     ---------------------------------------------------------------------------------------------------------------------------");
	}
	private static String getPhrase(String s) {
		String res;
		res = "     |";
		int cpt = 0;
		for (int i = 0; i < (123-s.length())/2; i++) {
			res+=' ';
			cpt++;
			
		}
		res += s;
		if(s.length()%2 != 0 ){
			cpt -=1;
		}
		for (int i = 0; i < cpt-1; i++) {
			res+=' ';
		}
		res += '|';
		return res;
	}
	private static String[] listToTab(List<Case> prop) {
		String[] res = new String[14];
		for (int i = 0; i < prop.size(); i++) {
			res[i] = prop.get(i).toString();
		}
		return res;
	}
	private static String getStringTab(int idx, int equipe) {
		if(equipe == 2){
			
			if(Affichage.tabPion2[idx]!=null){return Affichage.tabPion2[idx];}
			else{return "                     ";}
		}
		if(Affichage.tabPion1[idx]!=null){return Affichage.tabPion1[idx];}
		else{return "                     ";}
	}

	private static String getStringPrix2(int argent) {
		Integer i = argent;
		String res = i.toString();
		for (int x = res.length(); x < 13; x++) {
			res += " ";
		}
		
		return res;
	}
	private static void getStringPrix(Case c) {
		if(c instanceof CaseImmobilier){
			Integer prix = ((CaseImmobilier) c).getPrix();
			Integer loyer = ((CaseImmobilier) c).getLoyer();
			String res = "Prix : ";
			String res2 = "Loyer : ";
			 res += prix.toString();
			 res2 += loyer.toString();
			
			
			
			//System.out.println(res);
			

			for (int i = res.length(); i < 13; i++) {
				res += " ";
			}
			for (int i = res2.length(); i < 15; i++) {
				res2 += " ";
			}
			
			Affichage.prixCase2 = res2;
			Affichage.prixCase1 = res;
			
		}
		if(c instanceof CaseCompagnie){
			Integer prix = ((CaseCompagnie) c).getPrix();
			
			String res = "Prix : ";
			res += prix.toString();
			
			String res2 = "Loyer : dé x4  ";
			
			//System.out.println(res);
			

			for (int i = res.length(); i < 13; i++) {
				res += " ";
			}
			
			Affichage.prixCase2 = res2;
			Affichage.prixCase1 = res;
			
		}
		if(c.getNom().equals("Depart")){
			Affichage.prixCase1 = "Vous gagnez  ";
			Affichage.prixCase2 = "    1000       ";
		}
		
	}
	public static void getNomdecouper(String nom) {
		

		
		nomCase2 = "                     ";
		if(nom.length() == 21){
			nomCase1 = nom;
			
		}else if(nom.length()<21){
			nomCase1 = "";
			int nbresp = 21-nom.length();
			for (int i = 0; i < nbresp+1; i++) {
				if(i == nbresp/2){
					nomCase1 += nom;
				}
				else{
					nomCase1 += ' ';
				}
			}
		}
		else{
			
		}
		
	}
	
	
	private static String noir(){
		return "\033[0;30m\033[30m";
	}
	private static String vert(){
		return "\033[0;37m\033[42m";
	}
	private static String rouge(){
		return "\033[0;37m\033[41m";
	}
	private static String rose(){
		return "\033[37m\033[45m";
	}
	private static String cyan(){
		return "\033[0;37m\033[0;46m";
	}
	private static String gris(){
		return "\033[0;40m\033[37m";
	}
	private static String bleu(){
		return "\033[0;37m\033[44m";
	}
	private static String orange(){
		return "\033[0;40m\033[43m";
	}
	
	
	
	
	
	public static void main(String[]args) throws InterruptedException{
		Pion pion = new Pion(1);
		Pion pion2 = new Pion(2);
		Plateau p = new Plateau();
		p.getCase(0).setP1(pion);
		while(true){
		
		Affichage.afficher(p,p.getCase(pion.getIndice()),pion,pion2,"Appuiez sur entrée pour lancez le dé");
		pion.deplacer(3, p,true);
		}
		
	 
	}
	
}
