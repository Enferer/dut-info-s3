package monopoly;

import java.util.Random;
import java.util.Scanner;

public class Main {
	
	//fin=0;pas de fin
	//fin=1;j1 gagne
	//fin=2;j2 gagne
	static void clear(){
		for (int x=0 ;x<100;x++)
		System.out.println();
	}
	static void action(Pion jt,Pion ja,Plateau p) throws InterruptedException{
		int cpt=0;
		boolean same = true;
		while(same){
			Random r=new Random();
			Random r2=new Random();
			same = false;
			//System.out.println("Tour j"+jt.getEquipe()+(" Appuyez sur entree pour lancer le de"));
			Affichage.afficher("Tour j"+jt.getEquipe()+" Appuyez sur entree pour lancer le de");
			Scanner sc = new Scanner(System.in);
			String rep = sc.nextLine();
			
			int de=r.nextInt(6)+1;
			int de2=r2.nextInt(6)+1;
			if(de==de2){
				same=true;
				//System.out.println("Tour j"+jt.getEquipe()+(" Double !!! Rejouer possible"));
				Affichage.afficher("Tour j"+jt.getEquipe()+(" Double !!! Rejouer possible"));
				cpt++;
				Thread.sleep(1200);
			}
			if(cpt==3){
				p.getCase(jt.getIndice()).setP1(null);
				jt.deplacer(50-jt.getIndice(), p,true);
				jt.setIndice(10);
				p.getCase(jt.getIndice()).setP1(jt);
				//System.out.println("Tour j"+jt.getEquipe()+("Trop de double...La police vous a arrête pour tricherie"));
				Affichage.afficher("Tour j"+jt.getEquipe()+("Trop de double...La police vous a arrête pour tricherie"));
				jt.setLibre(false);
				Thread.sleep(1200);
			}
			int somme = de+de2;
			//System.out.println("j"+jt.getEquipe()+("Des="+de+"+"+de2));
			Affichage.afficher("Vous venez de faire : "+de+"+"+de2+" ( Appuyez sur entrée pour continuer ) ");
			sc.nextLine();
			
			jt.deplacer(somme, p,false);
			//Thread.sleep(1000);
		}
	};
	
	public static void main(String[] args) throws InterruptedException {
		Plateau p=new Plateau();
		Pion j1=new Pion(1);
		Pion j2=new Pion(2);
		Constante.chance.setP(p);
		Constante.commu.setP(p);
		Affichage.afficher(p,p.getCase(0),j1,j2,"");
		int fin=0;
		while(fin==0){
			//System.out.println(j1.getIndice());
			Affichage.afficher(p,p.getCase(j1.getIndice()),j1,j2,"");
			Main.action(j1,j2,p);
			Main.clear();
			Affichage.afficher(p,p.getCase(j2.getIndice()),j1,j2,"");
			Main.action(j2,j1,p);
			boolean bon=true;
			if (j1.getArgent()<=0){
				if(j1.getProp().isEmpty()){
					fin+=2;
				}
				else{
					while(j1.getArgent()<=0){
						Affichage.afficher("Vous n'avez plus d'argent, quelle propriete souhaitez-vous hypothequer ?");
						Affichage.afficher("Tapez le numero de la propriete que vous voulez vendre dans votre liste (à partir de 1)");
						
						do{
						try{
							bon=true;
						
						Scanner sc = new Scanner(System.in);
						String rep = sc.nextLine();
					
						j1.setArgent(j1.getArgent()+((CaseImmobilier)j1.getProp().get(Integer.parseInt(rep)-1)).getPrix()/2);
						((CaseImmobilier)j1.getProp().get(Integer.parseInt(rep)-1)).setLibre(true);
						j1.getProp().remove(Integer.parseInt(rep)-1);
						}catch(java.lang.ArrayIndexOutOfBoundsException BADABOUM){bon=false;Affichage.afficher("Vous n'avez plus d'argent, quelle propriete souhaitez-vous hypothequer ?");}
						}while ((!bon)&&(!j1.getProp().isEmpty()));
					}
				}
			}
			if (j2.getArgent()<=0){
				if(j2.getProp().isEmpty()){
					fin+=1;
				}
				else{
					while(j2.getArgent()<=0){
						do{
							try{
								bon=true;
						Affichage.afficher("Vous n'avez plus d'argent, quelle propriete souhaitez-vous hypothequer ?");
						Affichage.afficher("Tapez le numero de la propriete que vous voulez vendre dans votre liste (à partir de 1)");
						Scanner sc = new Scanner(System.in);
						String rep = sc.nextLine();
						j2.setArgent(j2.getArgent()+((CaseImmobilier)j2.getProp().get(Integer.parseInt(rep)-1)).getPrix()/2);
						((CaseImmobilier)j2.getProp().get(Integer.parseInt(rep)-1)).setLibre(true);
						j2.getProp().remove(Integer.parseInt(rep)-1);
							}catch(java.lang.ArrayIndexOutOfBoundsException BADABOUM){bon=false;}
							catch( NumberFormatException vza){bon=false;}
							
						}while ((!bon)&&(!j2.getProp().isEmpty()));
					}
				}
			}
			Main.clear();
		}
		if (fin==1){
			System.out.println("J1 gagne");
		}
		if (fin==2){
			System.out.println("J2 gagne");
		}
		if (fin==3){
			System.out.println("Match nul");
		}
	}

}
