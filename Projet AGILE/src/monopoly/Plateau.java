package monopoly;

public class Plateau {

	private	Case[] cases=new Case[40];
	final int taille=40;
	public static Plateau test=new Plateau();
	public Plateau(){
		 for (int idx=0;idx<40;idx++){
		 	cases[idx]=Constante.appliqueCase(idx);
		 }
		
	}
	public	Case getCase(int x){
		return cases[x];
	}
	public boolean activerCase(Pion c){
			Affichage.afficher("", cases[c.getIndice()]);
			cases[c.getIndice()].action(c, c);
			return false;
	
	}
	
	public static void main(String[] args){
		Pion p1=new Pion(1);
		Pion p2=new Pion(0);
		Plateau p=new Plateau();
		System.out.print(p.getCase(p1.getIndice()).getNom()+ ":Voulez vous acheter ?(Si oui ,taper <<o>> )");
		p.getCase(1).action(p1, p2);
		/*
		 *  00 01 02 03 04 05 06 07 08 09 10 
		 *  39                            11
		 *  38                            12
		 *  37                            13
		 *  36                            14
		 *  35                            15
		 *  34                            16
		 *  33                            17
		 *  32                            18
		 *  31                            19
		 *  30 29 28 27 26 25 24 23	22 21 20
		 */
		System.out.println("OK,monopoly");
		
	}
}
