
/*
Rappel : pour compiler ou excéuter, se place au dessus du repertoire mcv
javac -d metier metier/JeuPileOuFace.java
java -cp metier metier.JeuPileOuFace
 */

import java.util.* ;

public class JeuPileOuFace
{
	private ArrayList<Character> humain;
	private ArrayList<Character> ordi;
	private int pointsHumain;
	private int pointsOrdi;
	private int numPartie;

	// réinitialise une partie
	public void init()
	{
		humain =new ArrayList<Character>();
		ordi =new ArrayList<Character>();
		pointsHumain=0;
		pointsOrdi=0;
		numPartie=0;
	}

	public boolean termine(){return pointsHumain==10 || pointsOrdi==10;}

	public int getPointsHumain(){return pointsHumain;}
	public int getPointsOrdi(){return pointsOrdi;}

	public char getLastHumain(){return humain.get(humain.size()-1).charValue();}
	public char getLastOrdi(){return ordi.get(ordi.size()-1).charValue();}


	public void play(char h)
	{

		char o;
		if (Math.random()>0.5){
			o='P';
		}
		else o='F';

		humain.add(new Character(h));
		ordi.add(new Character(o));
		if (h==o) pointsOrdi++;
		else pointsHumain++;
		numPartie++;

	}

	public static void main(String[] args) {
		JeuPileOuFace j = new JeuPileOuFace();
		j.init();
		Scanner s = new Scanner(System.in);
		System.out.println("rentrer P ou F");
		while (!j.termine()) {
			char c = s.nextLine().charAt(0);
			if(c!='P'||c!='F') System.out.println("Mauvaise saisie");
			else{
				j.play(c);
				System.out.println("points de l'ordi : "+j.getPointsOrdi());
				System.out.println("points du joueur : "+j.getPointsHumain());
				System.out.println("rentrer P ou F");
			}
		
			
		}
		if(j.getPointsHumain()==10) System.out.println("Le joueur à gagné");
		else System.out.println("L'ordi a gagné");
	}
}