<!DOCTYPE html>
<%@ page import="java.sql.*, metier.Jouer" %>
<html>
  <head>
    <%@ page pageEncoding="UTF-8" %>
    <title> PileOuFace </title>
		   
	<%
 		out.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>");
		out.println("</head><body>");
		out.println("<div class='jumbotron text-center'>");
		out.println(" <h1>Pile ou face</h1> <p>Le premier arrive a 10 a gagne</p><p>Deux faces identiques je gagne, deux faces differentes vous gagnez !</p>");
		
		out.println("<hr>");
       Jouer j;
       String coup = request.getParameter("coup");
       if(coup==null){
           out.println("<p>Nous allons debuter une nouvelle partie</p>");
           j = new Jouer();
           j.init();
           session.setAttribute("jouer",j);
       }
       else{
            j = (Jouer)session.getAttribute("jouer");
            j.play(coup.charAt(0));
            out.println("Vous venez de jouer "+j.getLastHumain()+" et l'ordi "+j.getLastOrdi());
       }
       out.println("Scores "+j.getPointsHumain()+" à "+j.getPointsOrdi());
       
       if(j.termine()){
           if(j.getPointsHumain()!=10){
            out.println("<h2> Vous avez perdu :-( </h2>");
           }else{
            out.println("<h2> Vous avez gagne :) </h2>");
           }
            out.println("<a href=\"jouer.jsp\">Reinit ?</a>");
           }
           else{
            out.println("Vous jouez <a href=\"jouer.jsp?coup=P\">Pile</a> ou <a href=\"jouer.jsp?coup=F\">Face</a> ?");
       }
		out.println("</div>");
	   %>
  </body>
</html>
