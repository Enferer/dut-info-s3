// Servlet Test.java  de test de la configuration
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/servlet/Test")
public class Test extends HttpServlet{

	private static final long serialVersionUID = 1L;

public void service( HttpServletRequest req, HttpServletResponse res ) 
       throws ServletException, IOException{
	
    PrintWriter out = res.getWriter();
    
    res.setContentType( "text/html" );

    out.println("<!doctype html>");
    out.println("<head><title>servlet Test</title></head><body><center> ");
    out.println("<p>yop</p>");
    out.println("</body></html> ");
  }
}
