import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/Main")
public class Main  extends HttpServlet{

	private static final long serialVersionUID = 1L;
	Connection con;
	
	
	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
		
		try {
		
			Class.forName("org.postgresql.Driver");
			
			String url = "jdbc:postgresql://localhost/site";
			String nom = "depommit";
			String mdp = "moi";
			con = DriverManager.getConnection(url, nom, mdp);
			Statement stmt = con.createStatement();
			
			String query = "select * from commentaire order by mno desc;";
			System.out.println(query);
			ResultSet r = stmt.executeQuery(query);
			//ResultSetMetaData rs = r.getMetaData();
			
			
			//###### PARTIE HTML
			PrintWriter out = res.getWriter();
			// Head etc....
			 out.println("<!doctype html>");
			 out.println("<head><title>Accueil</title>");
			 out.println("<meta name='viewport' content='width=device-width, initial-scale=1'>");
			 out.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>");
			 out.println("</head><body>");
			 out.println("<div class='jumbotron text-center'>");
			 out.println("<h1>Commentaires</h1>");
			 out.println("</div>");
			
			 
			 
			 // Affichage commentaire
			 out.println("<center><table class='table table-striped'>");
			 
			// Form pour insert nouveau commentaire
			 out.println("<tr><th>");
			 out.println("<center><form method='post' action='InsertCommentaire'>");
			 out.println("<label for='message'>Entrez votre message :</label><br>");
			 out.println("<textarea name='message' id='message' /></textarea>");
			 out.println("<input id='login' name='login' type='hidden' value='"+req.getParameter("pseudo")+"'>");
			 out.println("<input id='mdp' name='mdp' type='hidden' value='"+req.getParameter("mdp")+"'>");
			 out.println("<input type='submit' value='Ajouter' />");
			 out.println("</form></center>");
			 out.println("</th></tr>");
			 out.println("");
			 out.println("");
			 
			 
			 while(r.next()){
				 out.println("<tr><th><center><h4>"+r.getString(1)+"</h4></center></th></tr>");
				 out.println("<tr><th><p class='text-right'>"+r.getString(2)+"</p></th></tr>");
			 }
			 out.println("</table></center>");
			 // Fin affichage commentaire
			 
			 
			 out.println("</body></html> ");
			
			
			
		}catch(Exception e){e.printStackTrace();}
		finally {try{con.close();}catch(Exception e){}}
		
		
	}
}
