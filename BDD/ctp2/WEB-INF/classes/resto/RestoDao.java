package resto;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RestoDao {
	
	private Connection getConnection() {
		try {
			Class.forName("org.postgresql.Driver");
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost/template1", "depommit", "moi");
			return conn;
		} catch( ClassNotFoundException | SQLException e ) {
			throw new Error( e );
		}
	}
	
	private Plat createPlat(ResultSet rs) throws SQLException {
		Plat p = new Plat();
		p.setPno( rs.getInt("pno") );
		p.setLibelle( rs.getString("libelle") );
		p.setDescription( rs.getString("description") );
		p.setType( rs.getString("type") );
		return p;
	}
	
	public List<Plat> getPlats( String type ) {
		Connection conn = getConnection();
		
		try {
			Statement st = conn.createStatement();
			String query = "select * from plats where type = '"+type+"'";
			ResultSet rs = st.executeQuery(query);
			List<Plat> plats = new ArrayList<>();
			while( rs.next() ) {
				plats.add(createPlat(rs));
			}
			return plats;
		} catch( SQLException e ) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public Plat getPlat(int pno) {
		Connection conn = getConnection();
		
		try {
			Statement st = conn.createStatement();
			String query = "select * from plats where pno = "+pno;
			ResultSet rs = st.executeQuery(query);
			rs.next();
			return createPlat(rs);
		} catch( SQLException e ) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
