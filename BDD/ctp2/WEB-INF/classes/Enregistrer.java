import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import resto.Menu;

@WebServlet("/servlet/Enregistrer")
public class Enregistrer extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	Connection con;

	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		try{
			// Connection base de donné <--
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://psqlserv/n3p1";
			String nom = "depommit";
			String mdp = "moi";
			con = DriverManager.getConnection(url,nom,mdp);
			HttpSession session = req.getSession();
			
			if(req.getParameter("mail")!=null && req.getParameter("mail")!= null && !Secure.testInjection(req.getParameter("mail"))|| !Secure.testInjection(req.getParameter("date"))) {
				System.out.println("####### Tentative d'injection ######");
				//res.sendRedirect("../plats.jsp");
			}else {
							
				int eno = ((Menu)session.getAttribute("menu")).getEntree().getPno();
				int pno = ((Menu)session.getAttribute("menu")).getPlat().getPno();
				int dno = ((Menu)session.getAttribute("menu")).getDessert().getPno();
				String query = "insert into menu(entree,plat,dessert,mail,resa) values("+eno+","+pno+","+dno+",'"+req.getParameter("mail")+"','"+req.getParameter("date")+"');";
				Statement st = con.createStatement();
				st.execute(query);
				//st.executeQuery(query);
				session.invalidate();
			}
			res.sendRedirect("../plats.jsp");

		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{con.close();}catch(Exception e){}
		}
	}

}
