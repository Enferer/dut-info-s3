import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/Accueil")
public class Accueil extends HttpServlet{


	private static final long serialVersionUID = 1L;
	Connection con;

	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		   if(req.getSession().getAttribute("connection")==null 
				   || !req.getSession().getAttribute("connection").equals("true")){
			   res.sendRedirect("../accueil.html");
		   }
		   new Date();
			try{
				/*Class.forName("org.postgresql.Driver");
				
				
				String url = "jdbc:postgresql://localhost/site";
				String nom = "depommit";
				String mdp = "moi";
				con = DriverManager.getConnection(url, nom, mdp);*/
			
				PrintWriter out = res.getWriter();
				res.setContentType( "text/html" );
	
			    out.println("<!doctype html>");
			    out.println("<head><title>Accueil</title>");
			    out.println("<meta name='viewport' content='width=device-width, initial-scale=1'>");
			    out.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>");
			    out.println("</head><body>");
			    out.println("<div class='jumbotron text-center'>");
			    out.println("<h1>Menu principale</h1>");
			    out.println("</div>");
			    out.println("<form action=\"../date.jsp\"><input type=\"submit\" value=\"Test jsp\"></form>");
			    out.println("</body></html>");
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				try{con.close();}catch(Exception e){}
			}
	}
	
}
