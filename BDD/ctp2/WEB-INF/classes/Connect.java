import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/servlet/Connect")
public class Connect extends HttpServlet{
	

	private static final long serialVersionUID = 1L;
	Connection con;

	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		try{
			// Connection base de donné <--
			Class.forName("org.postgresql.Driver");

			String url = "jdbc:postgresql://localhost/site";
			String nom = "depommit";
			String mdp = "moi";
			con = DriverManager.getConnection(url, nom, mdp);
			Statement st = con.createStatement();
			// -->
			
			// Test securité injection <--
			if(!Secure.testInjection(req.getParameter("pseudo"))
					||!Secure.testInjection(req.getParameter("mdp"))){
				
				System.out.println("####### Tentative d'injection #######");
				res.sendRedirect("../accueil.html");
			
			}
			// -->
			
			// Requéte sql pour savoir si il y a un utilisateur avec le bon mdp
			String query = "select * from users where pseudo='"+req.getParameter("pseudo")+"' and mdp='"+req.getParameter("mdp")+"';";
		
			ResultSet rs = st.executeQuery(query);
			if(rs.next()){
				// Si les identifiants sont bon
				HttpSession session = req.getSession();
				session.setAttribute("connection", "true");
				res.sendRedirect("Accueil");
			}else{
				// Si il n'y a le bon utilisateur
				res.sendRedirect("../accueil.html");
			}


		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{con.close();}catch(Exception e){}
		}
	}

}
