import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/Inscription")
public class Inscription  extends HttpServlet{
	

	private static final long serialVersionUID = 1L;
	Connection con;

	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		try{
			
			// Connection à la base <--
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost/site";
			String nom = "depommit";
			String mdp = "moi";
			con = DriverManager.getConnection(url, nom, mdp);
			Statement st = con.createStatement();
			// -->
			
			// Test securité injection <--
			if(!Secure.testInjection(req.getParameter("pseudo"))
					||!Secure.testInjection(req.getParameter("mdp"))){
				System.out.println("####### Tentative d'injection #######");
				res.sendRedirect("../accueil.html");
			}
			// -->
			
			
			// Test si quelqu'un à déja le méme pseudo <--
			String query = "select * from users where pseudo='"+req.getParameter("pseudo")+"';";
			ResultSet rs = st.executeQuery(query);
			if(!rs.next()){
				query = "insert into users values('"+req.getParameter("pseudo")+"','"+req.getParameter("mdp")+"')";
				System.out.println(query);
				st.execute(query);
			}else{
				
			}
			// -->
			
			// Redirect aprés l'inscription ou en cas d'erreur

			res.sendRedirect("../accueil.html");
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{con.close();}catch(Exception e){}
		}
	}

}
