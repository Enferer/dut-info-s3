import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import resto.Menu;
import resto.Plat;

@WebServlet("/servlet/Choix")
public class Choix   extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		
		HttpSession session = req.getSession();
		if(req.getParameter("E")!=null) {
			System.out.println("###############");
			Menu m = (Menu)session.getAttribute("menu");
			Plat p = new Plat();
			p.setPno(Integer.valueOf(req.getParameter("E")));
			m.setEntree(p);

		}
		if(req.getParameter("P")!=null) {
			Menu m = (Menu)session.getAttribute("menu");
			Plat p = new Plat();
			p.setPno(Integer.valueOf(req.getParameter("P")));
			m.setPlat(p);
		}
		if(req.getParameter("D")!=null) {
			Menu m = (Menu)session.getAttribute("menu");
			Plat p = new Plat();
			p.setPno(Integer.valueOf(req.getParameter("D")));
			m.setDessert(p);
			//System.out.println(m.getDessert().getPno()+"#####");
		}
		

		res.sendRedirect("../plats.jsp");
		
	}

}
