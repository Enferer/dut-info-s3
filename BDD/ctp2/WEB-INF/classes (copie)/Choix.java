import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/servlet/Choix")
public class Choix   extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		
		if(req.getParameter("E")!=null) {
			HttpSession session = req.getSession();
			session.setAttribute("E", req.getParameter("E"));
		}
		if(req.getParameter("P")!=null) {
			HttpSession session = req.getSession();
			session.setAttribute("P", req.getParameter("P"));
		}
		if(req.getParameter("D")!=null) {
			HttpSession session = req.getSession();
			session.setAttribute("D", req.getParameter("D"));
		}
		res.sendRedirect("../plats.jsp");
		
	}

}
