package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/Inscription")
public class Inscription  extends HttpServlet{

	private static final long serialVersionUID = 1L;
	Connection con;
	
	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		try{
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://psqlserv/n3p1";
			String nom = "depommit";
			String mdp = "moi";
			con = DriverManager.getConnection(url, nom, mdp);
			Statement stmt = con.createStatement();
			
			String query = "insert into users values('"+req.getParameter("login")+"','"+req.getParameter("mdp")+"','"+req.getParameter("pseudo")+"','"+getDate()+"');";
			System.out.println(query);
			
			boolean requeteCorrect =  stmt.execute(query);
			if(requeteCorrect) res.sendRedirect("../Accueil.html");
			
			PrintWriter out = res.getWriter();
			out.println("<h1> Information incorrect - L'inscription à échoué</h1>");
			Thread.sleep(2000);
			res.sendRedirect("../Accueil.html");
			
		}catch(Exception e){e.printStackTrace();}
		finally {
			try{con.close();}catch(Exception e){}
		}
	}

	private static String getDate(){
		Date d = new Date();
		String res = "";
		res += d.getYear()+1900+"-";
		res += d.getMonth()+"-";
		res += d.getDay()+" ";
		res += d.toString().split(" ")[3];
		return res;
	}
	
	public static void main(String[] args) {
		System.out.println(getDate());
	}
}
