import java.sql.*;

public class Create{  
  public static void main(String args[]){      
      
	
		Connection con = null;
		try{
			
			
			// enregistrement du driver
			  Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			  
			  // connexion à la base
			String url = "jdbc:odbc:acces";
			String nom = "depommit";
			String mdp = "moi";
			con = DriverManager.getConnection(url,nom,mdp);
			 

			  // execution de la requete
			  Statement stmt = con.createStatement();
			  stmt.executeUpdate("create table CLIENTS (NOM varchar(10), PRENOM varchar(10), AGE int)");


			  // fermeture des espaces
		  }
		  catch(Exception e){
			  System.out.print(e.getMessage());
		  }
		  finally{
			  try{con.close();}catch(Exception e){}
		  }

		  
  }
}