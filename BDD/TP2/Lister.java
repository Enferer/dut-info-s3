import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.sql.*;

public class Lister {
	public static void main(final String[] args) {

		Properties prop = new Properties();
		InputStream input = null;
		Connection con = null;

		try {
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			input = new FileInputStream("option.properties");


			prop.load(input);


			String url = prop.getProperty("url");
			String nom = prop.getProperty("username ");
			String mdp = prop.getProperty("password ");
			
			con = DriverManager.getConnection(url,nom,mdp);
			
			Statement state = con.createStatement();
			ResultSet rs = state.executeQuery("SELECT * FROM "+args[0]);
			ResultSetMetaData resultMeta = rs.getMetaData();
			int nbrColone = resultMeta.getColumnCount();
			String s ="";
			
			while (rs.next()) 
			{
              for(int i= 1; i <= nbrColone; i++){
				  s = rs.getString(i);
				  System.out.print("\t"+s+"\t");
			  }
			  System.out.println("");
			}
		}

	    catch (Exception e) {
			e.printStackTrace();
		} finally {
			try{
				input.close();
				con.close();
			}catch(Exception e){}
		}

	}
}