import java.sql.*;

public class Select
{
  public static void main(String args[]) throws Exception
  {
      Connection con=null;
      Statement stmt;
      
	  try{
		  Class.forName("org.postgresql.Driver");

		  String url = "jdbc:postgresql://psqlserv/n3p1";
		  String nom = "depommit";
		  String mdp = "moi";
		  con = DriverManager.getConnection(url,nom,mdp);
		  stmt = con.createStatement();
		  String query = "select*from Clients";
		ResultSet rs = stmt.executeQuery(query);
		ResultSetMetaData rsmd = rs.getMetaData();
		int nbCols = rsmd.getColumnCount();
		System.out.println("Cette table contient "+ nbCols + " colonnes");
		for (int i = 1; i <= nbCols; i++){
			System.out.println("Colonne "+ i);
			System.out.println("Nom  : " + rsmd.getColumnName(i));
			System.out.println("Type : " +  rsmd.getColumnTypeName(i));

			System.out.println("");
		}

		  
	  }
	  catch(Exception e){System.out.println(e);}
	  finally{
		  try{con.close();}catch(Exception e){}
	  }
      
  } 
}
