
import java.sql.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/servlet/Select")
public class Select extends HttpServlet
{

	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
		{
			Connection con=null;

			try{
				res.setContentType("text/html;charset=UTF-8");
				PrintWriter out = res.getWriter();

				Class.forName("org.postgresql.Driver");

				String url = "jdbc:postgresql://psqlserv/n3p1";

				String nom = "depommit";
				String mdp = "moi";
				con = DriverManager.getConnection(url,nom,mdp);


				Statement state = con.createStatement();



					out.println( "<head><title>servlet first</title></head><body><center>" );
					out.println( "<META content=\"charset=UTF-8\"></head><body><center>" );
					out.println("<form action='Select' method='get'>");
					out.println("<label for='table'>Table :</label> <input id='table' type='text' name='table'>");
					out.println("<input type='submit' value='Selectionner'>"); 
					out.println("");
					out.println("");
					out.println("</form>");
				
				if (!(req.getParameter("table") == null || req.getParameter("table").equals("")) && isTable(req.getParameter("table"),state)) {
						ResultSet result = state.executeQuery("SELECT * FROM "+req.getParameter("table"));
						ResultSetMetaData resultMeta = result.getMetaData();

						out.println(" <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");

						out.println("<table class=\"container table-hover table-condensed\"><tr>");
						for(int i = 1; i <= resultMeta.getColumnCount(); i++){
							out.print("<td>" + resultMeta.getColumnName(i).toUpperCase() + "</td>");
						}
						out.println("</tr>");
						while(result.next()){         
							out.println("<tr>");
							for(int i = 1; i <= resultMeta.getColumnCount(); i++){
								out.print("<td>" + result.getString(i)+"</td>");
							}
							
							//System.out.println("---"+"DeleteExecute?table="+req.getParameter("table")+"&cle='"+result.getString(1)+"'");
							out.println("<td><a href=\"DeleteExecute?table="+req.getParameter("table")+"&cle='"+result.getString(1)+"'\">Clique</a></td>");
							out.println("</tr>");



						}
						out.println("<tr>");
						
						out.println("<form action='Insert' method='get'>");
						for(int i = 1; i <= resultMeta.getColumnCount(); i++){
							out.print("<td><input id='table' type='text' name='champ"+i+"' value='"+resultMeta.getColumnLabel(i)+"'></td>");
						}
						//out.println("<input id='table' type='text' name='table'>");
						out.println("<input type='hidden' value='"+req.getParameter("table")+"' name='table'>"); 
						out.println("<input type='submit' value='Selectionner'>"); 
						out.println("");
						out.println("</form>");
						out.println("</table></body>");
						out.println("<tr>");
				}

			}

			catch(Exception e){e.printStackTrace();}
			finally{
				try{con.close();}catch(Exception e){}
			}


		} 
	}

	@SuppressWarnings("unused")
	private boolean isTable(String parameter,Statement state) {
		try {
			ResultSet test = state.executeQuery("SELECT * FROM "+parameter);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
