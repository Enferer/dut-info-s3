import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/DeleteExecute")
public class DeleteExecute extends HttpServlet{

	Connection con;
	
	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
	
		try{
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://psqlserv/n3p1";
			String nom = "depommit";
			String mdp = "moi";
			con = DriverManager.getConnection(url,nom,mdp);
			Statement state = con.createStatement();
			
			
			
			// On récupére le nom de la table 
			System.out.println("************************"+req.getParameter("table"));
			ResultSet result = state.executeQuery("SELECT * FROM "+req.getParameter("table"));
			ResultSetMetaData resultMeta = result.getMetaData();
			String nomCol = resultMeta.getColumnName(1);
			
			
			//On tente d'executer la commande sql
			System.out.println("DELETE FROM "+req.getParameter("table")+" WHERE "+nomCol+"="+req.getParameter("cle"));
			state.executeUpdate("DELETE FROM "+req.getParameter("table")+" WHERE "+nomCol+"="+req.getParameter("cle"));
			
			
		}
		catch(Exception e){
			//System.out.println("***************"+e.toString());
			e.printStackTrace();
		}finally {
			try {con.close();} catch (SQLException e) {}
		}

			res.sendRedirect("Select?table="+req.getParameter("table"));

		
		
	}
	
	
}
