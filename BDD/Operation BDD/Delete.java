import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/servlet/Delete")
public class Delete extends HttpServlet{
	

	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
		
		res.setContentType("text/html;charset=UTF-8");
		PrintWriter out = res.getWriter();
		
		out.println("<head>");
		out.println("<title>Delete Servlet</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<center><h1>Servlet DELETE</h1></center>");
		out.println("<hr>");

		//Début du formulaire		
		out.println("<form action='DeleteExecute' method='get'>");
		out.println("<p>Nom de la table</p>");
		out.println("<input type='text' name='table'>");
		out.println("<p>Clé de la ligne à supprimer</p>");
		out.println("<input type='text' name='cle'><br><br>");
		out.println("<input type='submit' value='Delete'>");
		out.println("</form>");
		//Fin du formulaire
		
		
		
		out.println("</body>");
		
		
	}



}
