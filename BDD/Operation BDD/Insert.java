// Servlet Test.java  de test de la configuration
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/servlet/Insert")
public class Insert extends HttpServlet
{
	Connection con;
	
  public void service( HttpServletRequest req, HttpServletResponse res ) 
       throws ServletException, IOException
  {
	  
	  
	try {
		Class.forName("org.postgresql.Driver");

		String url = "jdbc:postgresql://psqlserv/n3p1";

		String nom = "depommit";
		String mdp = "moi";
		con = DriverManager.getConnection(url,nom,mdp);

		PrintWriter out = res.getWriter();

		Statement state = con.createStatement();
		
		ResultSet result = state.executeQuery("Select * from "+req.getParameter("table"));
		ResultSetMetaData resultMeta = result.getMetaData();
		
		
		

		
		String values = "";
		for (int i = 1; i <= resultMeta.getColumnCount(); i++) {
			try {
				values += Integer.valueOf(req.getParameter("champ"+i)).toString()+",";
			} catch (Exception e) {
				values += "'"+req.getParameter("champ"+i)+"',";
			}
		}
		values = values.substring(0, values.length()-1);
		
		try {
			System.out.println("**********************INSERT INTO "+req.getParameter("table")+" VALUES ("+values+")");
			state.executeQuery("INSERT INTO "+req.getParameter("table")+" VALUES("+values+")");
			
		} catch (Exception e) {
			System.out.println("**********************Catch : SQL " + e);
		}
		
	} catch (Exception e) {
		
	}finally {
		try {con.close();} catch (SQLException e) {}
	}
	res.sendRedirect("Select?table="+req.getParameter("table"));

  }

}
