import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/servlet/UpdateSaisie")
public class UpdateSaisie extends HttpServlet {

	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
		
		res.setContentType("text/html;charset=UTF-8");
		PrintWriter out = res.getWriter();
	
		out.println("<form action='Update' method='get'>");
		out.println("<p>Nom de la table</p>");
		out.println("<input type='text' name='table'>");
		out.println("<p>Clé de la ligne à supprimer</p>");
		out.println("<input type='text' name='cle'><br><br>");
		out.println("<input type='submit' value='SaisieUpDate'>");
		out.println("</form>");
		
		
	}
}
