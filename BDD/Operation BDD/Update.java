import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/Update")
public class Update  extends HttpServlet{

	Connection con;

	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{


		try {
			res.setContentType("text/html;charset=UTF-8");
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://psqlserv/n3p1";
			String nom = "depommit";
			String mdp = "moi";
			con = DriverManager.getConnection(url,nom,mdp);
			Statement state = con.createStatement();

			
			PrintWriter out = res.getWriter();
			System.out.println("SELECT * FROM "+req.getParameter("table"));
			ResultSet result = state.executeQuery("SELECT * FROM "+req.getParameter("table"));
			ResultSetMetaData resultMeta = result.getMetaData();
			result.next();	
			String col = resultMeta.getColumnName(1);
			System.out.println("############SELECT * FROM "+req.getParameter("table")+" WHERE "+col+"="+req.getParameter("cle"));
			result = state.executeQuery("SELECT * FROM "+req.getParameter("table")+" WHERE "+col+"="+req.getParameter("cle"));
			resultMeta = result.getMetaData();
			
			result.next();
			out.println("<form action='UpdateExecute' method='get'>");
			for(int i = 1; i <= resultMeta.getColumnCount(); i++){
				String s = result.getString(i);
				out.println("<input type='text' name='"+s+"' value='"+s+"'>");
				
			}
			
			String colones ="";
			
			for (int i = 1; i <= resultMeta.getColumnCount(); i++) {
				colones += resultMeta.getColumnName(i)+"#";
			}
			
			out.println("<input type='hidden' name='colone' value='"+colones+"'>");
			out.println("<input type='submit' value='Submit'>");
			out.println("</form>");

		}
		catch(Exception e){
			e.printStackTrace();
			res.sendRedirect("UpdateSaisie");
		}
		finally {
			try{con.close();}catch(Exception e){};
		}

	}

}
