import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/Accueil")
public class Accueil extends HttpServlet{


	private static final long serialVersionUID = 1L;

	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		   
			PrintWriter out = res.getWriter();
			res.setContentType( "text/html" );

		    out.println("<!doctype html>");
		    out.println("<head><title>Accueil</title>");
		    out.println("<meta name='viewport' content='width=device-width, initial-scale=1'>");
		    out.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>");
		    out.println("</head><body>");
		    out.println("<div class='jumbotron text-center'>");
		    out.println("<h1>Bienvenue</h1>");
		    out.println("</div>");
		    out.println("<hr>");
		    // CONNEXION
		    out.println("<div class='container'>");
		    out.println("<div class='row'>");
		    out.println("<div class='col-sm-4'>");
		    out.println("<h3> CONNEXION </h3>");
		    out.println("<form method='post' action='Login'>");
		    out.println("<label for='pseudo'>Votre pseudo :</label>");
		    out.println("<input type='text' name='login' id='login' />");
		    out.println("<br>");
		    out.println("<label for='pass'>Votre mot de passe :</label>");
		    out.println("<input type='password' name='mdp' id='mdp' />");
		    if(req.getParameter("erreur")!=null) out.println("<p style='color:#FF0000';>Information incorrect</p>");
		    out.println("<input type='submit' value='Connexion' />");
		    out.println("</form>");
		    out.println("<hr>");
		    out.println("</div>");
		    // INSCRIPTION
		    out.println("<div class='col-sm-4'></div>");
		    out.println("<div class='col-sm-4'>");
		    out.println("<h3> INSCRIPTION </h3>");
		    out.println("<form method='post' action='Inscription'>");
		    out.println("<label for='pseudo'>Votre pseudos :</label>");
		    out.println("<input type='text' name='login' id='login' />");
		    out.println("<br>");
		    out.println("<label for='pass'>Votre mot de passe :</label>");
		    out.println("<input type='password' name='mdp' id='mdp' />");
		    out.println("<br>");
		    out.println("<label for='nom'>Votre nom :</label>");
		    out.println("<input type='text' name='nom' id='nom' />");
		    out.println("<br>");
		    out.println("<label for='prenom'>Votre prenom :</label>");
		    out.println("<input type='text' name='prenom' id='prenom' />");
		    out.println("<br>");
		    out.println("<input type='submit' value='Inscription' />");
		    out.println("</form>");
		    out.println("<hr>");
		    out.println("</div></div></div>");
		    out.println("</body></html> ");
		  }
	
}
