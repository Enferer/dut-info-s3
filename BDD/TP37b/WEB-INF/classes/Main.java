import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/servlet/Main")
public class Main  extends HttpServlet{

	private static final long serialVersionUID = 1L;
	Connection con;
	
	
	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
		
		try {
			
			HttpSession session = req.getSession(true);
			if (session.getAttribute("login")==null) res.sendRedirect("Accueil");
		
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://psqlserv/n3p1";
			String nom = "depommit";
			String mdp = "moi";
			
			con = DriverManager.getConnection(url, nom, mdp);
			
			Statement stmt = con.createStatement();
			
			//String query = "select * from users where login='"+session.getAttribute("login")+"';";
			String query;
			if(session.getAttribute("role")!= null && session.getAttribute("role").equals("admin")) query = "select * from users;";
			else query = "select * from users where login='"+session.getAttribute("login")+"';";
			
			System.out.println(query);
			ResultSet r = stmt.executeQuery(query);			
			
			
			//###### PARTIE HTML
			PrintWriter out = res.getWriter();
			// Head etc....
			 out.println("<!doctype html>");
			 out.println("<head><title>Main</title>");
			 out.println("<meta name='viewport' content='width=device-width, initial-scale=1'>");
			 out.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>");
			 out.println("</head><body>");
			 out.println("<div class='jumbotron text-center'>");
			 out.println("<h1>Profil</h1>");
			 out.println("</div>");
			 
			 //Body
			 out.println("<table class='table'>");
			 out.println("<tr>");
			 out.println("<td>Login </td>");
			 out.println("<td>Nom</td>");
			 out.println("<td>Prenom</td>");
			 out.println("</tr>");
			 
			 while(r.next()){
				 
				 
				 out.println("<tr>");

				 out.println("<td>"+r.getString(1)+"</td>");
				 out.println("<td>"+r.getString(3)+"</td>");
				 out.println("<td>"+r.getString(4)+"</td>");
				 out.println("</tr>");

			 }
			 out.println("</table>");

			 
			 	out.println("<a href='Modif'>Modifier information</a>");
			   out.println("<br><form method='post' action='Kill'>");
			    out.println("<input type='submit' value='Deconnexion' />");
			    out.println("</form>");
			 
			 out.println("</body></html> ");
			
			
			
		}catch(Exception e){e.printStackTrace();}
		finally {try{con.close();}catch(Exception e){}}
		
		
	}
}
