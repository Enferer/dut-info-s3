import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/servlet/Admin")
public class Admin extends HttpServlet{

	private static final long serialVersionUID = 1L;
	Connection con;
	
	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
		try{
			
			Class.forName("org.postgresql.Driver");
			
			String url = "jdbc:postgresql://psqlserv/n3p1";
			String nom = "depommit";
			String mdp = "moi";
			
			con = DriverManager.getConnection(url, nom, mdp);
			HttpSession session = req.getSession(true);
			Statement stmt = con.createStatement();
			String query = "select * from users where login='"+session.getAttribute("login")+"';";
			System.out.println(query);
			ResultSet r = stmt.executeQuery(query);			
			
			if(r.next() == false) res.sendRedirect("Accueil");
			
			if(!r.getString(5).equals("admin")) res.sendRedirect("Accueil");
			
			PrintWriter out = res.getWriter();
			out.println("Oui tu est admin !");
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try{con.close();}catch(Exception e){}
		}
		
	}
}
