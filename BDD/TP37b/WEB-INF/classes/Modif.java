

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/servlet/Modif")
public class Modif extends HttpServlet{

	
	private static final long serialVersionUID = 1L;
	Connection con;

	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
		
		try{
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://psqlserv/n3p1";
			String nom = "depommit";
			String mdp = "moi";
			
			con = DriverManager.getConnection(url, nom, mdp);
			
			HttpSession session = req.getSession(true);
			Statement stmt = con.createStatement();
			String query;
			
			if(session.getAttribute("role") != null && session.getAttribute("role").equals("admin")) query = "select * from users;";
			else query = "select * from users where login='"+session.getAttribute("login")+"';";
			
			System.out.println(query);
			ResultSet r = stmt.executeQuery(query);			
			
			
			PrintWriter out = res.getWriter();		   
			out.println("<!doctype html>");
		    out.println("<head><title>Accueil</title>");
		    out.println("<meta name='viewport' content='width=device-width, initial-scale=1'>");
		    out.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>");
		    out.println("</head><body>");
		    out.println("<div class='jumbotron text-center'>");
		    out.println("<h1>Modification des informations</h1>");
		    out.println("</div>");
		    out.println("<hr>");
		    
		    while(r.next()){
			    out.println("<form method='post' action='Update'>");
			    out.println("<label for='pseudo'>Votre nouveau pseudos :</label>");
			    out.println("<input type='text' name='login' id='login' value='"+r.getString(1)+"'/>");
			    out.println("<br>");
			    out.println("<label for='pass'>Votre nouveau mot de passe :</label>");
			    out.println("<input type='password' name='mdp' id='mdp' value='"+r.getString(2)+"'/>");
			    out.println("<br>");
			    out.println("<label for='nom'>Votre nouveau nom :</label>");
			    out.println("<input type='text' name='nom' id='nom' value='"+r.getString(3)+"'/>");
			    out.println("<br>");
			    out.println("<label for='prenom'>Votre nouveau prenom :</label>");
			    out.println("<input type='text' name='prenom' id='prenom' value='"+r.getString(4)+"'/>");
			    out.println("<input type='hidden' name='tlogin' id='tlogin' value='"+r.getString(1)+"'/>");
			    out.println("<br>");
			    out.println("<input type='submit' value='Modifier' />");
			    out.println("</form>");
		    }

		    
		    
		    	    
		    
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try{con.close();}catch(Exception e){}
		}

	}

}
