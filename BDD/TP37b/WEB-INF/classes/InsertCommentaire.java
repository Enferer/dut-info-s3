import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/InsertCommentaire")
public class InsertCommentaire  extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	Connection con;
	
	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
	
			try {
				Class.forName("org.postgresql.Driver");
				String url = "jdbc:postgresql://psqlserv/n3p1";
				String nom = "depommit";
				String mdp = "moi";
				con = DriverManager.getConnection(url, nom, mdp);
				Statement stmt = con.createStatement();
				
				String query = "insert into commentaire values('"+req.getParameter("message")+"', '"+req.getParameter("login")+"');";
				System.out.println(query);
				stmt.execute(query);
			}catch (Exception e) {e.printStackTrace();}
			finally {
				try{con.close();}catch(Exception e){e.printStackTrace();}
				res.sendRedirect("Main?pseudo="+req.getParameter("login")+"&mdp="+req.getParameter("mdp"));
			}
	
	}

}
