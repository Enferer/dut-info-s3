import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/servlet/Compteur")
public class Compteur extends HttpServlet
{

	private static final long serialVersionUID = 1L;
	int cptGlobal = 0;

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		this.cptGlobal++;
		HttpSession session = req.getSession( true );
		Integer cpt = (Integer)session.getAttribute( "compteur" );
		cpt = new Integer( cpt == null ? 1 : cpt.intValue() + 1 );
		session.setAttribute( "compteur", cpt );
		res.setContentType("text/html;charset=UTF-8");
		PrintWriter out = res.getWriter();
		out.println("<head> <title>Implémenter un compteur</title>");
		out.println(" </head> <body>");
		out.println("<h1>Vous avez accédé "+ cpt + " fois à cette page sur les "+cptGlobal+" accés au total..</h1>");
		out.println("</body>");
	}
}