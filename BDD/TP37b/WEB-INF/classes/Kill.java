import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/servlet/Kill")
public class Kill extends HttpServlet{

	private static final long serialVersionUID = 1L;
	Connection con;
	
	public void service( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
		HttpSession session = req.getSession(true);
		session.invalidate();
		res.sendRedirect("Accueil");
	}
}