<!DOCTYPE html>
<%@ page import="java.sql.*, product.*, java.util.ArrayList;" %>
<html>
<head>
	<%@ page pageEncoding="UTF-8" %>
	<title> MaPage </title>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
</head>
	<body>
	<div class='jumbotron text-center'>
	<h1>Bienvenue</h1>
	</div>
	<hr>
	<%! Connection con = null; %>
	<%
		try{
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://psqlserv/n3p1";
			String nom = "depommit";
			String mdp = "moi";
			con = DriverManager.getConnection(url,nom,mdp);
			
			ProductDAO p = new ProductDAO();
		    if(request.getParameter("pno")== null){
				 out.println(ProductView.getHTML(p.findAll()));  
		    }else{
		    	ArrayList<Product> l = new ArrayList<>();
		    	l.add(p.findById(Integer.valueOf(request.getParameter("pno"))));
		    	 out.println(ProductView.getHTML(l));  
		    }

		}catch(Exception e){ e.printStackTrace();}
		finally{
			try {con.close();}catch(Exception e){}
		}
	%>

</body>
</html>
