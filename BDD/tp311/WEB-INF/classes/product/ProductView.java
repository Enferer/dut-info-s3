package product;
import java.util.List;

public class ProductView {
	
	
	public static String getHTML(List<Product> l ){
		String res = "<table class='table table-hover'>";
		for (Product product : l) {
			res+="<tr>";
			res+="<td>"+product.getPno()+"</td>";
			res+="<td>"+product.getLibelle()+"</td>";
			res+="<td>"+product.getDescription()+"</td>";
			res+="<td><img src='"+product.getUrlImg()+"' width=100 height=100></td>";
			res+="<td>"+product.getPrix()+" euro</td>";
			res+="</tr>";
		}
		res+="</table>";
		return res;
	}

}
