package product;
public class Product {

	private int pno;
	private String libelle;
	private String description;
	private String urlImg;
	private int prix;
	
	
	public int getPno() {return pno;}
	public String getLibelle() {return libelle;}
	public String getDescription() {return description;}
	public String getUrlImg() {return urlImg;}
	public int getPrix() {return prix;}
	
	
	public Product(int pno, String libelle, String description, String urlImg, int prix) {
		super();
		this.pno = pno;
		this.libelle = libelle;
		this.description = description;
		this.urlImg = urlImg;
		this.prix = prix;
	}
	@Override
	public String toString() {
		return "Product [pno=" + pno + ", libelle=" + libelle + ", description=" + description + ", urlImg=" + urlImg
				+ ", prix=" + prix + "]\n";
	}
	
	
	
	
	
}
