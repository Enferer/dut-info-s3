package product;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/Accueil")
public class Accueil extends HttpServlet{


	private static final long serialVersionUID = 1L;
	
	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		   
		
		
		
			PrintWriter out = res.getWriter();
			res.setContentType( "text/html" );

		    out.println("<!doctype html>");
		    out.println("<head><title>Accueil</title>");
		    out.println("<meta name='viewport' content='width=device-width, initial-scale=1'>");
		    out.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>");
		    out.println("</head><body>");
		    out.println("<div class='jumbotron text-center'>");
		    out.println("<h1>Bienvenue</h1>");
		    out.println("</div>");
		    out.println("<hr>");
		    ProductDAO p = new ProductDAO();
		    if(req.getParameter("pno")== null){
				 out.println(ProductView.getHTML(p.findAll()));  
		    }else{
		    	ArrayList<Product> l = new ArrayList<>();
		    	l.add(p.findById(Integer.valueOf(req.getParameter("pno"))));
		    	 out.println(ProductView.getHTML(l));  
		    }
		    out.println("</body></html> ");
			
		  }
	
}
