package product;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO {

	
	private Connection getConnection(){
		try{
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://psqlserv/n3p1";
			String nom = "depommit";
			String mdp = "moi";
			return DriverManager.getConnection(url,nom,mdp);
		}catch(Exception e){e.printStackTrace();}
		return null;
	}
	
	public List<Product> findAll(){
		ArrayList<Product> res = new ArrayList<Product>();
		Connection con  = getConnection();
		try{	
			Statement s = con.createStatement();
			String query = "select * from product;";
			ResultSet rs = s.executeQuery(query);
			while(rs.next()){
				res.add(new Product(Integer.valueOf(rs.getString(1)), rs.getString(2), rs.getString(3), rs.getString(4), Integer.valueOf(rs.getString(5))));
			}
		}catch(Exception e){e.printStackTrace();}
		finally {
			try {con.close();}catch(Exception e){}
		}
		
		
		return res;
	}
	
	public Product findById(int pno){
		List<Product> l = findAll();
		for (Product product : l) {
			if(product.getPno() == pno) return product;
		}
		return null;
	}
	
	
	public static void main(String[] args) {
		ProductDAO p = new ProductDAO();
		System.out.println(p.findAll());
		
		System.out.println("\n\n"+p.findById(2));
	}
	
}
