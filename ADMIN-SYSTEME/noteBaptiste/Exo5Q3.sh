#!/bin/bash
serveur=192.168.194.200

for login in $(getent group info-n1p3 | cut -d : -f 4 | tr , \\n | grep '^[aeiouy]*')
do
	echo deluser --remove-home $login
done | ssh root@serveur "cat | bash"
