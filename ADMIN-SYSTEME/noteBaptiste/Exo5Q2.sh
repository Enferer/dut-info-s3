#!/bin/bash
serveur=192.168.194.200

for login in $(getent group info-n1p3 | cut -d : -f 4 | tr , ' ')
do
	echo usermod -L $login
done | ssh root@serveur "cat | bash"

for login in $(getent group info-n1p3-l | cut -d : -f 4 | tr , ' ')
do
	echo usermod -U $login
done | ssh root@serveur "cat | bash"
