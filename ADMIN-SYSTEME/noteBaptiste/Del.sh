#!/bin/bash

for login in $(getent group info-fi2-k | cut -d : -f 4 | tr , ' ')
do
	echo userdel -r $login
done | ssh root@192.168.194.200 "cat | bash"
