// DEPOMMIER Thibaut GROUPE k AYOU18
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>




int envoyer_donnees(int in, int out){
	int BUFFER_SIZE = 1000;
	char buffer[BUFFER_SIZE];
	int count;
	while((count=read(in, buffer, BUFFER_SIZE)) > 0){
		int c = write(out, buffer, count);
		if(c== -1) {
			perror("write");
			return -1;
		}
	}
	return 0;
}

int lance_commande(int in,const char *commande){
	//char *temp[] = {commande,NULL};
	int pid = fork();
	if (pid == 0){ // FILS
	   	close(0);
	    dup(in);
	    if(execlp(commande, commande, NULL)== -1){
	 		perror(commande);
	 		return -1;
		}	
   	}else if(pid==-1){
		perror("fork");
		return -1;
	}
   	return pid;
}

int lance_traitement(int in,char *commande){
  	int descripteur[2];
	if (pipe(descripteur)==-1){
		// GESTION ERREU CREATION PIPE
		perror("Pipe ");
		return -1;
	}
	int pid = fork();
	if(pid==-1){
		// GESTION ERREU FORK
		perror("Fork ");
		return -1;
	}
	wait(NULL);
	if(pid==0){
		envoyer_donnees(in,descripteur[1]);
		close(descripteur[1]);
		lance_commande(descripteur[0], commande);
		exit(0);
	}
	return 0;
  

}

int main(int argc, char** argv) {
  	int in;
  	if(argc == 3){
    	in = open(argv[2],O_RDONLY);
  	}else{
	   	printf("Erreur paramétre");
  	}
  	if(strcmp(argv[1], "-c")==0){
	  	lance_traitement(in,"cat");
  	}
	      
  	else if(strcmp(argv[1], "-h")==0){
	  	lance_traitement(in,"head");
  	}

  	else if(strcmp(argv[1], "-t")==0){
	  	lance_traitement(in,"tail");
  	}
	    
  	else if(strcmp(argv[1], "-w")==0){
	  	lance_traitement(in,"wc");
  	}
  	return 1;
}
