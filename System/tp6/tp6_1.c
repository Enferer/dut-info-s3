#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "tp6.h"


int lire_deux_octets(int fd, uint16 *val){
	return read(fd,val,2);
}

int lire_quatre_octets(int fd, uint32 *val){
	return read(fd,val,4);
}

int ecrire_deux_octets(int fd, uint16 val){
	return write(fd, &val, 2);
}

int ecrire_quatre_octets(int fd, uint32 val){
	return write(fd, &val, 4);
}

int lire_entete(int de,entete_bmp *entete){
	int cpt=0;
	cpt+=lire_deux_octets(de, &entete->fichier.signature);
	cpt+=lire_quatre_octets(de, &entete->fichier.taille_fichier);
	cpt+=lire_quatre_octets(de, &entete->fichier.reserve);
	cpt+=lire_quatre_octets(de, &entete->fichier.offset_donnees);

	cpt+=lire_quatre_octets(de, &entete->bitmap.taille_entete);
	cpt+=lire_quatre_octets(de, &entete->bitmap.largeur);
	cpt+=lire_quatre_octets(de, &entete->bitmap.hauteur);
	cpt+=lire_deux_octets(de, &entete->bitmap.nombre_plans);
	cpt+=lire_deux_octets(de, &entete->bitmap.profondeur);
	cpt+=lire_quatre_octets(de, &entete->bitmap.compression);
	cpt+=lire_quatre_octets(de, &entete->bitmap.taille_donnees_image);
	cpt+=lire_quatre_octets(de, &entete->bitmap.resolution_horizontale);
	cpt+=lire_quatre_octets(de, &entete->bitmap.resolution_verticale);
	cpt+=lire_quatre_octets(de, &entete->bitmap.taille_palette);
	cpt+=lire_quatre_octets(de, &entete->bitmap.nombre_de_couleurs_importantes);
	return cpt;
}

int ecrire_entete(int vers,const entete_bmp *entete){
	int cpt=0;
	cpt+=ecrire_deux_octets(vers, entete->fichier.signature);
	cpt+=ecrire_quatre_octets(vers, entete->fichier.taille_fichier);
	cpt+=ecrire_quatre_octets(vers, entete->fichier.reserve);
	cpt+=ecrire_quatre_octets(vers, entete->fichier.offset_donnees);

	cpt+=ecrire_quatre_octets(vers, entete->bitmap.taille_entete);
	cpt+=ecrire_quatre_octets(vers, entete->bitmap.largeur);
	cpt+=ecrire_quatre_octets(vers, entete->bitmap.hauteur);
	cpt+=ecrire_deux_octets(vers, entete->bitmap.nombre_plans);
	cpt+=ecrire_deux_octets(vers, entete->bitmap.profondeur);
	cpt+=ecrire_quatre_octets(vers, entete->bitmap.compression);
	cpt+=ecrire_quatre_octets(vers, entete->bitmap.taille_donnees_image);
	cpt+=ecrire_quatre_octets(vers, entete->bitmap.resolution_horizontale);
	cpt+=ecrire_quatre_octets(vers, entete->bitmap.resolution_verticale);
	cpt+=ecrire_quatre_octets(vers, entete->bitmap.taille_palette);
	cpt+=ecrire_quatre_octets(vers, entete->bitmap.nombre_de_couleurs_importantes);
	return cpt;
}

int verifier_entete(entete_bmp *entete){
	if(entete->bitmap.profondeur==24){
		return 1;
	}
	return 0;
}

unsigned char* allouer_pixels(entete_bmp *entete){
	uint32 x = 3*entete->bitmap.largeur;
	uint32 y = 3*entete->bitmap.hauteur;
	int taille = x * y;
	unsigned char* res = (unsigned char *) malloc(taille * sizeof(char *));
	return res;
}

