#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "tp6.h"
#include <unistd.h>

int main(int argc, char **argv) {
	int fd= open(argv[argc-1], O_RDONLY);
	entete_bmp ent_bmp;
	lire_entete(fd, &ent_bmp);
	if(verifier_entete(&ent_bmp)==0){
		printf("Erreur entete\n");
		return 0;
	}
	printf("signature: %d\n", ent_bmp.fichier.signature);
	printf("taille_fichier: %d\n", ent_bmp.fichier.taille_fichier);
	printf("reserve: %d\n", ent_bmp.fichier.reserve);
	printf("offset_donnees: %d\n", ent_bmp.fichier.offset_donnees);
	printf("taille_entete: %d\n", ent_bmp.bitmap.taille_entete);
	printf("largeur: %d\n", ent_bmp.bitmap.largeur);
	printf("hauteur: %d\n", ent_bmp.bitmap.hauteur);
	printf("nombre_plans: %d\n", ent_bmp.bitmap.nombre_plans);
	printf("profondeur: %d\n", ent_bmp.bitmap.profondeur);
	printf("compression: %d\n", ent_bmp.bitmap.compression);
	printf("taille_donnees_image: %d\n", ent_bmp.bitmap.taille_donnees_image);
	printf("resolution_horizontale: %d\n", ent_bmp.bitmap.resolution_horizontale);
	printf("resolution_verticale: %d\n", ent_bmp.bitmap.resolution_verticale);
	printf("taille_palette: %d\n", ent_bmp.bitmap.taille_palette);
	printf("nombre_de_couleurs_importantes: %d\n", ent_bmp.bitmap.nombre_de_couleurs_importantes);
	return 0;
}