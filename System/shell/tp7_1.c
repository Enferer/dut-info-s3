#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#include "ligne_commande.h"

void affiche_prompt(){
	char *env = getenv("USER") ;
	
	char hostname[12];
	gethostname(hostname,12);
	char cwd[128] ;
	getcwd(cwd,128);
	printf("%s@%s:%s~$ ",env,hostname,cwd	);
	fflush(stdout);

}


void execute_ligne_commande(){
	int flag;
	int nb=0;
	pid_t pid = 42;

	char *** tab =ligne_commande(&flag,&nb);
	if(flag < 0 || tab==NULL || nb==0) return;

	int a =0;

	if((pid = fork())==0){
		a = execvp(tab[0][0],tab[0]);
		if(a < 0){
		
			perror("Commande incorect");
			return;
		}
	}
	libere(tab);
	int status = 0;
	if(flag == 0 ){
		waitpid(pid,&status,WUNTRACED);
	}
   	else{ 
		printf("Processus terminé par signal %d\n", WTERMSIG(status));
    }
}	
	


