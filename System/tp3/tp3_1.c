#include <stdio.h>



int mon_strlen(char s[]){
  int cpt = 0;
  while(s[cpt] != '\0'){
    cpt++;
  }
  return cpt;
}


int mon_strlen2(const char *s){
  
  const char *p = s;
  while((*p) != '\0'){
    p++;
  }
  return p-s;
}

int min( int i1, int i2 ){
  if(i1<i2){
    return i1;
  }
  return i2;
}


int mon_strcmp(const char * s1, const char * s2){
  int i;
  int l1=mon_strlen2(s1);
  int l2=mon_strlen2(s2);
  int mini;
  mini = min(l1,l2);
  for(i=0;i<mini && s1[i]==s2[i];++i){
  }
  return s1[i]-s2[i];

}

int mon_strcmp2(const char * s1, const char * s2, int n){
  int i;
  for(i=0;i<n && s1[i]==s2[i];++i){
  }
  return s1[i]-s2[i];
}


char *mon_strchr(char *s, char c){
  int i;
  for(i=0;i<c;i++){
    if(s[i]==c){
      return &s[i];
    }
  }
  return NULL;
}

char *mon_strstr(char *haystack, char *needle){
  int cpt = 0;
  int i = 0;
  char* res = NULL;
  for(;i<mon_strlen2(haystack);i++){
    if(haystack[i]==needle[cpt]){
      if(cpt==0){
	res = &haystack[i];
      }
      cpt++;
    }else{
      cpt = 0;
    }
    if(cpt == mon_strlen2(needle)){
      return res;
    }
  }
  return NULL;

  
}