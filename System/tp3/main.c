#include <stdio.h>
#include "tp3.h"


int main(void){  

  char s[] = "toto";
  char s2[] = "tata";
  char s3[] = "ot";
  printf(" mon_strlen de toto:%d\n",mon_strlen(s));
  printf(" mon_strlen2 de toto:%d\n",mon_strlen2(&s[0]));
  printf(" mon_strcmp de toto et tata:%d\n",mon_strcmp(&s[0],&s2[0]));
  printf(" mon_strcmp2 de toto et tata:%d\n",mon_strcmp2(&s[0],&s2[0],1));
  printf(" mon_strchr de toto avec o:%s\n",mon_strchr(&s[0],'o'));
  printf(" mon_strstr de toto avec ot:%s\n",mon_strstr(&s[0],&s3[0]));
  char s4[]="zz";
  printf(" mon_strstr de toto avec zz:%s\n",mon_strstr(&s[0],&s4[0]));

  
  return 1;
}
