#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "tp6.h"
#include <unistd.h>
#define BUFFER_SIZE 1000000

int lire_deux_octets(int fd, uint16 *val){
	return read(fd,val,2);
}

int lire_quatre_octets(int fd, uint32 *val){
	return read(fd,val,4);
}

int ecrire_deux_octets(int fd, uint16 *val){
	return write(fd, val, 2);
}

int ecrire_quatre_octets(int fd, uint32 *val){
	return write(fd, val, 4);
}

int bourrage(entete_bmp *entete){
	return (4-(entete->bitmap.largeur*3)%4)%4;
}
int ecrire_entete(int vers, entete_bmp *entete){
	int cpt=0;
	cpt+=ecrire_deux_octets(vers, &entete->fichier.signature);
	cpt+=ecrire_quatre_octets(vers, &entete->fichier.taille_fichier);
	cpt+=ecrire_quatre_octets(vers, &entete->fichier.reserve);
	cpt+=ecrire_quatre_octets(vers, &entete->fichier.offset_donnees);

	cpt+=ecrire_quatre_octets(vers, &entete->bitmap.taille_entete);
	cpt+=ecrire_quatre_octets(vers, &entete->bitmap.largeur);
	cpt+=ecrire_quatre_octets(vers, &entete->bitmap.hauteur);
	cpt+=ecrire_deux_octets(vers, &entete->bitmap.nombre_plans);
	cpt+=ecrire_deux_octets(vers, &entete->bitmap.profondeur);
	cpt+=ecrire_quatre_octets(vers, &entete->bitmap.compression);
	cpt+=ecrire_quatre_octets(vers, &entete->bitmap.taille_donnees_image);
	cpt+=ecrire_quatre_octets(vers, &entete->bitmap.resolution_horizontale);
	cpt+=ecrire_quatre_octets(vers, &entete->bitmap.resolution_verticale);
	cpt+=ecrire_quatre_octets(vers, &entete->bitmap.taille_palette);
	cpt+=ecrire_quatre_octets(vers, &entete->bitmap.nombre_de_couleurs_importantes);
	return cpt;
}

int lire_entete(int de, entete_bmp *entete){
	int cpt=0;
	cpt+=lire_deux_octets(de, &entete->fichier.signature);
	cpt+=lire_quatre_octets(de, &entete->fichier.taille_fichier);
	cpt+=lire_quatre_octets(de, &entete->fichier.reserve);
	cpt+=lire_quatre_octets(de, &entete->fichier.offset_donnees);

	cpt+=lire_quatre_octets(de, &entete->bitmap.taille_entete);
	cpt+=lire_quatre_octets(de, &entete->bitmap.largeur);
	cpt+=lire_quatre_octets(de, &entete->bitmap.hauteur);
	cpt+=lire_deux_octets(de, &entete->bitmap.nombre_plans);
	cpt+=lire_deux_octets(de, &entete->bitmap.profondeur);
	cpt+=lire_quatre_octets(de, &entete->bitmap.compression);
	cpt+=lire_quatre_octets(de, &entete->bitmap.taille_donnees_image);
	cpt+=lire_quatre_octets(de, &entete->bitmap.resolution_horizontale);
	cpt+=lire_quatre_octets(de, &entete->bitmap.resolution_verticale);
	cpt+=lire_quatre_octets(de, &entete->bitmap.taille_palette);
	cpt+=lire_quatre_octets(de, &entete->bitmap.nombre_de_couleurs_importantes);
	return cpt;
}
/** Regarde si l'entête a bien une profondeur de 24 bits*/
int verifier_entete(entete_bmp *entete){
	if(entete->bitmap.profondeur==24){
		return 1;
	}
	return 0;
}
/** Retourne un tableau assez grand pour accepter tous les pixel de l'image*/
unsigned char* allouer_pixels(entete_bmp *entete){
	
	int taille = entete->bitmap.taille_donnees_image;
	//printf("%d\n", taille);
	unsigned char* tableau= (unsigned char *) malloc(taille * sizeof(char ));
	return tableau;
}

/** copie les pixels contenus dans le fichier de dans le tableau pixels */
int lire_pixels(int de, entete_bmp *entete, unsigned char *pixels){
	lseek(de,entete->fichier.offset_donnees,SEEK_SET);
	int taille = entete->bitmap.taille_donnees_image;
	read(de, pixels, taille);
	return 0;
}

/** écrit les pixels contenus dans le tableau pixels dans le fichier vers*/
int ecrire_pixels(int vers, entete_bmp *entete, unsigned char *pixels){
	lseek(vers,entete->fichier.offset_donnees,SEEK_SET);
	
	int taille = entete->bitmap.taille_donnees_image;
	write(vers, pixels, taille);
	return 0;
}

int copier_bmp(int de, int vers){
	entete_bmp entete;
	unsigned char *pixels;
	/* lecture du fichier source */
	lire_entete(de, &entete);
	pixels = allouer_pixels(&entete);
	lire_pixels(de, &entete, pixels);
	/* écriture du fichier destination */
	ecrire_entete(vers, &entete);
	ecrire_pixels(vers, &entete, pixels);
	/* on libère les pixels */
	free(pixels);
	return 1; /* on a réussi */
}

void rouge(entete_bmp *entete, unsigned char *pixels){
	//int taille = 3*entete->bitmap.largeur;
	int cpt = 0;
	int unsigned i;
	int unsigned j;
	
	for(i=0; i<entete->bitmap.hauteur; i++){
		for(j=0; j<entete->bitmap.largeur; j++){
			pixels[cpt]=0;//cpt+2
			pixels[cpt+1]=0;         
			cpt += 3;
		}
		cpt += bourrage(entete);
	}
}
void negatif(entete_bmp *entete, unsigned char *pixels){
	//int taille = 3*entete->bitmap.largeur;
	int cpt = 0;
	int unsigned i;
	int unsigned j;
	
	for(i=0; i<entete->bitmap.hauteur; i++){
		for(j=0; j<entete->bitmap.largeur; j++){
			pixels[cpt]=~pixels[cpt];
			pixels[cpt+1]=~pixels[cpt+1];
			pixels[cpt+2]=~pixels[cpt+2];
			cpt += 3;
		}
		cpt += bourrage(entete);
	}
}
void noirEtBlanc(entete_bmp *entete, unsigned char *pixels){
	//int taille = 3*entete->bitmap.largeur;
	int cpt = 0;
	int unsigned i;
	int unsigned j;
	int val=0;
	for(i=0; i<entete->bitmap.hauteur; i++){
		for(j=0; j<entete->bitmap.largeur; j++){
			val=(pixels[cpt]+pixels[cpt+1]+pixels[cpt+2])/3;
			pixels[cpt]=val;
			pixels[cpt+1]=val;
			pixels[cpt+2]=val;
			cpt += 3;
		}
		cpt += bourrage(entete);
	}
}

void moitie(entete_bmp *entete, unsigned char *pixels, int sup){
	int largeur=entete ->bitmap.largeur * (entete->bitmap.profondeur/8)+bourrage(entete);
	int i,j;
	if(entete->bitmap.hauteur%2==0){
		entete->bitmap.hauteur/=2;
	}else{
		entete->bitmap.hauteur=(entete->bitmap.hauteur-1)/2;
	}
	entete->bitmap.taille_donnees_image=largeur*entete->bitmap.hauteur;
	entete->fichier.taille_fichier=entete->bitmap.taille_donnees_image+entete->fichier.offset_donnees;

	if(sup==1){
		for( i=0;i<(int)entete->bitmap.hauteur;i++){
			for(j=0;j<largeur;j++){
				pixels[i*largeur+j]=pixels[(i+entete->bitmap.hauteur)*largeur+j];
			}
		}
	}
}
/*int taille_bourrage(entete_bmp *entete){
	int x=(entete->bitmap.largeur*3)%4;
	x=(4-x)%4;
	return x;
}*/

void afficherEntete(entete_bmp *ent_bmp){
	printf("signature: %d\n", ent_bmp->fichier.signature);
	printf("taille_fichier: %d\n", ent_bmp->fichier.taille_fichier);
	printf("reserve: %d\n", ent_bmp->fichier.reserve);
	printf("offset_donnees: %d\n", ent_bmp->fichier.offset_donnees);
	printf("taille_entete: %d\n", ent_bmp->bitmap.taille_entete);
	printf("largeur: %d\n", ent_bmp->bitmap.largeur);
	printf("hauteur: %d\n", ent_bmp->bitmap.hauteur);
	printf("nombre_plans: %d\n", ent_bmp->bitmap.nombre_plans);
	printf("profondeur: %d\n", ent_bmp->bitmap.profondeur);
	printf("compression: %d\n", ent_bmp->bitmap.compression);
	printf("taille_donnees_image: %d\n", ent_bmp->bitmap.taille_donnees_image);
	printf("resolution_horizontale: %d\n", ent_bmp->bitmap.resolution_horizontale);
	printf("resolution_verticale: %d\n", ent_bmp->bitmap.resolution_verticale);
	printf("taille_palette: %d\n", ent_bmp->bitmap.taille_palette);
	printf("nombre_de_couleurs_importantes: %d\n", ent_bmp->bitmap.nombre_de_couleurs_importantes);
}

	int main(int argc, char **argv){
		int i;
		int fd= open(argv[argc-2], O_RDONLY);
		entete_bmp ent_bmp;
		if(fd<0){
			return 1;
		}
		lire_entete(fd, &ent_bmp);
		if(verifier_entete(&ent_bmp)==1){
		unsigned char* tab = allouer_pixels(&ent_bmp);
		lire_pixels(fd, &ent_bmp, tab);
		int vers = open(argv[argc-1], O_WRONLY | O_CREAT | O_TRUNC, 0666);

		for(i=1; i<argc; i++){
			char* s= argv[i];
			if(s[0]=='-'){
				if(s[1]=='h'){
					if (argc<3){
		      			fprintf(stderr, "utilisation: %s source destination\n", argv[0]);
		      			return 1;
		   			}
					int fd= open(argv[argc-1], O_RDONLY);
					entete_bmp ent_bmp;
					lire_entete(fd, &ent_bmp);
					if(verifier_entete(&ent_bmp)==0){
						printf("Erreur entete\n");
						return 1;
					}
					unsigned char* tab = allouer_pixels(&ent_bmp);
					lire_pixels(fd, &ent_bmp, tab);
					afficherEntete(&ent_bmp);
					close(fd);
					free(tab);
				}
				if(s[1]=='r'){
					rouge(&ent_bmp, tab);
					  if (vers == -1 ){
		      			perror(argv[2]);
		      			return 1;
		   			  }
				}
				if(s[1]=='c'){
					int fd= open(argv[argc-2], O_RDONLY);
					int cop= open(argv[argc-1], O_WRONLY | O_CREAT | O_TRUNC, 0666);
					if (cop == -1 ){
		     			perror(argv[argc-1]);
		      			return 1;
		   			}
					copier_bmp(fd, cop);
					close(fd);
					close(cop);
				}
				if(s[1]=='n'){
					negatif(&ent_bmp, tab);
					  if (vers == -1 ){
		      			perror(argv[argc-2]);
		      			return 1;
		   			  }
				}
				if(s[1]=='b')
				{
					noirEtBlanc(&ent_bmp, tab);
					  if (vers == -1 ){
		      			perror(argv[argc-2]);
		      			return 1;
		   			  }
				}
				if(s[1]=='i'){
					moitie(&ent_bmp, tab, 0);
				}
				if(s[1]=='s'){
					moitie(&ent_bmp, tab, 1);
				}
			}
		}
		ecrire_entete(vers, &ent_bmp);
		ecrire_pixels(vers, &ent_bmp, tab);
		close(fd);
		close(vers);
		return 0;
	}return -1;
}
