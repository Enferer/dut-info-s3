#include "ds.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main (void){
	char* s = "thibaut";
	sapin(s);
   if (fork() == 0) /* fils */
   {
      char *argv[] = {"ls", "-l", NULL};
      execvp(argv[0], argv);
      perror(argv[0]);
      return 1;
   }
   int status;
   wait(&status);
   if (WIFEXITED(status))
   {
      printf("Fin de ls: ");
      if (WEXITSTATUS(status) == 0)
         printf("sans erreur\n");
      else
         printf("avec erreur\n");
   }
   else //!WIFEXITED 
   {
      printf("Processus terminé par signal %d\n", WTERMSIG(status));
   }
   return 0;
}