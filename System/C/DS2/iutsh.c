#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "ligne_commande.h"

void execute_ligne_commande(char **cmd) {
	const char *path = cmd[0];
	char **arg = &cmd[0];
	if (execvp(path, arg) == -1) {
		perror("Erreur: Commande inexistante");
		exit(0);
	}
}

void affiche_prompt() {
	char prompt[1024];
	char host[9];
	host[8] = '\0';
	gethostname(host, 8);
	char *login = getlogin();
	char cwd[1024];
	getcwd(cwd, 1024);
	strcat(prompt, login);
	strcat(prompt, "@");
	strcat(prompt, host);
	strcat(prompt, ":");
	strcat(prompt, cwd);
	strcat(prompt, "$ ");
	printf(prompt);
	fflush(stdout);
	memset(prompt, 0, sizeof prompt);
}

int lance_commande(int in, int out, char *com, char ** argv) {
	int pid = fork();
	if (pid == -1) {
		perror("Fork");
		return -1;
	}
	if (pid == 0) {
		if (in != 0) {
			close(0);
			dup(in);
		}
		if (out != 1) {
			close(1);
			dup(out);
		}
		if (execvp(com, argv) == -1) {
			perror("Erreur: Commande inexistante");
			exit(0);
		}
	}
	return pid;
}

int main() {
	int flag = 0;
	int nb = 0;
	//int status = 0;
	system("clear");
	while (1) {
		affiche_prompt();
		char *** list_cmd = ligne_commande(&flag, &nb);
		if (flag != -1) {
			if (nb > 1) {
				int i;
				int tmp = 0;
				for (i = 0; i < nb - 1; i++) {
					int t[2];
					pipe(t);
					lance_commande(tmp, t[1], list_cmd[i][0], list_cmd[i]);
					printf("pipe : %d,t[0] : %d , t[1] : %d\n", i + 1, tmp,
							t[1]);
					tmp = t[0];
				}

				lance_commande(tmp, 1, list_cmd[nb - 1][0], list_cmd[nb - 1]);
				printf("pipe : %d,t[0] : %d , t[1] : %d\n", i + 1, tmp, 1);
			} else
				lance_commande(0, 1, list_cmd[0][0], list_cmd[0]);
			libere(list_cmd);
			waitpid(-1, NULL, WUNTRACED);
		} else {
			perror("Erreur Commande");
		}

	}
	return 0;
}

