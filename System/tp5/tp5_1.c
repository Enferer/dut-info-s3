#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

int traiter (int f, int *car, int *mot, int *lig) {
	char tab[80];
	int i;
	int rd = 0;
	int espace = 1;
	do {
		rd = read(f,tab,80);
		for (i = 0; i < rd; i++) {
			if ( isspace(tab[i]) ) {
				espace = 1;
			}
			else if (espace == 1) {
				(*mot) += espace;
				espace = 0;
			}
			if ( tab[i] == '\n' ) {
					(*lig) ++;
				}
			(*car) ++;
		}
	} while ( rd > 0 );
	if (rd == -1) {
		perror("traiter");
	}
	return 0;
}