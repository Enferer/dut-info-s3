#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include "tp5.h"

int main(int argc, char** argv) {

	int cpt = 1;
	int l = 0;
	int c = 0;
	int w = 0;
	int fd;
	char *res ='\0';
	int totalc = 0;
	int* ca = &totalc;
	int totall = 0;
	int* li = &totall;
	int totalm = 0;
	int* mo = &totalm;
	int ttotalm = 0,ttotalc = 0,ttotall = 0;
	if ( argc < 2 ) {
		fd = 0;
		traiter(fd,ca,mo,li);
		printf("Nombre de mots : %d\n",totalm);
	 	printf("Nombre de lignes : %d\n",totall);
		printf("Nombre de caracteres : %d\n",totalc);
	}
	else {
		printf("\n");
		while(cpt < argc) {
			char *temp = argv[cpt];
			if(temp[0] == '-') {
				int cpt2 = 1;
				while(temp[cpt2] != '\0') {
					if(temp[cpt2] == 'l') {
						l = 1;
					}
					else if(temp[cpt2] =='c') {
						c = 1;
					}
					else if(temp[cpt2] =='w') {
						w = 1;
					}
					cpt2++;
				}		
			}
			else {
				res = temp;
				if ( res == NULL ) {
					fd = 0;
				}
				else {
					fd = open(res,O_RDONLY);
				}
				if ( fd == -1 ) {
					//perror(res);
					//return 0;
				}else{
					traiter(fd,ca,mo,li);
					if ( w == 0 && c == 0 && l == 0 ) {
						w = 1;
						c = 1;
						l = 1;
					}
					printf("---- Fichier : %s -----\n\n",res);
					if( w == 1 ) {
						printf("Nombre de mots : %d \n",totalm);
						ttotalm += totalm;
					}
					if( l == 1 ) {
						printf("Nombre de lignes : %d \n",totall); 
						ttotall += totall;
					}
					if( c == 1 ) {
						printf("Nombre de caracteres : %d \n",totalc);
						ttotalc += totalc;
					}
					close(fd);
					printf("\n");
				}
				
			}
			cpt++;
		}
	printf("\nNombre total de mots, lignes, caractères : %d,%d,%d\n\n",ttotalm,ttotall,ttotalc);	
	}
	return 0;
}
