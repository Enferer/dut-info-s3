#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "tp4.h"


int main(int argc , char ** argv){

  int boolMiroir = 0;
  int boolSaisie = 0;
  
  int i = 1;
  for(;i < argc;i++){
    if(estOption(argv[i])){
      int j = strlen(argv[i])-1;
      for(;j>0;j--){
	if(argv[i][j]=='s'){
	  boolSaisie = 1;
	}
	else if(argv[i][j]=='m'){
	  boolMiroir = 1;
	}
	else{
	  printf("Option inconnu");
	  return 0;
	}
      }
    }
  }

  if(boolMiroir && !boolSaisie){
    if(getParametre(argc,argv) != NULL){
      printf("%s",miroir(getParametre(argc,argv)));
    }
  }
  else if(boolSaisie && !boolMiroir){
    printf("%s",saisie());
  }else if(boolSaisie && boolMiroir){
    printf("%s",miroir(saisie()));
  }
  
  printf("\n");

  return 1;

}
