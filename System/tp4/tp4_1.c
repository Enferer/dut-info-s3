#include <stdio.h>
#include <string.h>
#include <stdlib.h>


char* miroir (char *s){
  char* res = malloc((strlen(s)+1)*sizeof(char));
  int i = strlen(s)-1;
  int j = 0;
  for(;i>=0;i--){
    res[j] = s[i];
    j++;
    
  }
  res[j]='\0';
  return res;
}


char* saisie(){
  int actuel = 0;
  int max = 500;
  char* res = malloc(max);
  int espace = 0;
  char c = getchar();
  int i = 0;
  while(c!='\n' && !espace){
    actuel++;

    if(actuel>= max){
      max += max;
      res = realloc(res,max);
    }
   // res = realloc(res,(i+1*sizeof(char));
    res[i] = c;
    i++;
    if(c == ' '){
      espace = 1;
    }else{
      c = getchar();  
    }
  }
  res = realloc(res,(i+1));
  res[i]='\0';
  return res;
}

int estOption(char * s){
  if(*s=='-'){
    return 1;
  }
  return 0;
}

char* getParametre(int argc,char** arg){
  int i = 1 ;
  int idx = -1;

  for(;i <argc;i++){
    if(!estOption(arg[i])){
      if(idx != -1){
	return NULL;
      }
      idx = i;
    }
  }
  return arg[idx];
  
}
