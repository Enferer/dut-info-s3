#include <stdio.h>
#include "tp2_1.h"




void afficher(int liste [], int taille){
  int i;
  for(i=0;i<taille;i++){
    printf("idx %d : %d \n",i,liste[i]);
    
  }

}

int somme (int liste[], int taille){
  int i;
  int res=0;
  for(i=0;i<taille;i++){
    res+=liste[i];
  }
  return res;
}

void copie_dans(int dest[], int src[], int taille){
  int i=0;
  for(i=0;i<taille;i++){
    dest[i]=src[i];
  }
}

void ajoute_apres(int dest[], int taille_dest, int src[], int taille_src){
  int j;
  for(j=0;j<taille_src;j++){
    dest[(taille_dest-taille_src)+j]=src[j];
  }
}

struct rat rat_produit(struct rat n1, struct rat n2){
  struct rat res;
  res.den=n1.den*n2.den;
  res.num=n1.num*n2.num;
  return res;
}

struct rat rat_somme(struct rat n1, struct rat n2){
  struct rat res;
  n1.den=n1.den*n2.den;
  n1.num=n1.num*n2.den;
  n2.num=n2.num*(n1.den/n2.den);
  n2.den=n1.den;
  res.den=n1.den;
  res.num=n1.num+n2.num;
  return res;

}

struct rat rat_difference(struct rat n1, struct rat n2){
  struct rat res;
  n1.den=n1.den*n2.den;
  n1.num=n1.num*n2.den;
  n2.num=n2.num*(n1.den/n2.den);
  n2.den=n1.den;
  res.den=n1.den;
  res.num=n1.num-n2.num;
  return res;

}



struct rat rat_plus_petit(struct rat list[]){
  struct rat min;
  int i=1;
  min=list[0];
  while(list[i].den!=0){

    if(rat_difference(list[i],min).num<0){
      min=list[i];
    }
	
    i++;
  }
  return min;


}
