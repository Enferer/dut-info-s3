#include "tp2_1.h"
#include <stdio.h>

int main(void){

  typedef struct rat rat;

  int tab1 [5] = { 1, 15, 42, 24,-10};
  int tab2 [6] = { 8, 31, -5, 14,-42};
  printf("Tab 1\n");
  afficher(tab1,5);
  printf("Tab 2\n");
  afficher(tab2,6);
  printf("Tab 1 copi dans tab2\n");
  copie_dans(tab2,tab1,5);
  printf("Tab 1\n");
  afficher(tab1,5);
  printf("Tab 2\n");
  afficher(tab2,6);
  printf("--------\najouter apres de tab3 et tab1\n");
  int tab3 [10]={1,2,3,4,5};
  ajoute_apres(tab3,10,tab1,5);
  afficher(tab3,10);

  printf("\n----- rationel -----\n\n");

  rat r1,r2;
  r1.num=2;
  r1.den=4;
  r2.num=1;
  r2.den=4;
  rat r3=rat_produit(r1,r2);
  printf("produit r1 et r2 : num %d den %d \n",r3.num,r3.den); 
  rat r4=rat_somme(r1,r2);
  printf("somme r1 et r2 : num %d den %d \n",r4.num,r4.den);

  rat listerat [5];
  listerat[0]=r4;
  listerat[1]=r3;
  listerat[2]=r2;
  listerat[3]=r1;
  rat rl1={ 10, 1 };
  rat rlf={ 0 , 0 };
  listerat[4]=rl1;
  listerat[5]=rlf;
  rat rmin = rat_plus_petit(listerat);
  printf("min : num: %d den %d \n",rmin.num,rmin.den);
  return 0;
}
